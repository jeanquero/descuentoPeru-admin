<?php

require_once "constantsDB.php";

class Config {

    private $con;
    
    public function __construct(){
        }
   
   public function setCon($con) {
        $this->con=$con;
    }

    public function getCon() {
        return $this->con;
    }

    public function getConexion() {
    $this->con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
        /* check connection */
    if (mysqli_connect_errno()) {
    printf("Error de conexión: %s\n", mysqli_connect_error());
    exit();
    }
     /*$this->con = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
        if($link === false){

        die("ERROR: Could not connect. " . mysqli_connect_error());*/

        }

   
}