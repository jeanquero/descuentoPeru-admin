<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include_once "../model/GeneryDao.php";

class GeneryController {

    public function __construct() {
        
    }

    function category() {
        $generyDao = new GeneryDao();
        $row = $generyDao->category();
        return $row;        
    }
    
    function sub_category() {
        $generyDao = new GeneryDao();
        $row = $generyDao->sub_category();
        return $row;        
    }
    
    function person($name) {
        $generyDao = new GeneryDao();
        $row = $generyDao->person($name);
        return $row;        
    }
       
    function rol() {
        $generyDao = new GeneryDao();
        $row = $generyDao->rol();
        return $row;        
    }
    
    function restri() {
        $generyDao = new GeneryDao();      
        return $generyDao->restri();
        
    }
    
    function negocios($id) {
        $generyDao = new GeneryDao();      
        return $generyDao->negocios($id);
        
    }

}

?>
