<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once "../model/entity/Login.php";
require_once "../model/entity/Person.php";
include_once "../model/AdminDao.php";

class AdminController {

    public function __construct() {
        
    }

    function users() {

        $adminDao = new AdminDao();
        $row = $adminDao->users();
        return $row;
        
    }
    
    function blockUser($id, $block){
        $adminDao = new AdminDao();
        $row = $adminDao->blockUser($id,$block);
        return $row;
    }
    
    function registryPerson($firstName, $lastName, $email, $sex, $birthdate, $login, $password,$phone,$rol) {


        $person = new Person();
        $person->setFirstName($firstName);
        $person->setLastName($lastName);
        $person->setEmail($email);
        $person->setSex($sex);
        $date2 = DateTime::createFromFormat('d-m-Y', $birthdate);
        $person->setBirthdate($date2->format('Y-m-d'));
        $person->setPhone($phone);
        $user = new Login();
        $user->setLogin($login);
        $user->setPassword($password);
        
        //var_dump($person);
        $registro = new AdminDao();

        return $registro->regPerson($person, $user,$rol);
    }
    
    
    
    

}

?>
