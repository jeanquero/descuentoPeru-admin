<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include_once "../model/AddressesDao.php";

class AddressesController {

    public function __construct() {
        
    }

    function department() {

        $AddressesDao = new AddressesDao();
        $row = $AddressesDao->department();
        return $row;
        
    }
    
    function province($id) {

        $AddressesDao = new AddressesDao();
        $row = $AddressesDao->province($id);
        return $row;
        
    }
    
    function district($id) {

        $AddressesDao = new AddressesDao();
        $row = $AddressesDao->district($id);
        return $row;
        
    }
    
    

}

?>
