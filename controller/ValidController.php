<?php

require_once "../model/ValidDao.php";

class ValidController {

    public function __construct() {
        
    }

    function validEmail($email) {
        $validDao = new ValidDao();
        return $validDao->validEmail($email);
    }

}

?>