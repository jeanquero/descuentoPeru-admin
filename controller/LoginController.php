<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once "model/entity/Login.php";
require_once "model/entity/Person.php";
require_once "model/entity/Department.php";
require_once "model/entity/District.php";
require_once "model/entity/Province.php";
require_once "model/LoginDao.php";

class LoginController {

    public function __construct() {
        
    }

    function login($login, $password) {

        $user = new Login();
        $user->setLogin($login);
        $user->setPassword($password);
        $loginDao = new LoginDao();
        $row = $loginDao->login($user);
        if ($row != null) {
            
            session_start();
            ob_start();
error_reporting(0);
            $user->setIdPerson($row[1]);
            $user->setId($row[0]);
            $user->setUpdatePerson($row[4]);
            $row_person = $loginDao->person($user->getIdPerson());
            $_SESSION['user'] = serialize($user);
            
            if ($row_person != null) {
                $person = new Person();
                $person->setId($user->getIdPerson());
                $person->setFirstName($row_person[1]);
                $person->setLastName($row_person[2]);
                $person->setEmail($row_person[3]);
                $person->setSex($row_person[4]);
                $date2 = DateTime::createFromFormat('Y-m-d H:i:s', $row_person[5]);
                $person->setBirthdate($date2->format('d-m-Y'));
                $person->setPhone($row_person[6]);
                $person->setUpdatePerson($row_person[7]);
                $person->setImgProfile($row_person[8]);
                $department = new Department();
                $department->setId($row_person[9]);
                $department->setNameDepartment($row_person[10]);
                $person->setDepartment($department);
                $province = new Province();
                $province->setId($row_person[11]);
                $province->setNameProvince($row_person[12]);
                $person->setProvince($province);
                $district = new District();
                $district->setId($row_person[13]);
                $district->setNameDistrict($row_person[14]);
                $person->setDistrict($district);
                $_SESSION['person'] = serialize($person);  
                $_SESSION['notifications'] = $loginDao->notifications($user->getIdPerson());
                $_SESSION['rol'] = $loginDao->rol($user->getId());
            }
            $_SESSION['login'] = TRUE;
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>
