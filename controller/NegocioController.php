<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include_once "../model/NegocioDao.php";

class NegocioController {

    public function __construct() {
        
    }

    

    function regNegocio($id_person,$name,$id_category,$id_departament,$id_province,$id_district,$address,$img,$description,$facebook,$web,$phone,$email,$office_phone,$service_extra, $int_dias,$int_horas,$id_sub_category) {
        $negocioDao = new NegocioDao();
        
        return $negocioDao->regNegocio($id_person,$name,$id_category,$id_departament,$id_province,$id_district,$address,$img,$description,$facebook,$web,$phone,$email,$office_phone,$service_extra, $int_dias,$int_horas,$id_sub_category);
        
    }
    
    function misNegocios($id_person) {
        $negocioDao = new NegocioDao();        
        return $negocioDao->misNegocios($id_person);
        
    }
    
    function deleteNegocio($id) {
        $negocioDao = new NegocioDao();        
        return $negocioDao->deleteNegocio($id);
    }
    
    function buscarNegocio($id_comm){
        $negocioDao = new NegocioDao();        
        return $negocioDao->buscarNegocio($id_comm);
    }
    
    function buscarTime($id_comm){
        $negocioDao = new NegocioDao();        
        return $negocioDao->buscarTime($id_comm);        
    }

    function buscarServiceExtra($id_comm){
        $negocioDao = new NegocioDao();        
        return $negocioDao->buscarServiceExtra($id_comm);        
    }

    function updateNegocio($id,  $name, $id_category, $id_departament, $id_province, $id_district, $address, $img, $description, $facebook, $web, $phone, $email, $office_phone, $service_extra, $int_dias, $int_horas, $id_sub_category,$del_time,$service_extra_delete) {
        $negocioDao = new NegocioDao();          
        return $negocioDao->updateNegocio($id,  $name, $id_category, $id_departament, $id_province, $id_district, $address, $img, $description, $facebook, $web, $phone, $email, $office_phone, $service_extra, $int_dias, $int_horas, $id_sub_category, $del_time,$service_extra_delete); 
        
    }

}

?>
