<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include_once "../model/UpdatePersonDao.php";


class UpdatePersonController {

    public function __construct() {
        
    }

    function updatePerson($firstName, $lastName, $sex, $birthdate, $phone, $department, $province, $district, $foto, $person) {
        $updatePersonDao = new UpdatePersonDao();
        $date2 = DateTime::createFromFormat('d-m-Y', $birthdate);
        $birthdate = $date2->format('Y-m-d');
        $response = $updatePersonDao->updatePerson($firstName, $lastName, $sex, $birthdate, $phone, $department, $province, $district, $foto, $person->getId());
        if ($response) {
            $row_person = $updatePersonDao->person($person->getId());
            if ($row_person != null) {
                $person->setFirstName($row_person[1]);
                $person->setLastName($row_person[2]);
                $person->setEmail($row_person[3]);
                $person->setSex($row_person[4]);
                $date2 = DateTime::createFromFormat('Y-m-d H:i:s', $row_person[5]);
                $person->setBirthdate($date2->format('d-m-Y'));
                $person->setPhone($row_person[6]);
                $person->setUpdatePerson($row_person[7]);
                $person->setImgProfile($row_person[8]);
                $department = new Department();
                $department->setId($row_person[9]);
                $department->setNameDepartment($row_person[10]);
                $person->setDepartment($department);
                $province = new Province();
                $province->setId($row_person[11]);
                $province->setNameProvince($row_person[12]);
                $person->setProvince($province);
                $district = new District();
                $district->setId($row_person[13]);
                $district->setNameDistrict($row_person[14]);
                $person->setDistrict($district);
                session_start();
                $_SESSION['person'] = serialize($person);
            }
            return $response;
        } else {
            return $response;
        }
    }

    function updatePassword($pass, $pass_actual) {
        $updatePersonDao = new UpdatePersonDao();
        session_start();
        $login = unserialize($_SESSION["user"]);
        $respActual = $updatePersonDao->validPassword($pass_actual, $login->getId());
        if ($respActual) {
            $response = $updatePersonDao->updatePassword($pass, $login->getId());
            if ($response) {
                echo 0;
            } else {
                echo 1;
            }
        } else {
            echo 2;
        }
    }

    function updateNoti($campo, $check, $id_person, $id_noti) {
        $updatePersonDao = new UpdatePersonDao();
        $respActual = $updatePersonDao->updateNoti($campo, $check, $id_person, $id_noti);
        if ($respActual) {
            session_start();
            $_SESSION['notifications'] = $updatePersonDao->notifications($id_person);
            echo 0;
        } else {
            echo 1;
        }
    }

}

?>
