<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include_once "../model/OfertasDao.php";

class OfertasController {

    public function __construct() {
        
    }

    

    function regOferta($id_person, $name, $desde, $hasta, $aplica, $img, $description, $service_extra, $int_dias, $int_horas,$restriccion,$id_commerce) {
        $ofertasDao = new OfertasDao();        
        return $ofertasDao->regOferta($id_person, $name, $desde, $hasta, $aplica, $img, $description, $service_extra, $int_dias, $int_horas,$restriccion,$id_commerce);
        
    }
    
    function misOfertas($id_person) {
        $ofertasDao = new OfertasDao();        
        return $ofertasDao->misOfertas($id_person);
        
    }
    
    function activeOferta($id,$active){
        $ofertasDao = new OfertasDao();        
        return $ofertasDao->activeOferta($id,$active);
    }
    
    
    function deleteOfertas($id) {
        $ofertasDao = new OfertasDao();        
        return $ofertasDao->deleteOfertas($id);
    }
    
    function buscarOfertas($id_ofertas){
        $ofertasDao = new OfertasDao();        
        return $ofertasDao->buscarOfertas($id_ofertas);
    }
    
    function buscarTime($id_oferta){
        $ofertasDao = new OfertasDao();        
        return $ofertasDao->buscarTime($id_oferta);        
    }

    function buscarExtra($id_oferta){
        $ofertasDao = new OfertasDao();        
        return $ofertasDao->buscarExtra($id_oferta);        
    }



    function updateOferta($id_person, $name, $desde, $hasta, $aplica, $img, $description, $service_extra, $int_dias, $int_horas, $restriccion,$id_commerce,$del_time,$service_extra_delete) {
        $ofertasDao = new OfertasDao();          
        return $ofertasDao->updateOferta($id_person, $name, $desde, $hasta, $aplica, $img, $description, $service_extra, $int_dias, $int_horas, $restriccion,$id_commerce,$del_time,$service_extra_delete); 
        
    }

}

?>
