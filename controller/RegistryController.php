<?php

require_once "../model/entity/Login.php";
require_once "../model/entity/Person.php";
require_once "../model/RegistryDao.php";

class RegistryController {

    public function __construct() {
        
    }

    function registryPerson($firstName, $lastName, $email, $sex, $birthdate, $login, $password) {


        $person = new Person();
        $person->setFirstName($firstName);
        $person->setLastName($lastName);
        $person->setEmail($email);
        $person->setSex($sex);
        $date2 = DateTime::createFromFormat('d-m-Y', $birthdate);
        $person->setBirthdate($date2->format('Y-m-d'));
        $user = new Login();
        $user->setLogin($login);
        $user->setPassword($password);
        $registroDao = new RegistryDao();

        return $registroDao->regPerson($person, $user);
    }

    function resetPass($email) {
        $registroDao = new RegistryDao();
        return $registroDao->resetEmail($email);
    }

    function updatePassword($pass, $id) {
        $registroDao = new RegistryDao();
        return $registroDao->updatePassword($pass, $id);
    }

}

?>