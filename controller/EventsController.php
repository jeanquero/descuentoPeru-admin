<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include_once "../model/EventsDao.php";

class EventsController {

    public function __construct() {
        
    }

    

    function regEvent($id_person, $name, $desde, $hasta,$dia, $varios_dias, $description,$id_departament, $id_province, $id_district, $address,$organize,$otro, $facebook, $web, $phone, $email, $office_phone, $type_event,$id_coin,$coin, $int_horas, $assistance_type,$img){
    $eventosDao = new EventsDao();        
        return $eventosDao->regEvent($id_person, $name, $desde, $hasta,$dia, $varios_dias, $description,$id_departament, $id_province, $id_district, $address,$organize,$otro, $facebook, $web, $phone, $email, $office_phone, $type_event,$id_coin,$coin, $int_horas, $assistance_type,$img);
        
    }
    
    function misEvents($id_person) {
        $eventosDao = new EventsDao();        
        return $eventosDao->misEvents($id_person);
        
    }
    
    
    
    function deleteEvents($id) {
        $eventosDao = new EventsDao();        
        return $eventosDao->deleteEvents($id);
    }

    function activeEvents($id,$active) {
        $eventosDao = new EventsDao();        
        return $eventosDao->activeEvents($id,$active);
    }
    
    function buscarEvents($id_event){
        $eventosDao = new EventsDao();        
        return $eventosDao->buscarEvents($id_event);
    }
    
    function buscarTime($id_oferta){
        $eventosDao = new EventsDao();        
        return $eventosDao->buscarTime($id_oferta);        
    }
    function updateEvent($id, $name, $desde, $hasta,$dia, $varios_dias, $description,$id_departament, $id_province, $id_district, $address,$organize,$otro, $facebook, $web, $phone, $email, $office_phone, $type_event,$id_coin,$coin, $int_horas, $assistance_type,$img,$del_time){
        $eventosDao = new EventsDao();          
        return $eventosDao->updateEvent($id, $name, $desde, $hasta,$dia, $varios_dias, $description,$id_departament, $id_province, $id_district, $address,$organize,$otro, $facebook, $web, $phone, $email, $office_phone, $type_event,$id_coin,$coin, $int_horas, $assistance_type,$img,$del_time) ;
        
    }

}

?>
