<?php

require_once "../config/config.php";

class NegocioDao {

    public function __construct() {
        
    }

    function regNegocio($id_person, $name, $id_category, $id_departament, $id_province, $id_district, $address, $img, $description, $facebook, $web, $phone, $email, $office_phone, $service_extra, $int_dias, $int_horas, $id_sub_category) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $mysqli->autocommit(FALSE);
        $query = "INSERT INTO `commerce` (`id_commerce`, `id_person`, `name`, `id_category`,`id_sub_category`,`id_departament`, `id_province`, `id_district`, `address`, `img`, `description`, `facebook`, `web`, `phone`, `email`, `office_phone`, `update_commerce`, `create_commerce`,delete_commerce) "
                . "VALUES (NULL, '" . $id_person . "', '" . $name . "', '" . $id_category . "','" . $id_sub_category . "', '" . $id_departament . "', '" . $id_province . "', '" . $id_district . "', '" . $address . "', '" . $img . "', '" . $description . "', '" . $facebook . "', '" . $web . "', '" . $phone . "', '" . $email . "', '" . $office_phone . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'0');";
        //echo $query;
        if ($mysqli->query($query)) {
            $id = $mysqli->insert_id;
            if (!empty($service_extra)) {
                $extra = explode(",", $service_extra);
                // echo $extra[0];
                for ($index = 0; $index < count($extra); $index++) {
                    $query_extra = "INSERT INTO `extra_service` (`id_extra_service`, `service`, `id_commerce`, `update_service`, `create_service`,`delete_service_extra`) VALUES (NULL, '" . $extra[$index] . "', '" . $id . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,0)";
                    if (!$mysqli->query($query_extra)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            

            if (!empty($int_dias)) {
                $dia = explode(",", $int_dias);
                $hora = explode(",", $int_horas);
                for ($i = 0; $i < count($hora); $i++) {
                    $query_time = "INSERT INTO `time_table` (`id_timetable`, `dia`, `hora`, `id_commerce`, `update_timetable`, `create_timetable`,`delete_time_table`) VALUES (NULL, '" . $dia[$i] . "', '" . $hora[$i] . "', '" . $id . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'0')";
                    if (!$mysqli->query($query_time)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            $mysqli->commit();
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->rollback();
            $mysqli->close();
            return FALSE;
        }
    }

    function misNegocios($id_person) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();


        $query = "SELECT * FROM `commerce` WHERE delete_commerce = 0 and `id_person`=  " . $id_person;
        if ($result = $mysqli->query($query)) {


            /* free result set */
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                    $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }

    function deleteNegocio($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "UPDATE `commerce` SET `delete_commerce` = '1' WHERE `commerce`.`id_commerce` = " . $id;
        if ($mysqli->query($query) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }

    function buscarNegocio($id_comm) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT cm.id_commerce,cm.id_person,cm.name,cm.address,cm.img,cm.description,cm.facebook,cm.web,cm.phone,"
                . "cm.email,cm.office_phone,d.name_department,d.id_department,p.name_province,p.id_province, di.id_district,"
                . " di.name_district, ca.id_category,ca.name as name_category,ca.icons, sc.id_sub_category,sc.name as name_sub_category,sc.icons FROM commerce cm "
                . "JOIN department d ON d.id_department=cm.id_departament JOIN province p ON p.id_province=cm.id_province "
                . "JOIN district di ON di.id_district = cm.id_district JOIN category ca ON ca.id_category= cm.id_category "
                . "JOIN sub_category sc ON sc.id_sub_category = cm.id_sub_category WHERE cm.id_commerce=" . $id_comm;
        // echo $query;
        if ($result = $mysqli->query($query)) {
            $row = $result->fetch_object();
            if ($row != null) {
                $mysqli->close();
                return $row;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }

    function buscarTime($id_comm) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT * FROM `time_table` WHERE `delete_time_table`= 0 AND  id_commerce=" . $id_comm;
        // echo $query;
        if ($result = $mysqli->query($query)) {
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                    $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }
    
    function buscarServiceExtra($id_comm) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT * FROM `extra_service`  WHERE `delete_service_extra`= 0 AND  id_commerce=" . $id_comm;
        // echo $query;
        if ($result = $mysqli->query($query)) {
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                    $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }


     function updateNegocio ($id,  $name, $id_category, $id_departament, $id_province, $id_district, $address, $img, $description, $facebook, $web, $phone, $email, $office_phone, $service_extra, $int_dias, $int_horas, $id_sub_category, $del_time,$service_extra_delete) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $mysqli->autocommit(FALSE);
        $query="";
        if($img == null){
        $query = "UPDATE `commerce` SET `name` = '".$name."', `id_category` = '".$id_category."', `id_sub_category` = '".$id_sub_category."', "
                . "`id_departament` = '".$id_departament."', `id_province` = '".$id_province."', `id_district` = '".$id_district."', "
                . "`address` = '".$address."', `description` = '".$description."', `facebook` = '".$facebook."', `web` = '".$web."', `phone` = '".$phone."', `email` = '".$email."', `office_phone` = '".$office_phone."' WHERE `commerce`.`id_commerce` =" . $id;
        } else {
            $query = "UPDATE `commerce` SET `name` = '".$name."', `id_category` = '".$id_category."', `id_sub_category` = '".$id_sub_category."', "
                . "`id_departament` = '".$id_departament."', `img` ='".$img."' ,`id_province` = '".$id_province."', `id_district` = '".$id_district."', "
                . "`address` = '".$address."', `description` = '".$description."', `facebook` = '".$facebook."', `web` = '".$web."', `phone` = '".$phone."', `email` = '".$email."', `office_phone` = '".$office_phone."' WHERE `commerce`.`id_commerce` =" . $id;
        }

        if ($mysqli->query($query) == TRUE) {
            if (!empty($int_dias)) {
                $dia = explode(",", $int_dias);
                $hora = explode(",", $int_horas);
                for ($i = 0; $i < count($hora); $i++) {
                    $query_time = "INSERT INTO `time_table` (`id_timetable`, `dia`, `hora`, `id_commerce`, `update_timetable`, `create_timetable`,`delete_time_table`) VALUES (NULL, '" . $dia[$i] . "', '" . $hora[$i] . "', '" . $id . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'0')";
                    if (!$mysqli->query($query_time)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            if (!empty($del_time)) {
                $time = explode(",", $del_time);
                for ($i = 0; $i < count($time); $i++) {
                    $query_delete_time="UPDATE `time_table` SET `delete_time_table` = '1' WHERE `time_table`.`id_timetable` = ".$time[$i];
                    if (!$mysqli->query($query_delete_time)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
                # code...
            }
            if (!empty($service_extra)) {
                $extra = explode(",", $service_extra);
                // echo $extra[0];
                for ($index = 0; $index < count($extra); $index++) {
                    $query_extra = "INSERT INTO `extra_service` (`id_extra_service`, `service`, `id_commerce`, `update_service`, `create_service`,`delete_service_extra`) VALUES (NULL, '" . $extra[$index] . "', '" . $id . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,0)";
                    if (!$mysqli->query($query_extra)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }

            if (!empty($service_extra_delete)) {
                $extra_dele = explode(",", $service_extra_delete);
                // echo $extra[0];
                for ($in = 0; $in < count($extra_dele); $in++) {
                    $query_extra_dele = "UPDATE `extra_service` SET `delete_service_extra` = '1' WHERE `extra_service`.`id_extra_service` = ".$extra_dele[$in];
                    if (!$mysqli->query($query_extra_dele)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            $mysqli->commit();
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->rollback();
            $mysqli->close();
            return FALSE;
        }
    }

}


?>