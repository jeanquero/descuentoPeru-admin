<?php

require_once "../config/config.php";

class FileDao {

    public function __construct() {
        
    }

    function regFile($id_person, $url,$type,$Description,$address,$title) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $mysqli->autocommit(FALSE);
        $query = "INSERT INTO `file` (`id_file`, `id_person`, `url`, `type`, `Description`, `id_department`, `update_file`, `create_file`,title) VALUES (NULL, '".$id_person."', '".$url."', '".$type."', '".$Description."', '".$address."', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$title."')";
        if ($mysqli->query($query)) {
            $mysqli->commit();
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->rollback();
            $mysqli->close();
            return FALSE;
        }
    }
    
    function file($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT * FROM `file` WHERE `id_file` = '" .$id  . "'";
        if ($result = $mysqli->query($query)) {
            $row = mysqli_fetch_row($result);
            if ($row != null) {
                return $row;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
        /* close connection */
    }
            
    function deleteFile($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "DELETE FROM `file` WHERE `file`.`id_file` = " . $id;
        if ($mysqli->query($query) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }
    
    function updateFile($description, $type, $idDepartment, $id, $title) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query_search_department = "UPDATE `file` SET `type` = '".$type."', `Description` = '".$description."', `id_department` = '".$idDepartment."', title ='".$title."' WHERE `file`.`id_file` = " . $id;
        if ($mysqli->query($query_search_department) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }
    
    
    function gallery($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr = null;

        $query = "SELECT * FROM `file` WHERE `id_person`=  ".$id;
        if ($result = $mysqli->query($query)) {
            /* free result set */
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                     $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }

        /* close connection */
    }

}

?>