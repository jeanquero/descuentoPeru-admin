<?php

require_once "../config/config.php";

class EventsDao {

    public function __construct() {
        
    }

    function regEvent($id_person, $name, $desde, $hasta,$dia, $varios_dias, $description,$id_departament, $id_province, $id_district, $address,$organize,$otro, $facebook, $web, $phone, $email, $office_phone, $type_event,$id_coin,$coin, $int_horas, $assistance_type,$img) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $mysqli->autocommit(FALSE);
        if ($varios_dias=="1"){
        $desde = $this->formatoFecha($desde);
        $hasta = $this->formatoFecha($hasta);
        $query = "INSERT INTO `events` (`id_event`, `id_person`, `name`, `description`, `several_days`, `from`, `to`, `day`, `id_departament`, `id_province`, `id_district`, `address`, `organize`, `other`, `facebook`, `web`, `phone`, `office_phone`, `email`, `event_type`, `id_coin`, `rode`, `assistance_type`,img, `update_event`, `create_event`, `delete_event`,active) "
                . "VALUES (NULL, '".$id_person."', '".$name."', '".$description."', '".$varios_dias."', '".$desde  ."', '".$hasta  ."', NULL, '".$id_departament."', '".$id_province."', '".$id_district."', '".$address."', '".$organize."', '".$otro."', '".$facebook."', '".$web."', '".$phone."', '".$office_phone."', '".$email."', '".$type_event."', '".$id_coin."', '".$coin."', '".$assistance_type."' ,'".$img."', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0','0');";
         
        } else {
         $dia = $this->formatoFecha($dia);
         $query = "INSERT INTO `events` (`id_event`, `id_person`, `name`, `description`, `several_days`, `from`, `to`, `day`, `id_departament`, `id_province`, `id_district`, `address`, `organize`, `other`, `facebook`, `web`, `phone`, `office_phone`, `email`, `event_type`, `id_coin`, `rode`, `assistance_type`,img, `update_event`, `create_event`, `delete_event`,active) "
                . "VALUES (NULL, '".$id_person."', '".$name."', '".$description."', '".$varios_dias."', NULL, NULL, '".$dia ."', '".$id_departament."', '".$id_province."', '".$id_district."', '".$address."', '".$organize."', '".$otro."', '".$facebook."', '".$web."', '".$phone."', '".$office_phone."', '".$email."', '".$type_event."', '".$id_coin."', '".$coin."', '".$assistance_type."','".$img."', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0','0');";
         
        }
        
        //echo $query;
        if ($mysqli->query($query)) {
            $id = $mysqli->insert_id;            
            if (!empty($int_horas)) {                
                $hora = explode(",", $int_horas);
                for ($i = 0; $i < count($hora); $i++) {
                    $query_time = "INSERT INTO `time_event` (`id_time_event`, `hour`, `id_event`, `update_time_event`, `create_time_event`,`delete_time_event`)"
                            . " VALUES (NULL, '".$hora[$i]."', '".$id."', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,0)";
                    if (!$mysqli->query($query_time)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            $mysqli->commit();
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->rollback();
            $mysqli->close();
            return FALSE;
        }
    }

    function misEvents($id_person) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();


        $query = "SELECT * FROM `events` "
                . " WHERE `events`.delete_event= 0 AND `events`.`id_person`=  " . $id_person;
        if ($result = $mysqli->query($query)) {


            /* free result set */
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                    $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }

    function deleteEvents($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "UPDATE `events` SET `delete_event` = '1' WHERE `events`.`id_event`  = " . $id;
        echo  $query ;
        if ($mysqli->query($query) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }

    function activeEvents($id,$active) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "UPDATE `events` SET `active` = '".$active."' WHERE `events`.`id_event`  = " . $id;
        //echo  $query ;
        if ($mysqli->query($query) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }

    function buscarEvents($id_event) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT ev.id_event,ev.id_person,ev.name,ev.address,ev.img,ev.description,ev.facebook,ev.web,ev.phone,ev.email,ev.office_phone,d.name_department,d.id_department,p.name_province,p.id_province, di.id_district, di.name_district,ev.several_days,ev.from,ev.to,ev.day,ev.organize,ev.other,ev.event_type,ev.id_coin,ev.rode,ev.assistance_type,ev.img,ev.active FROM events ev JOIN department d ON d.id_department=ev.id_departament JOIN province p ON p.id_province=ev.id_province JOIN district di ON di.id_district = ev.id_district  WHERE ev.id_event=" . $id_event;
        // echo $query;
        if ($result = $mysqli->query($query)) {
            $row = $result->fetch_object();
            if ($row != null) {
                $mysqli->close();
                return $row;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }

    function buscarTime($id_event) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT * FROM `time_event` WHERE delete_time_event= 0 AND id_event=" . $id_event;
        // echo $query;
        if ($result = $mysqli->query($query)) {
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                    $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }

    function updateEvent($id, $name, $desde, $hasta,$dia, $varios_dias, $description,$id_departament, $id_province, $id_district, $address,$organize,$otro, $facebook, $web, $phone, $email, $office_phone, $type_event,$id_coin,$coin, $int_horas, $assistance_type,$img,$del_time) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $mysqli->autocommit(FALSE);
        $query = "";
        if ($img == null) {
            if ($varios_dias=="1"){
        $desde = $this->formatoFecha($desde);
        $hasta = $this->formatoFecha($hasta);
            $query = "UPDATE `events` SET `name` = '".$name."', `description` = '".$description."', `several_days` = '".$varios_dias."', `from` = '".$desde."', `to` = '".$hasta."' `id_departament` = '".$id_departament."', `id_province` = '".$id_province."', `id_district` = '".$id_district."', `address` = '".$address."', `organize` = '".$organize."', `other` = '".$otro."', `facebook` = '" . $facebook . "', `web` = '" . $web . "', `phone` = '" . $phone . "', `email` = '" . $email . "', `office_phone` = '" . $office_phone . "', `event_type` = '".$type_event."', `id_coin` = '".$id_coin."', `rode` = '".$coin."', `assistance_type` = '".$assistance_type."' WHERE `events`.`id_event` = " . $id;
         }else {
            $dia = $this->formatoFecha($dia);

            $query = "UPDATE `events` SET `name` = '".$name."', `description` = '".$description."', `several_days` = '".$varios_dias."', `day` = '".$dia."', `id_departament` = '".$id_departament."', `id_province` = '".$id_province."', `id_district` = '".$id_district."', `address` = '".$address."', `organize` = '".$organize."', `other` = '".$otro."', `facebook` = '" . $facebook . "', `web` = '" . $web . "', `phone` = '" . $phone . "', `email` = '" . $email . "', `office_phone` = '" . $office_phone . "', `event_type` = '".$type_event."', `id_coin` = '".$id_coin."', `rode` = '".$coin."', `assistance_type` = '".$assistance_type."' WHERE `events`.`id_event` = " . $id;

         }           
        } else {
            if ($varios_dias=="1"){
        $desde = $this->formatoFecha($desde);
        $hasta = $this->formatoFecha($hasta);
            $query = "UPDATE `events` SET `name` = '".$name."', `description` = '".$description."', `several_days` = '".$varios_dias."', `from` = '".$desde."', `to` = '".$hasta."', `id_departament` = '".$id_departament."', `id_province` = '".$id_province."', `id_district` = '".$id_district."', `address` = '".$address."', `organize` = '".$organize."', `other` = '".$otro."', `facebook` = '" . $facebook . "', `web` = '" . $web . "', `phone` = '" . $phone . "', `email` = '" . $email . "', `office_phone` = '" . $office_phone . "', `event_type` = '".$type_event."', `id_coin` = '".$id_coin."', `rode` = '".$coin."', `assistance_type` = '".$assistance_type."', `img` = '".$img."' WHERE `events`.`id_event` = " . $id;
        }else {
             $dia = $this->formatoFecha($dia);
            $query = "UPDATE `events` SET `name` = '".$name."', `description` = '".$description."', `several_days` = '".$varios_dias."', `day` = '".$dia."', `id_departament` = '".$id_departament."', `id_province` = '".$id_province."', `id_district` = '".$id_district."', `address` = '".$address."', `organize` = '".$organize."', `other` = '".$otro."', `facebook` = '" . $facebook . "', `web` = '" . $web . "', `phone` = '" . $phone . "', `email` = '" . $email . "', `office_phone` = '" . $office_phone . "', `event_type` = '".$type_event."', `id_coin` = '".$id_coin."', `rode` = '".$coin."', `assistance_type` = '".$assistance_type."', `img` = '".$img."' WHERE `events`.`id_event` = " . $id;

        }
        }
        if ($mysqli->query($query) == TRUE) {
            if (!empty($int_horas)) {                
                $hora = explode(",", $int_horas);
                for ($i = 0; $i < count($hora); $i++) {
                    $query_time = "INSERT INTO `time_event` (`id_time_event`, `hour`, `id_event`, `update_time_event`, `create_time_event`,`delete_time_event`)"
                            . " VALUES (NULL, '".$hora[$i]."', '".$id."', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,0)";
                    if (!$mysqli->query($query_time)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            if (!empty($del_time)) {                
                $del = explode(",", $del_time);
                for ($i = 0; $i < count($del); $i++) {
                    $query_time_upd = "UPDATE `time_event` SET `delete_time_event` = '1' WHERE `time_event`.`id_time_event` = ".$del[$i];
                    if (!$mysqli->query($query_time_upd)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            $mysqli->commit();
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->rollback();
            $mysqli->close();
            return FALSE;
        }
    }

    function formatoFecha($param) {
        $date2 = DateTime::createFromFormat('d-m-Y', $param);
        return $date2->format('Y-m-d');
    }

}

?>