<?php
require_once "Department.php";
require_once "District.php";
require_once "Province.php";

class Person {

    private $id;
    private $firstName;
    private $lastName;
    private $email;
    private $sex;
    private $birthdate;
    private $phone;
    private $updatePerson;
    private $imgProfile;
    private $department;
    private $province;
    private $district;
    
    
    public function __construct(){
        }
   
   public function setId($id) {
        $this->id=$id;
    }

    public function getId() {
        return $this->id;
    }
    public function setFirstName($firstName) {
        $this->firstName=$firstName;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setLastName($lastName) {
        $this->lastName=$lastName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setEmail($email) {
        $this->email=$email;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setSex($sex) {
        $this->sex=$sex;
    }

    public function getSex() {
        return $this->sex;
    }

    public function setBirthdate($birthdate) {
        $this->birthdate=$birthdate;
    }

    public function getBirthdate() {
        return $this->birthdate;
    }

    public function setPhone($phone) {
        $this->phone=$phone;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setUpdatePerson($updatePerson) {
        $this->updatePerson=$updatePerson;
    }

    public function getUpdatePerson() {
        return $this->updatePerson;
    }
    public function setDepartment($department) {
        $this->department=$department;
    }

    public function getDepartment() {
        return $this->department;
    }
    
    public function setProvince($province) {
        $this->province=$province;
    }

    public function getProvince() {
        return $this->province;
    }
    
    public function setDistrict($district) {
        $this->district=$district;
    }

    public function getDistrict() {
        return $this->district;
    }
    
    public function setImgProfile($imgProfile) {
        $this->imgProfile=$imgProfile;
    }

    public function getImgProfile() {
        return $this->imgProfile;
    }
    
    
}
 