<?php
class Login {

    private $id;
    private $idPerson;
    private $login;
    private $password;
    private $updatePerson;
    
public function __construct(){
        }
   
   public function setId($id) {
        $this->id=$id;
    }

    public function getId() {
        return $this->id;
    }

    public function setIdPerson($idPerson) {
        $this->idPerson=$idPerson;
    }

    public function getIdPerson() {
        return $this->idPerson;
    }

    public function setLogin($login) {
        $this->login=$login;
    }

    public function getLogin() {
        return $this->login;
    }

    public function setPassword($password) {
        $this->password=$password;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setUpdatePerson($updatePerson) {
        $this->updatePerson=$updatePerson;
    }

    public function getUpdatePerson() {
        return $this->updatePerson;
    }
}
    ?>