<?php

include_once "../config/config.php";


class UpdatePersonDao {

    public function __construct() {
        
    }

    function updatePerson($firstName, $lastName, $sex, $birthdate, $phone, $department, $province, $district, $foto, $id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query_search_department = "UPDATE person SET first_name = '" . $firstName . "' , last_name = '" . $lastName . "' "
                . " , sex = '" . $sex . "' , birthdate = '" . $birthdate . "' , phone = '" . $phone . "' , id_department = " . $department . ", id_province = " . $province . " , id_district = " . $district . ", img_profile='" . $foto . "' ,update_person = CURRENT_TIMESTAMP WHERE person.id = " . $id;
        if ($mysqli->query($query_search_department) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }

    function updatePassword($pass, $id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "UPDATE `login` SET `password` = '" . md5($pass, TRUE) . " ' WHERE `login`.`id` = " . $id;
        if ($mysqli->query($query) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }

    function person($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query_search_person = " select `p`.`id` AS `id`,`p`.`first_name` AS `first_name`,`p`.`last_name` AS `last_name`,`p`.`email` AS `email`,`p`.`sex` AS `sex`,`p`.`birthdate` AS `birthdate`,`p`.`phone` AS `phone`,`p`.`update_person` AS `update_person`,`p`.`img_profile` AS `img_profile`,`p`.`id_department` AS `id_department`,`d`.`name_department` AS `name_department`,`p`.`id_province` AS `id_province`,`pr`.`name_province` AS `name_province`,`p`.`id_district` AS `id_district`,`di`.`name_district` AS `name_district` from (((`person` `p` left join `department` `d` on((`d`.`id_department` = `p`.`id_department`))) left join `province` `pr` on((`pr`.`id_province` = `p`.`id_province`))) left join `district` `di` on((`di`.`id_district` = `p`.`id_district`))) WHERE p.id = '" . $id . "'";
        if ($result = $mysqli->query($query_search_person)) {
            $row = mysqli_fetch_row($result);
            if ($row != null) {
                $mysqli->close();
                return $row;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
        /* close connection */
    }

    function validPassword($pass, $id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT * FROM  login WHERE  password = '" . md5($pass, TRUE) . "' AND `login`.`id` = " . $id;
        if ($result = $mysqli->query($query)) {
            $mysqli->close();
            $row_login = mysqli_fetch_row($result);
            /* free result set */
            if ($row_login != null) {
                $mysqli->close();
                return TRUE;
            } else {
                $mysqli->close();
                return FALSE;
            }
        } else {
            $mysqli->close();
            return FALSE;
        }
    }
    
    function updateNoti($campo,$check, $id_person, $id_noti) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "UPDATE `rel_person_notifications` SET `".$campo."` = '".$check."' WHERE id_notifications = ".$id_noti."  AND    id_person = ".$id_person;
        if ($mysqli->query($query) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }
    
    function notifications($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr[]= NULL;
        $query_search_notifications = "SELECT * FROM `rel_person_notifications` WHERE `id_person` = '" .$id."'";
        if ($result = $mysqli->query($query_search_notifications)) {
            /* free result set */
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                     $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
        /* close connection */
    }

}

?>