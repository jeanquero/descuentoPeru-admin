<?php

require_once "../config/config.php";

class OfertasDao {

    public function __construct() {
        
    }

    function regOferta($id_person, $name, $desde, $hasta, $aplica, $img, $description, $service_extra, $int_dias, $int_horas, $restriccion,$id_commerce) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $mysqli->autocommit(FALSE);
        $desde = $this->formatoFecha($desde);
        $hasta = $this->formatoFecha($hasta);
        $query = "INSERT INTO `ofertas` (`id_oferta`, `name`, `img`, `description`, `from`, `to`,  `aplica_horarios`, `update_oferta`, `create_oferta`, `id_person`,id_commerce,active) "
                . "VALUES (NULL, '" . $name . "', '" . $img . "', '" . $description . "', '" . $desde . "', '" . $hasta . "',  '" . $aplica . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" . $id_person . "', '".$id_commerce."','0');";
        // echo $query;
        if ($mysqli->query($query)) {
            $id = $mysqli->insert_id;
            if (!empty($service_extra)) {
                $extra = explode(",", $service_extra);
                // echo $extra[0];
                for ($index = 0; $index < count($extra); $index++) {
                    $query_extra = "INSERT INTO `extra_restricciones` (`id_extra_restri`, `id_oferta`, `restriccion`, `update_extra_restri`, `create_extra_restri`, `delete_extra_restri`) "
                            . "VALUES (NULL, '" . $id . "', '" . $extra[$index] . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0');";
                    if (!$mysqli->query($query_extra)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }

            if (!empty($restriccion)) {
                $restri = explode("|", $restriccion);
                for ($i = 0; $i < count($restri); $i++) {
                    $query_restri = "INSERT INTO `rel_ofertas_restricciones` (`id_rel_ofer_restr`, `id_oferta`, `id_restriccion`, `update_rel`, `create_rel`, `delete_rel`) "
                            . "VALUES (NULL, '" . $id . "', '" . $restri[$i] . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0');";
                    if (!$mysqli->query($query_restri)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            if (!empty($int_dias)) {
                $dia = explode(",", $int_dias);
                $hora = explode(",", $int_horas);
                for ($i = 0; $i < count($hora); $i++) {
                    $query_time = "INSERT INTO `time_table` (`id_timetable`, `dia`, `hora`, `id_oferta`, `update_timetable`, `create_timetable`,`delete_time_table`) VALUES (NULL, '" . $dia[$i] . "', '" . $hora[$i] . "', '" . $id . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'0')";
                    if (!$mysqli->query($query_time)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            $mysqli->commit();
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->rollback();
            $mysqli->close();
            return FALSE;
        }
    }

    function misOfertas($id_person) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();


        $query = "SELECT `ofertas`.* , `commerce`.`img` AS img_commerce FROM `ofertas` "
                . "LEFT JOIN `commerce` ON `commerce`.`id_commerce`= `ofertas`.`id_commerce`"
                . " WHERE `ofertas`.delete_oferta= 0 AND `ofertas`.`id_person`=  " . $id_person;
        if ($result = $mysqli->query($query)) {


            /* free result set */
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                    $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }

    function deleteOfertas($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "UPDATE `ofertas` SET `delete_oferta` = '1' WHERE `ofertas`.`id_oferta`  = " . $id;
        if ($mysqli->query($query) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }

    function buscarOfertas($id_ofertas) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT `ofertas`.* , `commerce`.`img` AS img_commerce FROM `ofertas` "
                . "LEFT JOIN `commerce` ON `commerce`.`id_commerce`= `ofertas`.`id_commerce`"
                . " WHERE `ofertas`.delete_oferta= 0 AND `ofertas`.id_oferta =" . $id_ofertas;
        // echo $query;
        if ($result = $mysqli->query($query)) {
            $row = $result->fetch_object();
            if ($row != null) {
                $mysqli->close();
                return $row;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }

    function buscarTime($id_oferta) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT * FROM `time_table` WHERE `delete_time_table`= 0 AND  id_oferta=" . $id_oferta;
        // echo $query;
        if ($result = $mysqli->query($query)) {
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                    $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }

    function buscarExtra($id_oferta) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT * FROM `extra_restricciones`  WHERE `delete_extra_restri`= 0 AND  id_oferta=".$id_oferta;
        // echo $query;
        if ($result = $mysqli->query($query)) {
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                    $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
    }
    function updateOferta($id, $name, $desde, $hasta, $aplica, $img, $description, $service_extra, $int_dias, $int_horas, $restriccion,$id_commerce,$del_time,$service_extra_delete) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $mysqli->autocommit(FALSE);
        $desde = $this->formatoFecha($desde);
        $hasta = $this->formatoFecha($hasta);
        $query = "";
        if ($img == null) {
            $query = "UPDATE `ofertas` SET `name` = '".$name."', `img` = '".$img."', `description` = '".$description."', `from` = '".$desde."', `to` = '".$hasta."', `aplica_horarios` = '".$aplica."', `id_commerce` = '".$id_commerce."' WHERE `ofertas`.`id_oferta` = " . $id;
        } else {
            $query = "UPDATE `ofertas` SET `name` = '".$name."', `img` = '".$img."', `description` = '".$description."', `from` = '".$desde."', `to` = '".$hasta."', `aplica_horarios` = '".$aplica."', `id_commerce` = '".$id_commerce."' WHERE `ofertas`.`id_oferta` = " . $id;
        }
        if ($mysqli->query($query) == TRUE) {
            if (!empty($int_dias)) {                 
                $dia = explode(",", $int_dias);
                $hora = explode(",", $int_horas);
                if ($hora != null) {
                for ($i = 0; $i < count($hora); $i++) {
                    $query_time = "INSERT INTO `time_table` (`id_timetable`, `dia`, `hora`, `id_oferta`, `update_timetable`, `create_timetable`,`delete_time_table`) VALUES (NULL, '" . $dia[$i] . "', '" . $hora[$i] . "', '" . $id . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'0')";
                    if (!$mysqli->query($query_time)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            }
            if (!empty($service_extra)) {
                $extra = explode(",", $service_extra);
                 if ($extra != null) {
                for ($index = 0; $index < count($extra); $index++) {
                    $query_extra = "INSERT INTO `extra_restricciones` (`id_extra_restri`, `id_oferta`, `restriccion`, `update_extra_restri`, `create_extra_restri`, `delete_extra_restri`) "
                            . "VALUES (NULL, '" . $id . "', '" . $extra[$index] . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0');";
                    if (!$mysqli->query($query_extra)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            }

            if (!empty($del_time)) {
                $time = explode(",", $del_time);
                if ($time != null) {
                 for ($i = 0; $i < count($time); $i++) {
                    $query_delete_time="UPDATE `time_table` SET `delete_time_table` = '1' WHERE `time_table`.`id_timetable` = ".$time[$i];
                    if (!$mysqli->query($query_delete_time)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
                # code...
            }

            if (!empty($service_extra_delete)) {
                $extra_dele = explode(",", $service_extra_delete);
                if ($extra_dele != null) {
                for ($in = 0; $in < count($extra_dele); $in++) {
                    $query_extra_dele = "UPDATE `extra_restricciones` SET `delete_extra_restri` = '1' WHERE `extra_restricciones`.`id_extra_restri` = ".$extra_dele[$in];
                    if (!$mysqli->query($query_extra_dele)) {
                        $mysqli->rollback();
                        $mysqli->close();
                        return FALSE;
                    }
                }
            }
            }
            $mysqli->commit();
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->rollback();
            $mysqli->close();
            return FALSE;
        }
    }

    function formatoFecha($param) {
        $date2 = DateTime::createFromFormat('d-m-Y', $param);
        return $date2->format('Y-m-d');
    }

    function activeOferta($id,$active) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "UPDATE `ofertas` SET `active` = '".$active."' WHERE `ofertas`.`id_oferta`  = " . $id;
        //echo  $query ;
        if ($mysqli->query($query) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }

}

?>