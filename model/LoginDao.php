<?php

require_once "config/config.php";
require_once "entity/Person.php";
require_once "entity/Login.php";

class LoginDao {

    public function __construct() {
        
    }

    function login($login) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();


        $query_search_login = "SELECT * FROM  login WHERE login = '" . $login->getLogin() . "' AND password = '" . md5($login->getPassword(), TRUE) . "' AND block = 0";
        if ($result_login = $mysqli->query($query_search_login)) {
            $row_login = mysqli_fetch_row($result_login);

            /* free result set */
            if ($row_login != null) {
                $mysqli->close();
                return $row_login;
            } else {
                $query_search_by_email = "SELECT login.* FROM `login`, person WHERE login.id_person = person.id AND person.email='" . $login->getLogin() . "' AND login.password = '" . md5($login->getPassword(), TRUE) . "' AND block = 0";
                if ($result_login_by_email = $mysqli->query($query_search_by_email)) {
                    $row_login_by_email = mysqli_fetch_row($result_login_by_email);
                    if ($row_login_by_email != null) {
                        $mysqli->close();
                        return $row_login_by_email;
                    } else {
                        $mysqli->close();
                        return null;
                    }
                } else {
                    $mysqli->close();
                    return null;
                }
            }
        } else {
            $mysqli->close();
            return null;
        }

        /* close connection */
    }

    function person($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query_search_person = " select `p`.`id` AS `id`,`p`.`first_name` AS `first_name`,`p`.`last_name` AS `last_name`,`p`.`email` AS `email`,`p`.`sex` AS `sex`,`p`.`birthdate` AS `birthdate`,`p`.`phone` AS `phone`,`p`.`update_person` AS `update_person`,`p`.`img_profile` AS `img_profile`,`p`.`id_department` AS `id_department`,`d`.`name_department` AS `name_department`,`p`.`id_province` AS `id_province`,`pr`.`name_province` AS `name_province`,`p`.`id_district` AS `id_district`,`di`.`name_district` AS `name_district` from (((`person` `p` left join `department` `d` on((`d`.`id_department` = `p`.`id_department`))) left join `province` `pr` on((`pr`.`id_province` = `p`.`id_province`))) left join `district` `di` on((`di`.`id_district` = `p`.`id_district`))) WHERE p.id = '" . $id . "'";
        if ($result = $mysqli->query($query_search_person)) {
            $row = mysqli_fetch_row($result);
            if ($row != null) {
                return $row;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
        /* close connection */
    }

    function notifications($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr[] = NULL;
        $query_search_notifications = "SELECT * FROM `rel_person_notifications` WHERE `id_person` = '" . $id . "'";
        if ($result = $mysqli->query($query_search_notifications)) {
            /* free result set */
            if ($result != null) {
                while ($row = $result->fetch_object()) {
                    $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
        /* close connection */
    }
    
    function rol($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr[] = null;

        $query = "SELECT * FROM `rel_rol_login` WHERE id_login = ".$id;
        if ($result = $mysqli->query($query)) {


            /* free result set */
            if ($result!= null) {
                $row = $result->fetch_object();
                $mysqli->close();
                return $row;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }

        /* close connection */
    }

}

?>