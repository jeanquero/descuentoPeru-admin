<?php

include_once "../config/config.php";

class EmailDao {

    public function __construct() {
        
    }

    function saveEmail($id_person_shipping, $id_person_receiving, $message) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr[] = null;
        $query = "INSERT INTO `chat` (`id_char`, `id_person_shipping`, `id_person_receiving`, `message`, `read`, `delete_chat`, `update_chat`, `create_chat`) VALUES (NULL, '" . $id_person_shipping . "', '" . $id_person_receiving . "', '" . $message . "', '0', '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);";
        if ($mysqli->query($query)) {
            $mysqli->close();
            return 1;
        } else {
            $mysqli->close();
            return 2;
        }

        /* close connection */
    }
    function mesages($id_person) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr[]=null;

        $query = "SELECT c.id_char, c.`id_person_shipping`,c.id_person_receiving, c.message,c.read,envia.first_name,envia.last_name, envia.img_profile FROM chat c JOIN person as envia ON envia.id = c.id_person_shipping WHERE c.id_person_receiving=".$id_person." AND `delete_chat`=0";
        if ($result = $mysqli->query($query)) {


            /* free result set */
            if ($result != null) {
                $i=0;
                while ($row = $result->fetch_object()) {
                     $group_arr[$i++] = $row;
                     
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }

        /* close connection */
    }
    function enviados($id_person_envia, $id_person_resive){
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr[]=null;
        $query="SELECT c.id_char, c.`id_person_shipping`,c.id_person_receiving, c.message,c.read,c.create_chat,envia.first_name,envia.last_name, envia.img_profile FROM chat c JOIN person as envia ON envia.id = c.id_person_shipping WHERE c.id_person_receiving=".$id_person_resive." AND c.id_person_shipping =".$id_person_envia." AND `delete_chat`=0"
             . " UNION ALL"
             . " SELECT c.id_char, c.`id_person_shipping`,c.id_person_receiving, c.message,c.read,c.create_chat,envia.first_name,envia.last_name, envia.img_profile FROM chat c JOIN person as envia ON envia.id = c.id_person_shipping WHERE c.id_person_receiving=".$id_person_envia." AND c.id_person_shipping =".$id_person_resive." AND `delete_chat`=0 ";
    if ($result = $mysqli->query($query)) {


            /* free result set */
            if ($result != null) {
                $i=0;
                while ($row = $result->fetch_object()) {
                     $group_arr[$i++] = $row;
                     
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
        
        
    }
    function person($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query_search_person = " select `p`.`id` AS `id`,`p`.`first_name` AS `first_name`,`p`.`last_name` AS `last_name`,`p`.`email` AS `email`,`p`.`sex` AS `sex`,`p`.`birthdate` AS `birthdate`,`p`.`phone` AS `phone`,`p`.`update_person` AS `update_person`,`p`.`img_profile` AS `img_profile`,`p`.`id_department` AS `id_department`,`d`.`name_department` AS `name_department`,`p`.`id_province` AS `id_province`,`pr`.`name_province` AS `name_province`,`p`.`id_district` AS `id_district`,`di`.`name_district` AS `name_district` from (((`person` `p` left join `department` `d` on((`d`.`id_department` = `p`.`id_department`))) left join `province` `pr` on((`pr`.`id_province` = `p`.`id_province`))) left join `district` `di` on((`di`.`id_district` = `p`.`id_district`))) WHERE p.id = '" . $id . "'";
        if ($result = $mysqli->query($query_search_person)) {
            $row = mysqli_fetch_row($result);
            if ($row != null) {
                return $row;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }
        /* close connection */
    }
}

?>