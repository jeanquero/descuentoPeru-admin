<?php

require_once "../config/config.php";
require_once "entity/Person.php";
require_once "entity/Login.php";

class RegistryDao {

    public function __construct() {
        
    }

    function regPerson($person, $login) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $mysqli->autocommit(FALSE);
        $query_search_person = "SELECT email FROM person WHERE email = '" . $person->getEmail() . "'";
        if ($result = $mysqli->query($query_search_person)) {
            $row = mysqli_fetch_row($result);
            if ($row == null) {
                $query_search_login = "SELECT login FROM  login WHERE login = '" . $login->getLogin() . "'";

                if ($result_login = $mysqli->query($query_search_login)) {
                    $row_login = mysqli_fetch_row($result_login);

                    /* free result set */
                    if ($row_login == null) {

                        $mysqli->query($query_search_person);
                        $query = "INSERT INTO person VALUES (NULL, '" . $person->getFirstName() . "' , '" . $person->getLastName() . "' , '" . $person->getEmail() . "' , '" . $person->getSex() . "' , '" . $person->getBirthdate() . "' ,NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,0,0,0,NULL)";
                        if ($mysqli->query($query)) {
                            $id = $mysqli->insert_id;
                            $query_login = "INSERT INTO login VALUES (NULL, '" . $id . "', '" . $login->getLogin() . "', '" . md5($login->getPassword(), TRUE) . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,0)";
                            if ($mysqli->query($query_login)) {
                                $query_noti_1 = "INSERT INTO `rel_person_notifications` (`id_rel_per_not`, `id_person`, `id_notifications`, `email`, `alert`, `update_rel`, `create_rel`) VALUES (NULL, '". $id ."', '1', '0', '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
                                $query_noti_2 = " INSERT INTO `rel_person_notifications` (`id_rel_per_not`, `id_person`, `id_notifications`, `email`, `alert`, `update_rel`, `create_rel`) VALUES (NULL, '". $id ."', '2', '0', '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
                                $query_noti_3 = " INSERT INTO `rel_person_notifications` (`id_rel_per_not`, `id_person`, `id_notifications`, `email`, `alert`, `update_rel`, `create_rel`) VALUES (NULL, '". $id ."', '3', '0', '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
                                $query_rol = "INSERT INTO  `rel_rol_login` (`id_rel_rol_login`, `id_login`, `id_rol`, `update_rel_rol_login`, `created_rel_rol_login`) VALUES  (NULL, '".$id."', '2', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);";
                                if ($mysqli->multi_query($query_noti_1) && $mysqli->multi_query($query_noti_2) && $mysqli->multi_query($query_noti_3) && $mysqli->multi_query($query_rol)) {
                                    $mysqli->commit();
                                    $mysqli->close();
                                    return 1;
                                } else {
                                    echo "Error: " . $sql . "<br>" . $mysqli->error;
                                    $mysqli->rollback();
                                    $mysqli->close();
                                    return 2;
                                }
                            } else {

                                $mysqli->rollback();
                                $mysqli->close();
                                return 2;
                            }
                        } else {

                            $mysqli->rollback();
                            $mysqli->close();
                            return 2;
                        }
                    } else {
                        $mysqli->close();
                        return 3;
                    }
                } else {
                    $mysqli->close();
                    return 3;
                }
            } else {
                $mysqli->close();
                return 4;
            }
        } else {
            $mysqli->close();
            return 4;
        }
        /* close connection */
    }

    function resetEmail($email) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "SELECT email, id FROM person WHERE email = '" . $email . "'";
        if ($result = $mysqli->query($query)) {
            $row = mysqli_fetch_row($result);
            if ($row != null) {
                $mysqli->close();
                return $row;
               
            } else {
                $mysqli->close();
                return FALSE;
            }
        } else {
            $mysqli->close();
            return FALSE;
        }
        /* close connection */
    }
    function updatePassword($pass, $id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $query = "UPDATE `login` SET `password` = '" . md5($pass, TRUE) . " ' WHERE `login`.`id_person` = " . $id;
        if ($mysqli->query($query) == TRUE) {
            $mysqli->close();
            return TRUE;
        } else {
            $mysqli->close();
            return FALSE;
        }
    }
}

?>