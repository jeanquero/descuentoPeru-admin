<?php

include_once "../config/config.php";


class AddressesDao {

    public function __construct() {
        
    }

    function department() {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr[] = null;

        $query_search_department = "SELECT * FROM  department ";
        if ($result_department = $mysqli->query($query_search_department)) {


            /* free result set */
            if ($result_department != null) {
                while ($row = $result_department->fetch_object()) {
                     $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }

        /* close connection */
    }

    function province($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr[] = null;

        $query_search_province = "SELECT * FROM  province WHERE id_department=  ".$id;
        if ($result_province = $mysqli->query($query_search_province)) {


            /* free result set */
            if ($result_province != null) {
                while ($row = $result_province->fetch_object()) {
                     $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }

        /* close connection */
    }
    
    function district($id) {
        $config = new Config();
        $config->getConexion();
        $mysqli = $config->getCon();
        $group_arr[] = null;

        $query_search_district = "SELECT * FROM district  WHERE id_province=  ".$id;
        if ($result_district = $mysqli->query($query_search_district)) {


            /* free result set */
            if ($result_district != null) {
                while ($row = $result_district->fetch_object()) {
                     $group_arr[] = $row;
                }
                $mysqli->close();
                return $group_arr;
            } else {
                $mysqli->close();
                return null;
            }
        } else {
            $mysqli->close();
            return null;
        }

        /* close connection */
    }


}

?>