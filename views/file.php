<?php

include_once "../controller/FileController.php";
require_once "../model/entity/Person.php";
session_start();

function updatePerson() {
    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    $path = '../upload/'; // upload directory
    if (!empty($_FILES['foto']["name"])) {
        $img = $_FILES['foto']['name'];
        $tmp = $_FILES['foto']['tmp_name'];
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        $final_image = rand(1000, 1000000) . $img;
// check's valid format
        if (in_array($ext, $valid_extensions)) {
            $path = $path . strtolower($final_image);
            if (move_uploaded_file($tmp, $path)) {                
                $fileController = new FileController();
                $person = unserialize($_SESSION['person']);                
                $res = $fileController->regFile($person->getId(),$path,$_POST["type"], $_POST["descripcion"], $_POST["department"],$_POST["title"]);
                if ($res == TRUE) {
                    echo 0;
                } else {
                    echo 1;
                }
            } else {
                echo 2;
            }
        } else {
            echo 3;
        }
    } 
}



if (!empty($_FILES['foto'])) {
     updatePerson();
   
}






