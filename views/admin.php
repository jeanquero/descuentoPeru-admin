<?php

include_once "../controller/AdminController.php";

function users() {
    $adminController = new AdminController();
    $users = $adminController->users();
    return $users;
}

function registryPerson() {
    $registry= new AdminController();
    $res = $registry->registryPerson(trim($_POST["firstName"]), trim($_POST["lastName"]), $_POST["email"], $_POST["sex"], $_POST["birthdate"], $_POST["login"], $_POST["password"], $_POST["phone"], $_POST["rol"]);
    echo $res;
    
}

if (isset($_POST) && $_POST["block"] == "true") {
    $adminController = new AdminController();
    $res = $users = $adminController->blockUser($_POST["id"], $_POST["isblock"]);
    if ($res == TRUE) {
        echo '1';
    } else {
        echo '0';
    }
}
if (isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["email"]) && isset($_POST["sex"]) && isset($_POST["birthdate"]) && isset($_POST["login"]) && isset($_POST["password"])) {
    //var_dump($_POST);
    registryPerson();
}
//var_dump($_POST);

