<?php

require_once "../controller/RegistryController.php";
require_once "../mail/mail.php";

function registryPerson() {


    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["email"]) && isset($_POST["sex"]) && isset($_POST["birthdate"]) && isset($_POST["login"]) && isset($_POST["password"])) {
            $registryController = new RegistryController();
            $res = $registryController->registryPerson(trim($_POST["firstName"]), trim($_POST["lastName"]), $_POST["email"], $_POST["sex"], $_POST["birthdate"], $_POST["login"], $_POST["password"]);
            if ($res == 1) {
                header("location: registry_success.php");
                exit;
            } elseif ($res == 3) {
                echo "<script >$.Notification.notify('error','top center','Usuario Invalido', 'El Usuario ya se Encuentra Registado.')</script>";
            } elseif ($res == 4) {
                echo "<script >$.Notification.notify('error','top center','Email Invalido', 'El Email ya se Encuentra Registado.')</script>";
            }
        } else {
            echo "<script >$.Notification.notify('error','top center','Campos son requerido', 'Todos los campos son requerido.')</script>";
        }
    }
}

function resetPass() {


    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (isset($_POST["email"])) {
            $registryController = new RegistryController();
            $res = $registryController->resetPass($_POST["email"]);
            if ($res) {
                $pass = rand(10000000, 100000000);
                $resPass = $registryController->updatePassword($pass, $res[1]);
                if ($resPass) {
                    if (sent($pass) == TRUE) {
                        echo "<script >$.Notification.notify('success','top center','La nueva clave fue enviada a su correo', 'La nueva clave fue enviada a su correo.')</script>";
                    } else {
                        echo "<script >$.Notification.notify('error','top center','Error enviando correo', 'Error enviando correo.')</script>";
                    }
                } else {
                    echo "<script >$.Notification.notify('error','top center','Error de Sistema', 'Error de Sistema.')</script>";
                }
            } else {
                echo "<script >$.Notification.notify('error','top center','Email Invalido', 'El Email no se Encuentra Registado.')</script>";
            }
        } else {
            echo "<script >$.Notification.notify('error','top center','Campos son requerido', 'Todos los campos son requerido.')</script>";
        }
    }
}

?>