<?php

include_once "../controller/FileController.php";

session_start();

function gallery() {
    $fileController = new FileController();
    $person = unserialize($_SESSION['person']);
    return $fileController->gallery($person->getId());
}

if (!empty($_POST["id"])) {
    $fileController = new FileController();
    $res = $fileController->deleteFile($_POST["id"]);
    if ($res) {
        echo '1';
    } else {
        echo '0';
    }
}