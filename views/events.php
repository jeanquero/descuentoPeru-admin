<?php

include_once "../controller/EventsController.php";
require_once "../model/entity/Person.php";
session_start();





if ($_POST["save"] == "true") {
    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    $path = '../upload/negocio/'; // upload directory
    if (!empty($_FILES['file']["name"])) {

        $img = $_FILES['file']['name'];
        $tmp = $_FILES['file']['tmp_name'];
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        $final_image = rand(1000, 1000000) . $img;
// check's valid format
        if (in_array($ext, $valid_extensions)){
            $path = $path . strtolower($final_image);
            if (move_uploaded_file($tmp, $path)) {
    $eventsController = new EventsController();
    $person = unserialize($_SESSION['person']);
      
    $res = $eventsController->regEvent($person->getId(), $_POST["name"], $_POST["desde"], $_POST["hasta"],$_POST["dia"] ,$_POST["varios_dias"] ,$_POST["description"],$_POST["department"], $_POST["province"], $_POST["district"], $_POST["addrees"], $_POST["organizador"], $_POST["otro"],$_POST["face"], $_POST["web"], $_POST["cel"], $_POST["email"], $_POST["ofi"], $_POST["type_event"],$_POST["id_coin"], $_POST["coin"], $_POST["int_horas"], $_POST["assistance_type"],$path);
    if ($res == TRUE) {
        echo 0;
    } else {
        echo 1;
    }
} else {
        echo 1;
    }
} else {
        echo 1;
    }
}
}

function misEvents() {
    $eventsController = new EventsController();
    $person = unserialize($_SESSION['person']);
    return $eventsController->misEvents($person->getId());
}

function deleteEvent($id) {
    $eventsController = new EventsController();
    return $eventsController->deleteEvents($id);
}

if (isset($_POST["even"]) && $_POST["even"] == "neg") {
    echo deleteEvent($_POST["id"]);
}

if (isset($_POST["acti"]) && $_POST["acti"] == "activar") {
    $eventsController = new EventsController();
    echo $eventsController->activeEvents($_POST["id"],$_POST["actibool"]);
}

function buscarEvents($id_event) {
    $eventsController = new EventsController();
    return $eventsController->buscarEvents($id_event);
}

function buscarTime($id_oferta) {
    $eventsController = new EventsController();
    return $eventsController->buscarTime($id_oferta);
}

if (isset($_POST["update"]) && $_POST["update"] == "true") {

    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    $path = '../upload/negocio/'; // upload directory
    if (!empty($_FILES['file']["name"])) {

        $img = $_FILES['file']['name'];
        $tmp = $_FILES['file']['tmp_name'];
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        $final_image = rand(1000, 1000000) . $img;
// check's valid format
        if (in_array($ext, $valid_extensions)) {
            $path = $path . strtolower($final_image);
            if (move_uploaded_file($tmp, $path)) {
                $eventsController = new EventsController();
                $res = $eventsController->updateNegocio($_POST["id_event"], $_POST["name"], $_POST["desde"], $_POST["hasta"],$_POST["dia"] ,$_POST["varios_dias"] ,$_POST["description"],$_POST["department"], $_POST["province"], $_POST["district"], $_POST["addrees"], $_POST["organizador"], $_POST["otro"],$_POST["face"], $_POST["web"], $_POST["cel"], $_POST["email"], $_POST["ofi"], $_POST["type_event"],$_POST["id_coin"], $_POST["coin"], $_POST["int_horas"], $_POST["assistance_type"],$path,$_POST["del_time"]);
                if ($res == TRUE) {
                    echo 0;
                } else {
                    echo 1;
                }
            } else {
                echo 2;
            }
        } else {
            echo 3;
        }
    } else {

        $eventsController = new EventsController();
        $res = $eventsController->updateEvent($_POST["id_event"], $_POST["name"], $_POST["desde"], $_POST["hasta"],$_POST["dia"] ,$_POST["varios_dias"] ,$_POST["description"],$_POST["department"], $_POST["province"], $_POST["district"], $_POST["addrees"], $_POST["organizador"], $_POST["otro"],$_POST["face"], $_POST["web"], $_POST["cel"], $_POST["email"], $_POST["ofi"], $_POST["type_event"],$_POST["id_coin"], $_POST["coin"], $_POST["int_horas"], $_POST["assistance_type"],null,$_POST["del_time"]);
        if ($res == TRUE) {
            echo 0;
        } else {
            echo 1;
        }
    }
}

function formatoFecha($param) {
    $date2 = DateTime::createFromFormat('Y-m-d H:i:s', $param);
    return $date2->format('d-m-Y');
}
