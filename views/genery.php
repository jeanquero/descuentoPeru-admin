<?php

include_once "../controller/GeneryController.php";

function category($id) {
    $generyController = new GeneryController();
    $category = $generyController->category();

    foreach ($category as $array) {
        if ($id == $array->id_category) {
            echo '<option value="' . $array->id_category . '" data-icon="' . $array->icons . '" selected  >' . $array->name . '</option>';
        } else {
            echo '<option value="' . $array->id_category . '" data-icon="' . $array->icons . '"  >' . $array->name . '</option>';
        }
    }
}

function rol($id) {
    $generyController = new GeneryController();
    $rol = $generyController->rol();

    foreach ($rol as $array) {
        if ($id == $array->id_rol) {
            echo '<option value="' . $array->id_rol . '"  selected  >' . $array->rol . '</option>';
        } else {
            echo '<option value="' . $array->id_rol . '"  >' . $array->rol . '</option>';
        }
    }
}

function sub_category($id) {
    $generyController = new GeneryController();
    $category = $generyController->sub_category();

    foreach ($category as $array) {
        if ($id == $array->id_sub_category) {
            echo '<option value="' . $array->id_sub_category . '" data-icon="' . $array->icons . '"  selected >' . $array->name . '</option>';
        } else {
            echo '<option value="' . $array->id_sub_category . '" data-icon="' . $array->icons . '"  >' . $array->name . '</option>';
        }
    }
}

if (!empty($_POST["term"])) {

    $autocomplete = array();
    $generyController = new GeneryController();
    $person = $generyController->person($_POST["term"]);
    foreach ($person as $array) {
        $autocomplete[$array->id] = array(
            'id' => $array->id,
            'name' => $array->name
        );
    }
    //var_dump($person);
    echo json_encode($autocomplete);
}

function restri() {
    $generyController = new GeneryController();
    return $generyController->restri();
}

function negocios($id) {
    session_start();
    $generyController = new GeneryController();
    $person = unserialize($_SESSION['person']);
    $negocios = $generyController->negocios($person->getId());
    foreach ($negocios as $array) {
        if ($id == $array->id_category) {
            echo '<option value="' . $array->id_commerce . '" selected  >' . $array->name . '</option>';
        } else {
            echo '<option value="' . $array->id_commerce . '"  >' . $array->name . '</option>';
        }
    }
}
 function formatDate($date){
    if ($date != "") {
$date2 = DateTime::createFromFormat('Y-m-d H:i:s', $date);
    return $date2->format('d-m-Y');
}
                }
