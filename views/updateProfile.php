<?php
include_once "../model/entity/Person.php";
include_once "../model/entity/Login.php";
include_once "../controller/UpdatePersonController.php";

session_start();

function updatePerson() {
    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    $path = '../upload/'; // upload directory
    if (!empty($_FILES['foto']["name"])) {
        $img = $_FILES['foto']['name'];
        $tmp = $_FILES['foto']['tmp_name'];
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        $final_image = rand(1000, 1000000) . $img;
// check's valid format
        if (in_array($ext, $valid_extensions)) {
            $path = $path . strtolower($final_image);
            if (move_uploaded_file($tmp, $path)) {
                $updatePersonController = new UpdatePersonController();
                $person = unserialize($_SESSION['person']);
                $res = $updatePersonController->updatePerson($_POST["firstName"], $_POST["lastName"], $_POST["sex"], $_POST["birthdate"], $_POST["phone"], $_POST["department"], $_POST["province"], $_POST["district"], $path, $person);
                if ($res == TRUE) {
                    echo 0;
                } else {
                    echo 1;
                }
            } else {
                echo 2;
            }
        } else {
            echo 3;
        }
    } else {
        $updatePersonController = new UpdatePersonController();
        $person = unserialize($_SESSION['person']);
        $res = $updatePersonController->updatePerson($_POST["firstName"], $_POST["lastName"], $_POST["sex"], $_POST["birthdate"], $_POST["phone"], $_POST["department"], $_POST["province"], $_POST["district"], null, $person);
        if ($res == TRUE) {
            echo 0;
        } else {
            echo 1;
        }
    }
}

if (!empty($_POST["firstName"])) {
    updatePerson();
}

if (!empty($_POST["change_pass"])){
    $updatePersonController = new UpdatePersonController();
    echo $updatePersonController->updatePassword($_POST["password"],$_POST["actual_password"]);
}

if (!empty($_POST["tipo"]) && !empty($_POST["id_noti"])){
    $person = unserialize($_SESSION['person']);    
    $updatePersonController = new UpdatePersonController();
    echo $updatePersonController->updateNoti($_POST["tipo"],$_POST["checked"],$person->getId(),$_POST["id_noti"]);
}




