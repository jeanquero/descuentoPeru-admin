<?php

include_once "../controller/AddressesController.php";

function department($id) {
    $addressesController = new AddressesController();
    $department = $addressesController->department();

    foreach ($department as $array) {
        if ($id == $array->id_department){
        echo '<option value="' . $array->id_department . '" selected>' . $array->name_department . '</option>';
        } else {
           echo '<option value="' . $array->id_department . '" >' . $array->name_department . '</option>' ;
        }
    }
}

function province($id) {
    $addressesController = new AddressesController();
    $province = $addressesController->province($id);
    foreach ($province as $array) {
        echo '<option value="' . $array->id_province . '">' . $array->name_province . '</option>';
    }
}

function district($id) {
    $addressesController = new AddressesController();
    $district = $addressesController->district($id);
    foreach ($district as $array) {
        echo '<option value="' . $array->id_district . '">' . $array->name_district . '</option>';
    }
}

function provinceSelect($id, $idSelect) {
    $addressesController = new AddressesController();
    $province = $addressesController->province($id);
    foreach ($province as $array) {
        if ($idSelect == $array->id_province) {
            echo '<option value="' . $array->id_province . '" selected>' . $array->name_province . '</option>';
        } else {
            echo '<option value="' . $array->id_province . '">' . $array->name_province . '</option>';
        }
    }
}

function districtSelect($id, $idSelect) {
    $addressesController = new AddressesController();
    $district = $addressesController->district($id);
    foreach ($district as $array) {
        if ($idSelect == $array->id_district) {
            echo '<option value="' . $array->id_district . '" selected>' . $array->name_district . '</option>';
        } else {
            echo '<option value="' . $array->id_district . '" >' . $array->name_district . '</option>';
        }
    }
}

if ($_GET["action"] == "department") {
    province($_GET["id"]);
}

if ($_GET["action"] == "province") {
    district($_GET["id"]);
}

