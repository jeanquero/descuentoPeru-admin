<?php

include_once "../controller/OfertasController.php";
require_once "../model/entity/Person.php";
session_start();

function regOferta() {
    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    $path = '../upload/negocio/'; // upload directory
    if (!empty($_FILES['file']["name"])) {

        $img = $_FILES['file']['name'];
        $tmp = $_FILES['file']['tmp_name'];
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        $final_image = rand(1000, 1000000) . $img;
// check's valid format
        if (in_array($ext, $valid_extensions)) {
            $path = $path . strtolower($final_image);
            if (move_uploaded_file($tmp, $path)) {
                $ofertasController = new OfertasController();
                $person = unserialize($_SESSION['person']);

                $res = $ofertasController->regOferta($person->getId(), $_POST["name"], $_POST["desde"], $_POST["hasta"], $_POST["aplica"], $path, $_POST["description"], $_POST["service_extra"], $_POST["int_dias"], $_POST["int_horas"], $_POST["restriccion"], $_POST["negocio"]);
                if ($res == TRUE) {
                    echo 0;
                } else {
                    echo 1;
                }
            } else {
                echo 2;
            }
        } else {
            echo 3;
        }
    }
}

if (isset($_FILES) && $_POST["save"] == "true") {
    regOferta();
}

if (empty($_FILES) && $_POST["save"] == "true") {
    //var_dump($_POST);
//var_dump($_FILES);
    $ofertasController = new OfertasController();
    $person = unserialize($_SESSION['person']);

    $res = $ofertasController->regOferta($person->getId(), $_POST["name"], $_POST["desde"], $_POST["hasta"], $_POST["aplica"], null, $_POST["description"], $_POST["service_extra"], $_POST["int_dias"], $_POST["int_horas"], $_POST["restriccion"], $_POST["negocio"]);
    if ($res == TRUE) {
        echo 0;
    } else {
        echo 1;
    }
}

function misOfertas() {
    $ofertasController = new OfertasController();
    $person = unserialize($_SESSION['person']);
    return $ofertasController->misOfertas($person->getId());
}

function deleteOfer($id) {
    $ofertasController = new OfertasController();
    return $ofertasController->deleteOfertas($id);
}

if (isset($_POST["even"]) && $_POST["even"] == "neg") {
    echo deleteOfer($_POST["id"]);
}

function buscarOfertas($id_ofertas) {
    $ofertasController = new OfertasController();
    return $ofertasController->buscarOfertas($id_ofertas);
}

function buscarTime($id_oferta) {
    $ofertasController = new OfertasController();
    return $ofertasController->buscarTime($id_oferta);
}

function buscarExtra($id_oferta) {
    $ofertasController = new OfertasController();
    return $ofertasController->buscarExtra($id_oferta);
}

if (isset($_POST["update"]) && $_POST["update"] == "true") {

    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    $path = '../upload/negocio/'; // upload directory
    if (!empty($_FILES['file']["name"])) {

        $img = $_FILES['file']['name'];
        $tmp = $_FILES['file']['tmp_name'];
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        $final_image = rand(1000, 1000000) . $img;
// check's valid format
        if (in_array($ext, $valid_extensions)) {
            $path = $path . strtolower($final_image);
            if (move_uploaded_file($tmp, $path)) {
                $ofertasController = new OfertasController();
                $res = $ofertasController->updateOferta($_POST["id_oferta"], $_POST["name"], $_POST["desde"], $_POST["hasta"], $_POST["aplica"], $path, $_POST["description"], $_POST["service_extra"], $_POST["int_dias"], $_POST["int_horas"], $_POST["restriccion"], $_POST["negocio"],$_POST["del_time"],$_POST["service_extra_delete"]);
                if ($res == TRUE) {
                    echo 0;
                } else {
                    echo 1;
                }
            } else {
                echo 2;
            }
        } else {
            echo 3;
        }
    } else {

        $ofertasController = new OfertasController();
        $res = $ofertasController->updateOferta($_POST["id_oferta"], $_POST["name"], $_POST["desde"], $_POST["hasta"], $_POST["aplica"], null, $_POST["description"], $_POST["service_extra"], $_POST["int_dias"], $_POST["int_horas"], $_POST["restriccion"], $_POST["negocio"],$_POST["del_time"],$_POST["service_extra_delete"]);
        if ($res == TRUE) {
            echo 0;
        } else {
            echo 1;
        }
    }
}

function formatoFecha($param) {
    $date2 = DateTime::createFromFormat('Y-m-d H:i:s', $param);
    return $date2->format('d-m-Y');
}

if (isset($_POST["acti"]) && $_POST["acti"] == "activar") {
    $ofertasController = new OfertasController();
    echo $ofertasController->activeOferta($_POST["id"],$_POST["actibool"]);
}


//var_dump($_POST);