<?php

include_once "../controller/NegocioController.php";
require_once "../model/entity/Person.php";
session_start();

function regNegocio() {
    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    $path = '../upload/negocio/'; // upload directory
    if (!empty($_FILES['file']["name"])) {

        $img = $_FILES['file']['name'];
        $tmp = $_FILES['file']['tmp_name'];
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        $final_image = rand(1000, 1000000) . $img;
// check's valid format
        if (in_array($ext, $valid_extensions)) {
            $path = $path . strtolower($final_image);
            if (move_uploaded_file($tmp, $path)) {
                $negocioController = new NegocioController();
                $person = unserialize($_SESSION['person']);

                $res = $negocioController->regNegocio($person->getId(), $_POST["name"], $_POST["category"], $_POST["department"], $_POST["province"], $_POST["district"], $_POST["addrees"], $path, $_POST["description"], $_POST["face"], $_POST["web"], $_POST["cel"], $_POST["email"], $_POST["ofi"], $_POST["service_extra"], $_POST["int_dias"], $_POST["int_horas"], $_POST["sub_category"]);
                if ($res == TRUE) {
                    echo 0;
                } else {
                    echo 1;
                }
            } else {
                echo 2;
            }
        } else {
            echo 3;
        }
    }
}

if (isset($_FILES) && $_POST["save"] == "true") {
    regNegocio();
}

function misNegocios() {
    $negocioController = new NegocioController();
    $person = unserialize($_SESSION['person']);
    return $negocioController->misNegocios($person->getId());
}

function deleteNeg($id) {
    $negocioController = new NegocioController();
    return $negocioController->deleteNegocio($id);
}

if (isset($_POST["even"]) && $_POST["even"] == "neg") {
    echo deleteNeg($_POST["id"]);
}

function buscarNegocio($id_comm) {
    $negocioController = new NegocioController();
    return $negocioController->buscarNegocio($id_comm);
}

function buscarTime($id_comm) {
    $negocioController = new NegocioController();
    return $negocioController->buscarTime($id_comm);
}

function buscarServiceExtra($id_comm) {
    $negocioController = new NegocioController();
    return $negocioController->buscarServiceExtra($id_comm);
}

if (isset($_POST["update"]) && $_POST["update"] == "true") {

    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    $path = '../upload/negocio/'; // upload directory
    if (!empty($_FILES['file']["name"])) {

        $img = $_FILES['file']['name'];
        $tmp = $_FILES['file']['tmp_name'];
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        $final_image = rand(1000, 1000000) . $img;
// check's valid format
        if (in_array($ext, $valid_extensions)) {
            $path = $path . strtolower($final_image);
            if (move_uploaded_file($tmp, $path)) {
                $negocioController = new NegocioController();
                $res = $negocioController->updateNegocio($_POST["id_com"], $_POST["name"], $_POST["category"], $_POST["department"], $_POST["province"], $_POST["district"], $_POST["addrees"], $path, $_POST["description"], $_POST["face"], $_POST["web"], $_POST["cel"], $_POST["email"], $_POST["ofi"], $_POST["service_extra"], $_POST["int_dias"], $_POST["int_horas"], $_POST["sub_category"],$_POST["del_time"],$_POST["service_extra_delete"]);
                if ($res == TRUE) {
                    echo 0;
                } else {
                    echo 1;
                }
            } else {
                echo 2;
            }
        } else {
            echo 3;
        }
    } else {

        $negocioController = new NegocioController();
        $res = $negocioController->updateNegocio($_POST["id_com"], $_POST["name"], $_POST["category"], $_POST["department"], $_POST["province"], $_POST["district"], $_POST["addrees"], null, $_POST["description"], $_POST["face"], $_POST["web"], $_POST["cel"], $_POST["email"], $_POST["ofi"], $_POST["service_extra"], $_POST["int_dias"], $_POST["int_horas"], $_POST["sub_category"],$_POST["del_time"],$_POST["service_extra_delete"]);
        if ($res == TRUE) {
            echo 0;
        } else {
            echo 1;
        }
    }
}



//var_dump($_POST);

//var_dump($_FILES);




