<?php
require_once "controller/LoginController.php";

function login() {
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["login"]) && isset($_POST["password"])) {
            $loginController = new LoginController();
            $res = $loginController->login($_POST["login"], $_POST["password"]);
            if ($res) {               
                header("location: html/index.php");             
                exit();
            } else {
                echo "<script >$.Notification.notify('error','top center','Usuario Invalido', 'El Usuario o el Password son incorrectos.')</script>";
            } 
        } else {
            echo "<script >$.Notification.notify('error','top center','Campos son requerido', 'Todos los campos son requerido.')</script>";
        }
    }
}

?>

