

<?php
ob_start();

require_once "./template/header.php";
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>


<?php
require_once "./template/menus.php";
require_once "../views/negocio.php";
$negocios = buscarNegocio($_GET["id"]);
$time = buscarTime($_GET["id"]);
?>			
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">





            <div class="card-box">



                <div class="row">
                    <div class="col-md-4">
                        <a href="init_negocio.php" class="btn btn-primary waves-effect waves-light" role="button">Volver</a>

                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img src="<?php echo $negocios->img ?>" class="img-responsive">
                            <div class="caption">
                                <h3>Descripci&oacute;n</h3>
                                <p>
                                    <?php echo $negocios->description ?>
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-4"><div class="col-md-12"><h3><?php echo $negocios->name ?></h3></div>
                            <div class="col-md-4"><h4><i class="ti-heart"></i><i class="ti-heart"></i><i class="ti-heart"></i><i class="ti-heart"></i><i class="ti-heart"></i> </h4></div>
                        </div>
                        <div class="col-md-4"></div>
                        <a href="mi_negocio_edit.php?id=<?php echo $_GET["id"];?>" class="btn btn-primary waves-effect waves-light" role="button">Modificar</a>
                        
                    </div>

                </div>
                
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div class="col-md-12"><i class="ti-location-pin"></i></div>
                        <div class="col-md-4"><h4><?php echo $negocios->address ?></h4></div>

                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12"><i class="ti-time"></i></div>
                        <?php foreach ($time as $value) {
                            
                        ?>
                        <div class="col-md-12"><h4>*<?php echo $value->dia ." ".$value->hora ?></h4></div>
                        <?php }?>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12"><i class="ti-mobile"></i></div>
                        <div class="col-md-12"><h4>*<?php echo $negocios->phone ?></h4></div>
                     
                        <div class="col-md-12"><h4>*<?php echo $negocios->office_phone ?></h4></div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div class="col-md-12"><i class="ti-gallery"></i></div>
                        <div class="col-md-4"><h4><?php echo $negocios->web ?></h4></div>

                    </div>
                    
                    <div class="col-md-3">
                        <div class="col-md-12"><i class="ti-facebook"></i></div>
                        <div class="col-md-4"><h4><?php echo $negocios->facebook ?></h4></div>

                    </div>
                    <div class="col-md-3 ">
                        <div class="col-md-12"><i class="ti-email"></i></div>
                        <div class="col-md-4"><h4><?php echo $negocios->email ?></h4></div>

                    </div>
                    
<div class="col-md-1"></div>
                </div>
            </div>







        </div> <!-- container -->

    </div> <!-- content -->



</div>


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<?php
require_once "./template/footer.php";
ob_end_flush();
?> 