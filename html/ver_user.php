

<?php
ob_start();

require_once "./template/header.php";
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>


<?php
require_once "./template/menus.php";
require_once "../views/admin.php";
 $users = users();

?>
			<!-- Left Sidebar End -->

			<!-- ============================================================== -->
			<!-- Start right Content here -->
			<!-- ============================================================== -->
			<div class="content-page">
				<!-- Start content -->
				<div class="content">
					<div class="container">

						



                        <div class="panel">
                            
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="m-b-30">
                                            <a href="crear_usuario.php" class="btn btn-default waves-effect waves-light" data-animation="push" data-plugin="custommodal" 
                           data-overlaySpeed="100" data-overlayColor="#36404a">Agregar Usuario <i class="fa fa-plus"></i></a>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="">
                                	<table class="table table-striped" id="datatable-editable">
	                                    <thead>
	                                        <tr>
	                                            <th>Nombres</th>
	                                            <th>Login</th>
	                                            <th>Rol</th>
                                                    <th>Estado</th>
                                                    <th>Acci&oacute;n</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
                                                <?php foreach ($users as $value) {
                                                    
                                                 ?>
	                                        <tr id="<?php echo $value->id."_".$value->block; ?>" class="gradeX">
                                                <td><?php echo $value->first_name." ".$value->last_name; ?></td>
	                                            <td><?php echo $value->login; ?>   </td>
	                                            <td><?php echo $value->rol; ?></td>
                                                    <td><?php echo $value->block == "0" ? "Activo" : "Bloqueado"; ?></td>
	                                            <td class="actions">
                                                        
	                                                <a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>
	                                                <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>
	                                              <!--  <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a> -->
	                                                <a href="#" class="on-default remove-row"><i class="fa <?php echo $value->block == "0" ? " fa-unlock" : "fa-lock"; ?>"></i></a>
	                                            </td>
	                                        </tr>
                                                <?php } ?>
	                                    </tbody>
	                                </table>
                                </div>
                            </div>
                            <!-- end: page -->

                        </div> <!-- end Panel -->
                        
                        
                        <!-- MODAL -->
            <div id="dialog" class="modal-block mfp-hide">
                <section class="panel panel-info panel-color">
                    <header class="panel-heading">
                        <h2 class="panel-title">Estás seguro?</h2>
                    </header>
                    <div class="panel-body">
                        <div class="modal-wrapper">
                            <div class="modal-text">
                                <p>¿Estás seguro de que deseas Bloquear este Usuario?</p>
                            </div>
                        </div>

                        <div class="row m-t-20">
                            <div class="col-md-12 text-right">
                                <button id="dialogConfirm" class="btn btn-primary waves-effect waves-light">Confirmar</button>
                                <button id="dialogCancel" class="btn btn-default waves-effect">Cancelar</button>
                            </div>
                        </div>
                    </div>
                    
                </section>
            </div>
            <!-- end Modal -->
                        

                    </div> <!-- container -->
                               
                </div> <!-- content -->

                

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <?php
require_once "./template/footer.php";
ob_end_flush();
?> 
	    <!-- Examples -->
	    <script src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
	    <script src="assets/plugins/jquery-datatables-editable/jquery.dataTables.js"></script> 
	    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
	    <script src="assets/plugins/tiny-editable/mindmup-editabletable.js"></script>
	    <script src="assets/plugins/tiny-editable/numeric-input-example.js"></script>
	    
	    
	    <script src="assets/pages/datatables.editable.init.js"></script>
	    
	    <script>
			$('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
			
		</script>
	
	</body>
</html>