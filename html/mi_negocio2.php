<?php
ob_start();
require_once "../views/addresses.php";
require_once "../views/genery.php";
require_once "./template/header.php";
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>
<link type="text/css" href="assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="assets/plugins/bootstrapvalidator/src/css/bootstrapValidator.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/select2/select2.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
<!-- ========== Left Sidebar Start ========== -->   
<link rel="stylesheet" type="text/css" href="assets/plugins/jquery.steps/demo/css/jquery.steps.css" />
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/style.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/jquery.fileupload.css">
<?php
require_once "./template/menus.php";
?>
<!-- Left Sidebar End -->

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">








            <!-- Wizard with Validation -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box ">
                        <h4 class="m-t-0 header-title"><b>Registro de Negocio</b></h4>


                        <form id="wizard-validation-form" action="#">
                            <div>
                                <h3>Datos Principales</h3>
                                <section>
                                    <div class="form-group clearfix">
                                        <label class="col-lg-2 control-label " for="name">Nombre de la Empresa </label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" required id="name" name="name" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label class="col-lg-2 control-label " for="category"> Categoria</label>
                                        <div class="col-lg-10">
                                            <select class="selectpicker m-b-0" required  data-style="btn-white" id="category" name="category">
                                                <?php category(); ?>
                                            </select>
                                        </div>
                                    </div>


                                </section>
                                <h3>Direccion</h3>
                                <section>


                                    <div class="form-group clearfix">
                                        <label for="department" class="col-lg-2 control-label">Departamento</label> 
                                        <div class="col-lg-10">
                                            <select  class="form-control" required id="department" name="department" >
                                                <?php department(null); ?>


                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="province" class="col-lg-2 control-label">Provincia</label> 
                                        <div class="col-lg-10">
                                            <select  class="form-control" required id="province" name="province" >
                                            </select>
                                        </div>
                                    </div> 

                                    <div class="form-group clearfix">
                                        <label for="district" class="col-lg-2 control-label">Distrito</label> 
                                        <div class="col-lg-10">
                                            <select  class="form-control" required id="district" name="district">
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="form-group clearfix">
                                        <label class="col-lg-2 control-label " for="addrees">Direccion</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" required id="addrees" name="addrees" type="text">
                                        </div>
                                    </div>
                                </section>
                                <h3>Logo del Negocio</h3>
                                <section>

                                    <div class="form-group clearfix">
                                        <div class="col-lg-4">
                                            <span class="btn btn-success fileinput-button">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                <span>Subir Imagen</span>
                                                <input name="foto" type="file" id="foto" accept="image/*"/>
                                            </span>
                                        </div>
                                        <div class="col-lg-6">
                                            <div id="files" class="files"></div><div id="files" class="files"></div>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label class="col-lg-2 control-label " for="description"> Describe tu Empresa</label>
                                        <div class="col-lg-10">
                                            <textarea id="description" name="description" class="form-control" data-parsley-id="50" ></textarea>

                                        </div>
                                    </div>



                                </section>
                                <h3>Servicios Extras</h3>
                                <section>
                                    <div class="form-group clearfix">
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="extra" name="extra" type="text">
                                            <input type="hidden" name="service_extra" id="service_extra">
                                        </div>
                                        <div class="col-lg-4">
                                            <button type="button" class="btn btn-default waves-effect" id="add">Agregar</button> 
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-4">
                                            <ul id="add_ul">

                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <h3>Horarios de Atenci&oacute;n</h3>
                                <section>
                                    <div class="col-lg-9">
                                        <div class="form-group clearfix">
                                            <div class="col-lg-12">
                                                <table class="table m-0">

                                                    <thead>
                                                        <tr>
                                                        <th>Lunes</th>
                                                        <th>Martes</th>
                                                        <th>Miercoles</th>
                                                        <th>Jueves</th>
                                                        <th>Viernes</th>
                                                        <th>Sabado</th>
                                                        <th>Domingo</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                        <td><input id="dia_1" type="checkbox" class="dia" name="lunes" value="Lunes" data-plugin="switchery" data-color="#81c868"/></td>
                                                        <td><input id="dia_2" type="checkbox" class="dia" name="martes" value="Martes" data-plugin="switchery" data-color="#81c868"/></td>
                                                        <td><input id="dia_3" type="checkbox" class="dia" name="miercoles" value="Miercoles" data-plugin="switchery" data-color="#81c868"/></td>
                                                        <td><input id="dia_4" type="checkbox" class="dia" name="jueves" value="Jueves" data-plugin="switchery" data-color="#81c868"/></td>
                                                        <td><input id="dia_5" type="checkbox" class="dia" name="vierne" value="Viernes" data-plugin="switchery" data-color="#81c868"/></td>
                                                        <td><input id="dia_6" type="checkbox" class="dia" name="sabado" value="Sabados" data-plugin="switchery" data-color="#81c868"/></td>
                                                        <td><input id="dia_7" type="checkbox" class="dia" name="domingo" value="Domingos" data-plugin="switchery" data-color="#81c868"/></td>
                                                        </tr>


                                                    </tbody>
                                                </table>

                                            </div>

                                            <div class="col-lg-12">
                                                <hr  style="color: #2184be;border:0;  border-top:1px solid">
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">

                                            <div class="col-lg-3">
                                                <label class="col-lg-7 control-label " for="horario"> Horario Personalizado</label>
                                                <input id="horas_1" type="radio" name="horas" value="1" data-plugin="switchery" data-color="#81c868"/>
                                            </div>

                                            <div class="col-lg-2">
                                                <label class="col-lg-6 control-label " for="horario"> 24 Horas</label>
                                                <input id="horas_2" checked type="radio" name="horas" value="24" data-plugin="switchery" data-color="#81c868"/>
                                            </div>
                                            <div class="col-lg-12">
                                                <hr  style="color: #2184be;border:0;  border-top:1px solid">
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="horario">¿Tiene Intervalo de Descanso?</label>
                                            <div class="col-lg-2">
                                                Si <input id="inter_1" type="radio" name="inter"  />
                                            </div>
                                            <div class="col-lg-2">
                                                No <input id="inter_2" checked type="radio" name="inter"   />
                                            </div>


                                            <div class="col-lg-3" id="num_inter">
                                                <label class="col-lg-6 control-label " for="h_inter"> Numeros de Intervalos</label>
                                                <select id="hor_inter">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    <option>6</option>
                                                </select>
                                            </div>


                                        </div>

                                        <div class="form-group clearfix" id="dh_horas">


                                            <div class="col-lg-2" id="num_inter">
                                                <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                                <select id="desde_man">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2" id="num_inter">
                                                <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                                <select id="hasta_man">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-1" id="num_inter">Cerrado</div>

                                            <div class="col-lg-2" id="num_inter">
                                                <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                                <select id="desde_noc">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2" id="num_inter">
                                                <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                                <select id="hasta_noc">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group clearfix">

                                            <div class="col-lg-4">
                                                <button type="button" class="btn btn-default waves-effect" id="add_horas">Agregar</button> 
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group clearfix">
                                            <table class="table m-0">

                                                <thead>
                                                    <tr>
                                                    <th>Horario de Atencio</th>

                                                    </tr>
                                                </thead>
                                                <tbody id="horas_tr">
                                                    <tr>
                                                    <td>D&iacute;a</td>
                                                    <td>Hora</td>
                                                    <td></td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </section>
                                <h3>Datos de Contacto</h3>
                                <section>
                                    <div class="form-group clearfix">
                                        <label class="col-lg-2 control-label " for="face">Facebook </label>
                                        <div class="col-lg-4">
                                            <input class=" form-control" id="face" name="face" type="text">
                                        </div>
                                        <label class="col-lg-2 control-label " for="web">Web </label>
                                        <div class="col-lg-4">
                                            <input class=" form-control" id="web" name="web" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label class="col-lg-2 control-label " for="cel">Telefono Celular </label>
                                        <div class="col-lg-4">
                                            <input class=" form-control" id="cel" name="cel" type="text">
                                        </div>
                                        <label class="col-lg-2 control-label " for="ofi">Telefono Oficina </label>
                                        <div class="col-lg-4">
                                            <input class=" form-control" id="ofi" name="ofi" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="col-lg-2 control-label " for="email">Email</label>
                                        <div class="col-lg-4">
                                            <input class=" form-control" id="email" name="email" type="text">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="checkbox">
                                                <input id="checkbox0" type="checkbox"  >
                                                <label for="checkbox0">
                                                    Acepto terminos y condiciones 
                                                </label>
                                                <a href="#custom-modal"   data-toggle="modal" data-target="#myModal">Leer</a>

                                            </div>
                                        </div>

                                    </div>
                                </section>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End row -->

        </div> <!-- container -->

    </div> <!-- content -->
    <!-- sample modal content -->
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Acerca de estas condiciones</h4>
                </div>
                <div class="modal-body">
                    <h4>Condiciones variables</h4>
                    <p>Lo que has aceptado hoy es posible que ya no sea válido mañana. Esto lo deja claro descuento Peru desde el principio.
                        Una vez te registras, aceptas que las condiciones del servicio puedan cambiar en cualquier momento.</p>
                    <hr>
                    <h4>Control de tus publicaciones</h4>
                    <p>Aunque se supone que eres el propietario de las fotos, vídeos o contenidos que compartes, mientras seas usuario del servicio,
                        Descuento Peru tiene también derecho a utilizarlos.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<?php
require_once "./template/footer.php";
ob_end_flush();
?>        


<script src="assets/plugins/moment/moment.js"></script>
<script type="text/javascript" src="assets/plugins/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="assets/plugins/moment/moment.js"></script>
<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

<script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
<script src="assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>
<!--Form Wizard-->
<script src="assets/plugins/jquery.steps/build/jquery.steps.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>

<!--wizard initialization-->
<script src="assets/pages/jquery.wizard-init.js" type="text/javascript"></script>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="assets/plugins/jquery-file-upload-master/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-validate.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(window).load(function () {
        $('.selectpicker').selectpicker();
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        $('.autonumber').autoNumeric('init');
        $('.portfolioFilter a').click(function () {
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });
    $(document).ready(function () {
        $("#num_inter").hide();
        $("#dh_horas").hide();

        $("#inter_2").change(function () {
            $("#num_inter").hide();
        });
        $("#inter_1").change(function () {
            $("#num_inter").show()();
        });

        $("#horas_2").change(function () {
            $("#dh_horas").hide();
        });
        $("#horas_1").change(function () {
            $("#dh_horas").show()();
        });
        $("#desde_man").change(function () {
            if ($("#desde_man").val() > $("#hasta_man").val()) {
                $("#desde_man").val(" ");
                $.Notification.notify('error', 'top center', 'Error', 'Desde no puede ser mayor que hasta');

            }
        });
        $("#hasta_man").change(function () {
            if ($("#desde_man").val() > $("#hasta_man").val()) {
                $("#desde_man").val("1");
                $.Notification.notify('error', 'top center', 'Error', 'Desde no puede ser mayor que hasta');

            }
        });
        $("#hasta_noc").change(function () {
            if ($("#desde_noc").val() > $("#hasta_noc").val()) {
                $("#desde_noc").val(" ");
                $.Notification.notify('error', 'top center', 'Error', 'Desde no puede ser mayor que hasta');

            }
        });
        $("#desde_noc").change(function () {
            if ($("#desde_noc").val() > $("#hasta_noc").val()) {
                $("#desde_noc").val("1");
                $.Notification.notify('error', 'top center', 'Error', 'Desde no puede ser mayor que hasta');

            }
        });
        $("#add_horas").click(function () {
            if ($('.dia:checked').size() == 7) {
                if ($('input[name=horas]:checked').attr('value') == "24") {
                    $("#horas_tr").append('<tr><td>Todos Los Dias</td><td>24 Horas</td><td><button type="button" class="btn btn-default eliminar" id="del_td_' + $("#horas_tr td").size() + '">Eliminar</button></td></tr>')
                } else {
                    $("#horas_tr").append('<tr><td>Todos Los Dias</td><td>' + $("#desde_man").val() + ' a ' + $("#hasta_man").val() + '-' + $("#desde_noc").val() + ' a ' + $("#hasta_noc").val() + '</td><td><button type="button" class="btn btn-default eliminar" id="del_td_' + $("#horas_tr td").size() + '">Eliminar</button></td></tr>')
                }
            } else {
                var dias = " ";
                var i = 0;
                $('.dia:checked').each(function () {

                    if (i == $('.dia:checked').size())
                        dias += $(this).val();
                    else
                        dias += $(this).val() + ", ";
                    i++;
                });
                if ($('input[name=horas]:checked').attr('value') == "24") {
                    $("#horas_tr").append('<tr><td>' + dias + '</td><td>24 Horas</td><td><button type="button" class="btn btn-default eliminar" id="del_td_' + $("#horas_tr td").size() + '">Eliminar</button></td></tr>')
                } else {
                    $("#horas_tr").append('<tr><td>' + dias + '</td><td>' + $("#desde_man").val() + ' a ' + $("#hasta_man").val() + '-' + $("#desde_noc").val() + ' a ' + $("#hasta_noc").val() + '</td><td><button type="button" class="btn btn-default eliminar" id="del_td_' + $("#horas_tr td").size() + '">Eliminar</button></td></tr>')
                }
            }
        });
        $("#add").click(function () {
            var extra = $("#extra").val();
            if (extra != "") {
                if ($("#add_ul li").size() == 0) {
                    $("#service_extra").val($("#extra").val());
                } else {
                    $("#service_extra").val($("#service_extra").val() + ";" + $("#extra").val());
                }
                $("#add_ul").append('<li id="li_'  + $("#add_ul li").size() + '_'+$("#extra").val() +'">' + $("#extra").val() + ' <button type="button" class="btn btn-default eliminar" id="del_' + $("#add_ul li").size() + '_'+$("#extra").val() +'">Eliminar</button> <br> </li> ');
                $("#extra").val("");
                $(".eliminar").click(function () {
                    var del = $(this).attr("id").split("_");
                    $("#li_" + del[1]+"_"+del[2]).remove();
                    if ($("#add_ul li").size() == 0) {
                        $("#service_extra").val(" ");
                    } else {
                        var le = $("#service_extra").val().split(";");
                        $("#service_extra").val(" ");
                        alert(le[del[1]]);
                        le[del[1]] = " ";
                        alert(le[del[1]]);
                        for (var i = 0; i < le.length; i++) {
                           // alert(le[i]);
                            if (le[i] != " ") {
                                if ($("#service_extra").val() == " ") {
                                    $("#service_extra").val(le[i]);
                                } else {
                                    $("#service_extra").val($("#service_extra").val() + ";" + le[i]);

                                }
                            }
                        }

                    }
                });
            }
        });


        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            }
        });

        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        $('form').parsley();

        $('#department').change(function ()
        {







            var idDepartment = $(this).val();



            $('#province').empty();
            $('#district').empty();
            $.ajax({
                cache: false,
                url: '../views/addresses.php?action=department&id=' + idDepartment,
                type: 'GET',
                cache: false,
                success: function (response) {
                    $('#province').append(response);
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });




        });

        $('#province').change(function ()
        {




            var idProvince = $(this).val();



            $('#district').empty();

            $.ajax({
                url: '../views/addresses.php?action=province&id=' + idProvince,
                type: 'GET',
                cache: false,
                success: function (response) {
                    $('#district').append(response);
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });




        });
        $("#wizard-validation-form").submit(function (event)
        {
            var file_data = $('#foto').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);

            alert("data");
            event.preventDefault();
            $.ajax({
                url: "../views/negocio.php",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function ()
                {
                    $(".se-pre-con").show();
                },
                success: function (data)
                {
                    alert(data);
                    switch (data) {
                        case "0":
                            location.reload();
                            break;
                        case "1":
                            $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "2":
                            $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "3":
                            $.Notification.notify('error', 'top center', 'Formato de imagen incorrecto', 'Formato de imagen incorrecto.');
                            break;

                    }

                },
                error: function (e)
                {
                    //$("#err").html(e).fadeIn();
                }
            });


        })


    });
    /*jslint unparam: true, regexp: true */
    /*global window, $ */
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'server/php/',
                uploadButton = $('<button/>')
                .addClass('btn btn-primary')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function () {
                    var $this = $(this),
                            data = $this.data();
                    $this
                            .off('click')
                            .text('Abort')
                            .on('click', function () {
                                $this.remove();
                                data.abort();
                            });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
        $("#submit_foto").hide();
        $('#foto').change(function () {
            $("#files").empty();
        });
        $('#foto').fileupload({
            url: url,
            dataType: 'json',
            autoUpload: false,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize: 999000000,
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            limitMultiFileUploads: 1,
            disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
            previewMaxWidth: 200,
            previewMaxHeight: 200,
            previewCrop: true
        }).on('fileuploadadd', function (e, data) {
            data.context = $('<div/>').appendTo('#files');
            $.each(data.files, function (index, file) {
                var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                if (!index) {
                    node
                            .append('<br>')

                    $("#close_img").click(function () {


                        $("#files").empty();
                        $("#submit_foto").hide();
                    });
                }

                node.appendTo(data.context);
            });
        }).on('fileuploadprocessalways', function (e, data) {
            var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
            if (file.preview) {
                node
                        .prepend('<br>')
                        .prepend(file.preview);
            }
            if (file.error) {
                node
                        .append('<br>')
                // .append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                $("#submit_foto").show();

            }

        }).on('fileuploaddone', function (e, data) {
            $.each(data.result.files, function (index, file) {
                if (file.url) {
                    var link = $('<a>')
                            .attr('target', '_blank')
                            .prop('href', file.url);
                    $(data.context.children()[index])
                            .wrap(link);
                } else if (file.error) {
                    var error = $('<span class="text-danger"/>').text(file.error);
                    $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                }
            });
        }).on('fileuploadfail', function (e, data) {
            $.each(data.files, function (index) {
                var error = $('<span class="text-danger"/>').text('File upload failed.');
                $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
            });
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

    });

    //Se utiliza para que el campo de texto solo acepte letras
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toString();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
        especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }
</script>

