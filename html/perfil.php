<?php
ob_start();
require_once "../views/addresses.php";
require_once "./template/header.php";
require_once "../views/gallery.php";
$galllery = gallery();
?>
<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>
<link type="text/css" href="assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="assets/plugins/bootstrapvalidator/src/css/bootstrapValidator.css" rel="stylesheet" type="text/css" />
<!-- ========== Left Sidebar Start ========== -->   
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/style.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/jquery.fileupload.css">
<?php
require_once "./template/menus.php";
?>



<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->                      
<div class="content-page">
    <!-- Start content -->
    <div class="content">



        <div class="wraper container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="bg-picture text-center">
                        <div class="bg-picture-overlay"></div>
                        <div class="profile-info-name">
                            <img src="<?php echo!empty($person->getImgProfile()) ? $person->getImgProfile() : "assets/images/users/avatar-1.jpg"; ?>" class="thumb-lg img-circle img-thumbnail" alt="profile-image">
                            <h4 class="m-b-5"><b>Perfil</b></h4>
                            <?php if (!empty($person->getDepartment()->getNameDepartment())) { ?>
                                <p class="text-muted"><i class="fa fa-map-marker"></i> <?php echo $person->getDepartment()->getNameDepartment(); ?> , <?php echo $person->getDistrict()->getNameDistrict(); ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <!--/ meta -->
                </div>
            </div>




            <div class="row">
                <div class="col-md-4">

                    <div class="card-box m-t-20">
                        <h4 class="m-t-0 header-title"><b>Informacion Personal</b></h4>
                        <div class="p-20">
                            <div class="about-info-p">
                                <strong>Nombres</strong>
                                <br>
                                <p class="text-muted"><?php echo $person->getFirstName(); ?></p>
                            </div>
                            <div class="about-info-p">
                                <strong>Apellidos</strong>
                                <br>
                                <p class="text-muted"><?php echo $person->getLastName(); ?></p>
                            </div>
                            <div class="about-info-p">
                                <strong>Fecha de Nacimiento</strong>
                                <br>
                                <p class="text-muted"><?php echo $person->getBirthdate(); ?></p>
                            </div>      
                            <div class="about-info-p">
                                <strong>Genero</strong>
                                <br>
                                <p class="text-muted"><?php echo $person->getSex() == "M" ? "Masculino" : "Femenino"; ?></p>
                            </div>

                            <div class="about-info-p">
                                <strong>Email</strong>
                                <br>
                                <p class="text-muted"> <?php echo $person->getEmail(); ?></p>
                            </div>
                            <?php if (!empty($person->getPhone())) { ?>           
                                <div class="about-info-p">
                                    <strong>Telefono</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $person->getPhone(); ?></p>
                                </div>
                            <?php } ?>
                            <?php if (!empty($person->getDepartment()->getNameDepartment())) { ?>
                                <div class="about-info-p">
                                    <strong>Direccion:</strong>

                                </div>
                                <div class="about-info-p m-b-0">
                                    <strong>Departamento</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $person->getDepartment()->getNameDepartment(); ?></p>
                                </div>
                                <div class="about-info-p m-b-0">
                                    <strong>Provincia</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $person->getProvince()->getNameProvince(); ?></p>
                                </div>
                                <div class="about-info-p m-b-0">
                                    <strong>Distrito</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $person->getDistrict()->getNameDistrict(); ?></p>
                                </div>

                            <?php } ?>
                            <div class="about-info-p">
                                <button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">Editar Perfil</button>
                                <button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#cambio-clave-modal">Cambio de Clave</button>
                            </div>   
                        </div>
                    </div>



                    <!-- Personal-Information -->
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Configurar de Notificaciones</b></h4>

                        <div class="p-20">
                            <table class="table table table-hover m-0">
                                <thead>
                                    <tr>
                                    <th>Deseo Recibir notificaciones</th>
                                    <th><i class="glyphicon glyphicon-envelope"></i> Email</th>
                                    <th><i class="glyphicon glyphicon-phone"></i> Alerta</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <th scope="row">Promociones en mi ciudad</th>
                                    <td><div class="checkbox">
                                            <input id="email_1" <?php echo $_SESSION['notifications'][1]->email =="0" ? " ":"checked" ; ?> type="checkbox" name="email_promo">
                                          <label for="email_1">
                                                
                                            </label>
                                        </div></td>
                                    <td><div class="checkbox">
                                            <input id="alert_1"  <?php echo $_SESSION['notifications'][1]->alert =="0" ? " ":"checked" ; ?> type="checkbox" name="alerta_promo">
                                           <label for="alert_1">
                                                
                                            </label>
                                        </div></td>

                                    </tr>
                                    <tr>
                                    <th scope="row">Eventos en mi ciudad</th>
                                    <td><div class="checkbox">
                                            
                                            <input id="email_2" <?php echo $_SESSION['notifications'][2]->email =="0" ? " ":"checked" ; ?>  type="checkbox" name="email_evento">
                                            <label for="email_2">                               
                                            </label>
                                            </div></td>
                                    <td><div class="checkbox">
                                            <input id="alert_2" <?php echo $_SESSION['notifications'][2]->alert =="0" ? " ":"checked" ; ?> type="checkbox" name="alerta_evento">
                                            <label for="alert_2">
                                                
                                            </label>
                                             </div></td>

                                    </tr>
                                    <tr>
                                    <th scope="row">Sorteos</th>
                                    <td><div class="checkbox">
                                            <input id="email_3" <?php echo $_SESSION['notifications'][3]->email =="0" ? " ":"checked" ; ?> type="checkbox" name="email_sorteo"> 
                                            <label for="email_3">
                                                
                                            </label>
                                        </div></td>
                                    <td><div class="checkbox">
                                            <input id="alert_3" <?php echo $_SESSION['notifications'][3]->alert =="0" ? " ":"checked" ; ?> type="checkbox" name="alerta_sorteo">
                                            <label for="alert_3">
                                                
                                            </label>
                                        </div></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Personal-Information -->




                </div>


                <div class="col-md-8">

                    <div class="card-box m-t-20">
                        <h4 class="m-t-0 header-title"><b>Fotos</b></h4>
                        <div class="p-20">
                            <div class="timeline-2">
                                <!-- ============================================================== -->
                                <!-- Start right Content here -->
                                <!-- ============================================================== -->                      
                                <div class="content-page">
                                    <!-- Start content -->
                                    <div class="content">
                                        <div class="container">




                                            <!-- SECTION FILTER
                                            ================================================== -->  
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 ">
                                                    <div class="portfolioFilter">
                                                       <a href="#" data-filter="*" class="current">Todas</a>
                        <a href="#" data-filter=".profesional">Profesional</a>
                        <a href="#" data-filter=".amateur">Amateur</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row port">
                                                <div class="portfolioContainer">
                                                     <?php
                    if ($galllery != NULL) {
                        foreach ($galllery as $res) {
                            ?>
                            <div class="col-sm-6 col-lg-3 col-md-4  <?php echo $res->type == "0" ? "amateur" : "profesional"; ?>">
                                <div class="gal-detail thumb">
                                    <h4><i class="ti-heart"></i> <?php echo $res->point == NULL ? "0" : $res->point; ?></h4>
                                    <a href="<?php echo $res->url; ?>" class="image-popup" >
                                        <img src="<?php echo $res->url; ?>" class="thumb-img" alt="work-thumbnail">
                                    </a>
                                   
                                    

                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>



                                                </div>
                                            </div> <!-- End row -->

                                        </div> <!-- container -->

                                    </div> <!-- content -->



                                </div>
                                <!-- ============================================================== -->
                                <!-- End Right content here -->
                                <!-- ============================================================== -->


                            </div>
                        </div>
                    </div>


                </div>


            </div>

            <div class="row">

            </div>
        </div> <!-- container -->

    </div> <!-- content -->

    <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
            <div class="modal-content"> 
                <div class="modal-header"> 
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                    <h4 class="modal-title">Editar Perfil</h4> 
                </div> 
                <form  id="perfil_update" data-parsley-validate  novalidate method="post" enctype="multipart/form-data">
                    <div class="modal-body"> 

                        <div class="row"> 
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Subir Foto</span>
                                    <input name="foto" type="file" id="foto" accept="image/*"/>
                                    </span>
                                </div> 
                            </div> 
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                 <div id="files" class="files"></div>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <label for="firstName" class="control-label">Nombres</label> 
                                    <input class="form-control"  type="text" required placeholder="Nombre" value="<?php echo $person->getFirstName(); ?>" name="firstName" id="firstName" onkeypress="return soloLetras(event);"/> 
                                </div> 
                            </div> 
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <label for="lastName" class="control-label">Apellidos</label> 
                                    <input class="form-control"  type="text" required placeholder="Apellido" value="<?php echo $person->getLastName(); ?>" name="lastName" id="lastName" onkeypress="return soloLetras(event);">
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <label for="email" class="control-label">Email</label> 
                                    <input class="form-control"  type="email" disabled="" required placeholder="Email" value="<?php echo $person->getEmail(); ?>" name="email" id="email"> 
                                </div> 
                            </div> 
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <label for="birthdate" class="control-label">Fecha de Nacimiento</label> 
                                    <input type="text" class="form-control" required placeholder="mm/dd/yyyy" id="datepicker-autoclose" value="<?php echo $person->getBirthdate(); ?>" name="birthdate" id="birthdate">
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <label for="email" class="control-label">Telefono</label> 
                                    <input class="form-control" data-mask="+99-999-999-999" type="text"  placeholder="Telefono" value="<?php echo $person->getPhone(); ?>" name="phone" id="phone"> 
                                </div> 
                            </div> 
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <label for="sex1" class="control-label">Genero</label> 
                                    <div class="radio radio-inline">
                                        <input type="radio" id="sex1" value="M" name="sex" <?php echo $person->getSex() == "M" ? 'checked=""' : ''; ?> >
                                        <label for="inlineRadio1"> Masculino </label>
                                    </div>
                                    <div class="radio radio-inline">
                                        <input type="radio" id="sex2" value="F" name="sex" <?php echo $person->getSex() == "F" ? 'checked=""' : ''; ?>>
                                        <label for="inlineRadio2"> Femenino </label>
                                    </div>         
                                </div> 
                            </div> 


                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group no-margin"> 
                                    <label for="department" class="control-label">Departamento</label> 
                                    <select required class="form-control" id="department" name="department" >
                                        <?php department($person->getDepartment()->getId()); ?>

                                    </select>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group no-margin"> 
                                    <label for="province" class="control-label">Provincia</label> 
                                    <select required class="form-control" id="province" name="province" >
                                        <?php $person->getProvince()->getId() == null ? " " : provinceSelect($person->getDepartment()->getId(), $person->getProvince()->getId()); ?>
                                    </select>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group no-margin"> 
                                    <label for="district" class="control-label">Distrito</label> 
                                    <select required class="form-control" id="district" name="district">
                                        <?php $person->getDistrict()->getId() == null ? "" : districtSelect($person->getProvince()->getId(), $person->getDistrict()->getId()); ?>
                                    </select>
                                </div> 
                            </div> 
                        </div> 
                    </div> 

                    <div class="modal-footer"> 
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button> 
                        <button type="submit" class="btn btn-info waves-effect waves-light">Guardar</button> 
                    </div> 
                </form>
            </div> 
        </div>
    </div><!-- /.modal -->

    <div id="cambio-clave-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
            <div class="modal-content"> 
                <div class="modal-header"> 
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                    <h4 class="modal-title">Cambio de Clave</h4> 
                </div> 
                <form  id="cambio_clave" data-parsley-validate  novalidate method="post" >
                    <div class="modal-body"> 

                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="actual_password" class="control-label">Contase&ntilde;a Actual</label> 
                                    <input  class="form-control"  minlength="6" maxlength="20" type="password" required="" placeholder="Contase&ntilde;a Actual" name="actual_password" id="actual_password" data-parsley-pattern="/^[a-zA-Z0-9\^$.*+?=!:|\\/()\[\]{}]*$/" data-parsley-pattern-message="El campo password no puede contener espacios" >
                                </div> 
                            </div> 

                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="password" class="control-label">Contase&ntilde;a</label> 
                                    <input  class="form-control"  minlength="6" maxlength="20" type="password" required="" placeholder="Contase&ntilde;a" name="password" id="password" data-parsley-pattern="/^[a-zA-Z0-9\^$.*+?=!:|\\/()\[\]{}]*$/" data-parsley-pattern-message="El campo password no puede contener espacios">
                                </div> 
                            </div> 

                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="repassword" class="control-label">Repite la Contase&ntilde;a</label> 
                                    <input data-parsley-equalto="#password"  minlength="6" maxlength="20" class="form-control" type="password" required placeholder="Repite la Contase&ntilde;a" name="repassword" id="repassword" data-parsley-pattern="/^[a-zA-Z0-9\^$.*+?=!:|\\/()\[\]{}]*$/" data-parsley-pattern-message="El campo password no puede contener espacios">
                                </div> 
                            </div> 

                        </div> 

                    </div> 

                    <div class="modal-footer"> 
                        <button type="button" id="close_c_clave" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button> 
                        <button type="submit" class="btn btn-info waves-effect waves-light">Guardar</button> 
                    </div> 
                </form>
            </div> 
        </div>
    </div><!-- /.modal -->

</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<?php
require_once "./template/footer.php";
ob_end_flush();
?>        


<script src="assets/plugins/moment/moment.js"></script>
<script type="text/javascript" src="assets/plugins/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="assets/plugins/moment/moment.js"></script>
<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

<script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
<script src="assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>


<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="assets/plugins/jquery-file-upload-master/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-validate.js"></script>

<script type="text/javascript">

    $(window).load(function () {
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        $('.autonumber').autoNumeric('init');
        $('.portfolioFilter a').click(function () {
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });
    $(document).ready(function () {
        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            }
        });

        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        $('form').parsley();

        $('#department').change(function ()
        {







            var idDepartment = $(this).val();



            $('#province').empty();
            $('#district').empty();
            $.ajax({
                cache: false,
                url: '../views/addresses.php?action=department&id=' + idDepartment,
                type: 'GET',
                cache: false,
                success: function (response) {
                    $('#province').append(response);
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });




        });

        $('#province').change(function ()
        {




            var idProvince = $(this).val();



            $('#district').empty();

            $.ajax({
                url: '../views/addresses.php?action=province&id=' + idProvince,
                type: 'GET',
                cache: false,
                success: function (response) {
                    $('#district').append(response);
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });




        });
        $("#perfil_update").submit(function (event)
        {
            var file_data = $('#foto').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            /*
             $.ajax({
             url: '../views/updateProfile.php',
             type: 'POST',
             data: $("#perfil_update").serialize(),
             enctype: 'multipart/form-data',
             //            contentType: false, // The content type used when sending data to the server.
             cache: false, // To unable request pages to be cached
             //  processData: false,
             success: function (response) {
             alert(response);
             },
             
             error: function (e) {
             alert("Error en sistema comuniquese con Soporte Tecnico");
             console.log(e);
             }
             });*/

            event.preventDefault();
            $.ajax({
                url: "../views/updateProfile.php",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function ()
                {
                    $(".se-pre-con").show();
                },
                success: function (data)
                {
                    $(".se-pre-con").hide();
                    switch (data) {
                        case "0":                            
                            location.reload();
                            break;
                        case "1":
                            $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "2":
                            $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "3":
                            $.Notification.notify('error', 'top center', 'Formato de imagen incorrecto', 'Formato de imagen incorrecto.');
                            break;

                    }

                },
                error: function (e)
                {
                    //$("#err").html(e).fadeIn();
                }
            });


        })
        $("#cambio_clave").submit(function (event)
        {

            
            $.ajax({
                url: '../views/updateProfile.php',
                type: 'POST',
                data: {password: $("#password").val(), actual_password: $("#actual_password").val(), change_pass: "cambio"},
                cache: false, // To unable request pages to be cached
                success: function (response) {
                    switch (response) {
                        case "0":
                            $.Notification.notify('success', 'top center', 'Password Actualizada', 'Password Actualizada con exito.');
                            $("#close_c_clave").click();
                            $("#password").val("");
                            $("#repassword").val("");
                            $("#actual_password").val("");
                            break;
                        case "1":
                            $.Notification.notify('error', 'top center', 'Error al cambiar el Password', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "2" :
                            $.Notification.notify('error', 'top center', 'No es el Password actual', 'No es el Password actual.');
                            $("#close_c_clave").click();
                            $("#password").val("");
                            $("#repassword").val("");
                            $("#actual_password").val("");
                            break;

                    }
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });

            event.preventDefault();



        })
        
        $("input[type=checkbox]").change(function (event){
            
            var envio =$(this).attr("id").split("_");
            
            $.ajax({
                url: '../views/updateProfile.php',
                type: 'POST',
                data: {checked: $(this).is( ":checked" )? "1" : "0", tipo: envio[0], id_noti: envio[1]},
                cache: false, // To unable request pages to be cached
                success: function (response) {
                
                    
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });
        });

    });
    /*jslint unparam: true, regexp: true */
    /*global window, $ */
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'server/php/',
                uploadButton = $('<button/>')
                .addClass('btn btn-primary')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function () {
                    var $this = $(this),
                            data = $this.data();
                    $this
                            .off('click')
                            .text('Abort')
                            .on('click', function () {
                                $this.remove();
                                data.abort();
                            });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
        $("#submit_foto").hide();
        $('#foto').change(function (){
             $("#files").empty();
        });
        $('#foto').fileupload({
            url: url,
            dataType: 'json',
            autoUpload: false,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize: 999000000,
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            limitMultiFileUploads: 1,
            disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
            previewMaxWidth: 200,
            previewMaxHeight: 200,
            previewCrop: true
        }).on('fileuploadadd', function (e, data) {
            data.context = $('<div/>').appendTo('#files');
            $.each(data.files, function (index, file) {
                var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                if (!index) {
                    node
                            .append('<br>')

                    $("#close_img").click(function () {


                        $("#files").empty();
                        $("#submit_foto").hide();
                    });
                }

                node.appendTo(data.context);
            });
        }).on('fileuploadprocessalways', function (e, data) {
            var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
            if (file.preview) {
                node
                        .prepend('<br>')
                        .prepend(file.preview);
            }
            if (file.error) {
                node
                        .append('<br>')
                // .append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                $("#submit_foto").show();

            }

        }).on('fileuploaddone', function (e, data) {
            $.each(data.result.files, function (index, file) {
                if (file.url) {
                    var link = $('<a>')
                            .attr('target', '_blank')
                            .prop('href', file.url);
                    $(data.context.children()[index])
                            .wrap(link);
                } else if (file.error) {
                    var error = $('<span class="text-danger"/>').text(file.error);
                    $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                }
            });
        }).on('fileuploadfail', function (e, data) {
            $.each(data.files, function (index) {
                var error = $('<span class="text-danger"/>').text('File upload failed.');
                $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
            });
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

    });
    
    //Se utiliza para que el campo de texto solo acepte letras
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toString();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
        especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }
</script>

