<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="html/assets/images/logoMiniAdmin.png">

        <title>Descuento Peru</title>
        <?php
        echo "<link href=\"" . HTML . "assets/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />";
        echo "<link href=\"" . HTML . "assets/css/core.css\" rel=\"stylesheet\" type=\"text/css\" />";
        echo "<link href=\"" . HTML . "assets/css/components.css\" rel=\"stylesheet\" type=\"text/css\" />";
        echo "<link href=\"" . HTML . "assets/css/icons.css\" rel=\"stylesheet\" type=\"text/css\" />";
        echo "<link href=\"" . HTML . "assets/css/pages.css\" rel=\"stylesheet\" type=\"text/css\" />";
        echo "<link href=\"" . HTML . "assets/css/responsive.css\" rel=\"stylesheet\" type=\"text/css\" />";
        echo "<link href=\"" . HTML . "assets/plugins/bootstrapvalidator/src/css/bootstrapValidator.css\" rel=\"stylesheet\" type=\"text/css\" />";
        echo "<script src=\"" . HTML . "assets/js/modernizr.min.js\"></script>";

        echo "<script src=\"" . HTML . "assets/js/jquery.min.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/bootstrap.min.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/detect.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/fastclick.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/jquery.slimscroll.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/jquery.blockUI.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/waves.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/wow.min.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/jquery.nicescroll.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/jquery.scrollTo.min.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/jquery.core.js\"></script>";
        echo "<script src=\"" . HTML . "assets/js/jquery.app.js\"></script>";
        
         echo "<script src=\"" . HTML . "assets/plugins/notifyjs/dist/notify.min.js\"></script>";
        echo "<script src=\"" . HTML . "assets/plugins/notifications/notify-metro.js\"></script>";
        ?>


        

        


    </head>
    <body>