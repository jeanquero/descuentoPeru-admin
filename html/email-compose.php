<?php
ob_start();

require_once "./template/header.php";
$name = $_GET["name"];
$id = $_GET["id_recive"];
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>


<?php
require_once "./template/menus.php";
?>


<style>
    .ui-menu {
        width: 200px;
        margin-bottom: 2em;
    }
    
    .ui-menu-item {
        margin: 8px 0 18px 15px;
    }
    .ui-menu {
        list-style: none;
        padding: 2px;
        margin: 0;
        display: block;
        outline: none;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        cursor: default;
    }
    .ui-corner-all {
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .ui-widget-content {
        border: 1px solid #aaaaaa;
        background: #ffffff url("images/ui-bg_glass_75_ffffff_1x400.png") 50% 50% repeat-x;
        color: #404040;
    }
    .ui-widget {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 13px;
    }
    .ui-front {
        z-index: 100;
    }
    .ui-menu .ui-menu-item {
        margin: 8px 0 18px 15px;
        padding: 0;
        width: 100%;
        list-style: none;
        /*support: IE10, see #8844;*/
    }
    .ui-menu .ui-menu-item a.ui-corner-all {
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }
    .ui-menu .ui-menu-item a {
        text-decoration: none;
        display: block;
        padding: 2px .4em;
        line-height: 1.5;
        min-height: 0;
        support: IE7;
        font-weight: normal;
    }
    .ui-corner-all {
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }


</style>

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->                      
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">



            <div class="row">

                <!-- Left sidebar -->
                <div class="col-lg-3 col-md-4">

                    <div class="p-20">
                        <a href="chat.php" class="btn btn-danger btn-rounded btn-custom btn-block waves-effect waves-light">Volver a Bandeja</a>



                    </div>

                </div>
                <!-- End Left sidebar -->

                <!-- Right Sidebar -->
                <div class="col-lg-9 col-md-8">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box m-t-20">
                                <div class="p-20">
                                    <form id="mensaje_form">
                                        <div class="form-group">
                                            <input type="text" name="person" value="<?php echo $name; ?>" id="person" class="form-control" placeholder="Para">
                                            <input type="hidden" name="id_person" id="id_person" value="<?php echo $id; ?>">
                                        </div>


                                        <div class="form-group">
                                            <div class="summernote">
                                                <textarea id="elm1" name="mensaje"></textarea>

                                            </div>
                                        </div>

                                        <div class="btn-toolbar form-group m-b-0">
                                            <div class="pull-right">

                                                <button class="btn btn-purple " type="button" id="enviar"> <span>Enviar</span> <i class="fa fa-send m-l-10"></i> </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- End row -->


                </div> <!-- end Col-9 -->

            </div><!-- End row -->



        </div> <!-- container -->

    </div> <!-- content -->



</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<?php
require_once "./template/footer.php";
ob_end_flush();
?>  

<!--form validation init-->
<script src="assets/plugins/tinymce/tinymce.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $("#enviar").click(function () {
            var editor = tinymce.get('elm1');

            // Save contents using some XHR call

            if ($("#person").val() == "") {
                $.Notification.notify('error', 'top center', 'Campo del destinatario no puede estar en blanco', 'Campo del destinatario no puede estar en blanco.');
            } else if ($("#id_person").val() == "") {
                $.Notification.notify('error', 'top center', 'Persona no Exite', 'La persona ingresada no existe.');
            } else if (editor.getContent() == "") {
                $.Notification.notify('error', 'top center', 'Debe ingresar un texto para enviar', 'Debe ingresar un texto para enviar.');
            } else {
                $.ajax({
                    type: "POST",
                    url: "../views/email.php",
                    data: {person: $("#person").val(), id_person: $("#id_person").val(), mensaje: editor.getContent()},
                    success: function (data) {
                        if (data == "1") {
                            $.Notification.notify('success', 'top center', 'Mensaje Enviado', 'Mensaje Enviado.');
                            location.reload();
                        } else {
                            $.Notification.notify('error', 'top center', 'Problemas para enviar Mensaje', 'Problemas para enviar Mensaje.');

                        }

                    }
                });
            }
        });
        $("#person").autocomplete({
            source: function (request, response) {

                $.ajax({
                    type: "POST",
                    url: "../views/genery.php",
                    dataType: 'json',
                    data: request,
                    success: function (data) {

                        response($.map(data, function (item) {
                            var object = new Object();
                            object.label = item.name;
                            object.img = "assets/images/users/avatar-1.jpg";
                            object.value = item.name;
                            object.id = item.id;
                            return object
                        }));
                    }
                });
            }, minLength: 1,
            select: function (event, ui) {

                $("#id_person").val(ui.item.id);
            }
        });
        if ($("#elm1").length > 0) {
            tinymce.init({
                selector: "textarea#elm1",
                theme: "modern",
                height: 300,
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }
    });
</script>