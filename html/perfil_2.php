<?php
require_once "./template/header.php";
require_once "../model/entity/Person.php";
$person = unserialize($_SESSION['person']);
?>

<link type="text/css" href="assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<!-- ========== Left Sidebar Start ========== -->    
<?php
require_once "./template/menus.php";
?>
<!-- Left Sidebar End -->

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->                      
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Perfil</h4>

                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-lg-6">
                                 
                                
                                <h4 class="m-b-30 m-t-0 header-title"><b>Foto</b></h4>  
                                <form action="#" class="form-horizontal">
                                    <div class="form-group">
                                       
                                            <?php echo $person->getFirstName(); ?>
                                        
                                    </div>
                                    <h4 class="m-b-30 m-t-0 header-title"><b>Datos</b></h4> 
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Nombres:</label>
                                        <div class="col-sm-7">
                                            <?php echo $person->getFirstName(); ?>
                                        </div>
                                    </div>
                                    
                                   

                                    


                                    <div class="form-group">                                        
                                            <label class="col-sm-5 control-label">Genero: </label>   
                                        <div class="col-sm-7">
                                            <?php echo $person->getSex() == "M" ? "Masculino" : "Femenino"; ?>
                                        </div>

                                    </div>
                                    <div class="form-group">                                        
                                            <label class="col-sm-5 control-label">Fecha de Nacimiento: </label>   
                                        <div class="col-sm-7">
                                            <?php echo $person->getBirthdate(); ?>
                                        </div>

                                    </div>
                                    <h4 class="m-b-30 m-t-0 header-title"><b>Direccion</b></h4>
                                    <div class="form-group">                                        
                                            <label class="col-sm-5 control-label">Departamento: </label>   
                                        <div class="col-sm-7">
                                            <?php        
                                            echo $person->getDepartment()->getNameDepartment(); ?>
                                        </div>

                                    </div>
                                    <div class="form-group">                                        
                                            <label class="col-sm-5 control-label">Distrito: </label>   
                                        <div class="col-sm-7">
                                            <?php        
                                            echo $person->getDistrict()->getNameDistrict(); ?>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-6">
                                <br>  <br>      
                                <form action="#" class="form-horizontal">
                                    <br>  <br>    <br> <br> 
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Apellidos:</label>
                                        <div class="col-sm-7">
                                           <?php echo $person->getLastName(); ?>
                                        </div>
                                    </div>
                                       <div class="form-group">
                                        <label class="col-sm-5 control-label">Email:</label>
                                        <div class="col-sm-7">
                                           <?php echo $person->getEmail(); ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Telefono:</label>
                                        <div class="col-sm-7">
                                           <?php echo $person->getPhone(); ?>
                                        </div>
                                    </div> 
                                 <br>  <br>  
                                    <div class="form-group">                                        
                                            <label class="col-sm-5 control-label">Provincia: </label>   
                                        <div class="col-sm-7">
                                            <?php        
                                            echo $person->getProvince()->getNameProvince(); ?>
                                        </div>

                                    </div>
                                </form>
                            </div>




                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container -->

    </div> <!-- content -->



</div>

<?php
require_once "./template/footer.php";
?>        

<!-- XEditable Plugin -->
<script src="assets/plugins/moment/moment.js"></script>

