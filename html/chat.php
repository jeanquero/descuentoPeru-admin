<?php
ob_start();

require_once "./template/header.php";
require_once "../views/email.php";
$mesajes = mensaje();
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>


<?php
require_once "./template/menus.php";

?>
    



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        

                        <div class="row">
                        
                            <!-- Left sidebar -->
                            <div class="col-lg-3 col-md-4">
                                
                                <div class="p-20">
                                	<a href="email-compose.php" class="btn btn-danger btn-rounded btn-custom btn-block waves-effect waves-light">Enviar Mensaje</a>
                                	
                                	
                                    
                                    
                                    
                                        
                                </div>
                                
                            </div>
                            <!-- End Left sidebar -->
                        
                            <!-- Right Sidebar -->
                            <div class="col-lg-9 col-md-8">
                             <!--   <div class="row">
                                    <div class="col-lg-12">
                                        <div class="btn-toolbar m-t-20" role="toolbar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary waves-effect waves-light "><i class="fa fa-trash-o"></i></button>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>  End row -->
                                
                                <div class="panel panel-default m-t-20">
                                    <div class="panel-body p-0">
                                        <div class="table-responsive">
                                            <table class="table table-hover mails m-0">
                                                <tbody>
                                                    <?php
                                                 
                                                    foreach ($mesajes as $value) {
                                                        
                                                    


?>
   
                                                    <tr class="unread">
                                                        <td class="mail-select">
                                                         <!--   <div class="checkbox checkbox-primary m-r-10">
                                                                <input id="checkbox1" type="checkbox">
                                                                <label for="checkbox1"></label>
                                                                
                                                            </div>-->
                                                    <div class="m-r-5">       
                                         <img src="<?php echo !empty($value->imgProfile) ? $value->imgProfile : "assets/images/users/avatar-1.jpg"; ?>" alt="image" class="img-responsive thumb-sm">    
                                
                                    
                                                    </div>
                                
                               
                                           
                                                            
                                                        </td>
                                                        
                                                        <td>
                                                            <a href="crm-contact.php?id_envia=<?php echo $value->id_person_shipping ?>" class="email-name"><?php echo $value->first_name." ".$value->last_name ?></a>
                                                        </td>
                                                        
                                                        <td>
                                                            <a href="crm-contact.php?id_envia=<?php echo $value->id_person_shipping ?>" class="email-msg"><?php echo $value->message ?> </a>
                                                        </td>
                                                        
                                                        <td class="text-right">
                                                            07:23 AM
                                                        </td>
                                                    </tr>
                                                    
                                              <?php 
                                                    }
                                              ?>     
                                          
                                                   
                                                    
                                                
                                                </tbody>
                                            </table>
                                        </div>
                                    
                                    </div> <!-- panel body -->
                                </div> <!-- panel -->
                                
                                
                                
                                
                                
                            </div> <!-- end Col-9 -->
                        
                        </div><!-- End row -->



                    </div> <!-- container -->
                               
                </div> <!-- content -->

                

            </div>
<?php
require_once "./template/footer.php";
ob_end_flush();
?>          