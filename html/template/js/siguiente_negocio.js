$(window).load(function () {



        $("#tab_2").hide();
        $("#tab_3").hide();
        $("#tab_4").hide();
        $("#tab_5").hide();
        $("#tab_6").hide();
        $("#siguiente_1").click(function () {
            var form = $('#home_2_form');
            form.parsley().validate();
            if (form.parsley().isValid()) {
                $("#tab_2").show();
                $("#tab_2").click();

            }
        });

        $("#siguiente_2").click(function () {
            var form = $('#profile-2_form');
            form.parsley().validate();
            if (form.parsley().isValid()) {
                $("#tab_3").show();
                $("#tab_3").click();
            }
        });

        $("#atras_1").click(function () {
            $("#tab_1").click();
        });

        $("#siguiente_3").click(function () {
            var form = $('#messages-2_form');
            form.parsley().validate();
            if (form.parsley().isValid()) {
                var foto = $("#foto").val();
                if (foto != "") {
                    $("#tab_4").show();
                    $("#tab_4").click();
                } else {
                    $.Notification.notify('error', 'top center', 'Debe selecionar una Imagen', 'Debe selecionar una Imagen.');
                }
            }
        });

        $("#atras_2").click(function () {
            $("#tab_2").click();
        });

        $("#siguiente_4").click(function () {
            $("#tab_5").show();
            $("#tab_5").click();
        });

        $("#atras_3").click(function () {
            $("#tab_3").click();
        });

        $("#siguiente_5").click(function () {
            $("#tab_6").show();
            $("#tab_6").click();
        });

        $("#atras_4").click(function () {
            $("#tab_4").click();
        });

        $("#siguiente_6").click(function () {
            var form3 = $('#messages-2_form');
            form3.parsley().validate();
            if (form3.parsley().isValid()) {
                var form2 = $('#profile-2_form');
                form2.parsley().validate();
                if (form2.parsley().isValid()) {
                    var form1 = $('#home_2_form');
                    form1.parsley().validate();
                    if (form1.parsley().isValid()) {
                        var form = $('#datos-2_form');
                        form.parsley().validate();
                        if (form.parsley().isValid()) {
                        	var cel = $("input[name='cel[]']")
              .map(function(){return $(this).val();}).get();
              var int_dias = $("input[name='int_dias[]']")
              .map(function(){return $(this).val();}).get();
               var int_horas = $("input[name='int_horas[]']")
              .map(function(){return $(this).val();}).get();
              var service_extra = $("input[name='service_extra[]']")
              .map(function(){return $(this).val();}).get();
                            var file_data = $('#foto').prop('files')[0];
                            var form_data = new FormData();
                            form_data.append('file', file_data);
                            form_data.append("name", $("#name").val());
                            form_data.append("category", $("#category").val());
                            form_data.append("sub_category", $("#sub_category").val());
                            form_data.append("department", $("#department").val());
                            form_data.append("province", $("#province").val());
                            form_data.append("district", $("#district").val());
                            form_data.append("addrees", $("#addrees").val());
                            form_data.append("description", $("#description").val());
                            form_data.append("service_extra", service_extra);
                            form_data.append("face", $("#face").val());
                            form_data.append("web", $("#web").val());
                            form_data.append("cel", cel);
                            form_data.append("ofi", $("#ofi").val());
                            form_data.append("email", $("#email").val());
                            form_data.append("int_dias", int_dias);
                            form_data.append("int_horas", int_horas);
                            
                            form_data.append("save", "true");
                            $.ajax({
                                url: "../views/negocio.php",
                                type: "POST",
                                data: form_data,
                                contentType: false,
                                cache: false,
                                processData: false,
                                beforeSend: function ()
                                {
                                  $(".se-pre-con").show();
                                },
                                success: function (data)
                                {
                                   // alert(data);
                                    switch (data) {
                                        case "0":
                                            $.Notification.notify('success', 'top center', 'Guardado con exito', 'Guardado con exito.');
                                            window.location.replace("init_negocio.php");
                                            break;
                                        case "1":
                                            $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                                            break;
                                        case "2":
                                            $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                                            break;
                                        case "3":
                                            $.Notification.notify('error', 'top center', 'Formato de imagen incorrecto', 'Formato de imagen incorrecto.');
                                            break;

                                    }

                                },
                                error: function (e)
                                {
                                    //$("#err").html(e).fadeIn();
                                }
                            });
                        }

                    } else {
                        $("#tab_1").click();
                    }
                } else {
                    $("#tab_2").click();
                }
            } else {
                $("#tab_3").click();
            }

        });

        $("#atras_5").click(function () {
            $("#tab_5").click();
        });
});