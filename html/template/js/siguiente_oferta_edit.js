var check = "";
$(window).load(function () {

   
    $("#siguiente_1").click(function () {
        var form = $('#home_2_form');
        form.parsley().validate();
        if (form.parsley().isValid()) {
            $("#tab_2").show();
            $("#tab_2").click();

        }
    });

    $("#siguiente_2").click(function () {
        var form = $('#messages-2_form');
        form.parsley().validate();
        if (form.parsley().isValid()) {
            $("#tab_3").show();
            $("#tab_3").click();
        }
    });

    $("#atras_1").click(function () {
        $("#tab_1").click();
    });

    $("#siguiente_3").click(function () {
        $("#tab_4").show();
        $("#tab_4").click();
    });

    $("#atras_2").click(function () {
        $("#tab_2").click();
    });

    $("#siguiente_4").click(function () {
        $('.restri:checked').each(
                function () {
                    if (check == "") {
                        check = $(this).val();
                    } else {
                        check = check + "|" + $(this).val();
                    }

                 //   alert("El checkbox con valor " + $(this).val() + " está seleccionado");
                }
        );
        $("#tab_5").show();
        $("#tab_5").click();
    });

    $("#atras_3").click(function () {
        $("#tab_3").click();
    });



    $("#siguiente_5").click(function () {
        var form3 = $('#messages-2_form');
        form3.parsley().validate();
        if (form3.parsley().isValid()) {

            var form1 = $('#home_2_form');
            form1.parsley().validate();
            if (form1.parsley().isValid()) {
                var form = $('#settings-2_form');
                form.parsley().validate();
                if (form.parsley().isValid()) {
                    var file_data = $('#foto').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    var int_dias = $("input[name='int_dias[]']")
              .map(function(){return $(this).val();}).get();
               var int_horas = $("input[name='int_horas[]']")
              .map(function(){return $(this).val();}).get();
              var service_extra = $("input[name='service_extra[]']")
              .map(function(){return $(this).val();}).get();
                    form_data.append("name", $("#name").val());
                    form_data.append("description", $("#description").val());
                    form_data.append("negocio", $("#negocio").val());
                    form_data.append("desde", $("#desde").val());
                    form_data.append("hasta", $("#hasta").val());
                    form_data.append("aplica", $("input[name=aplica]:checked").val());
                    form_data.append("restriccion", check);
                    form_data.append("service_extra", service_extra);
                    form_data.append("int_dias", int_dias);
                    form_data.append("int_horas", int_horas);
                    form_data.append("id_oferta", $("#id_oferta").val());
                    form_data.append("service_extra_delete", $("#service_extra_delete").val());
                    form_data.append("del_time", $("#del_time").val());
                    form_data.append("update", "true");
                    $.ajax({
                        url: "../views/ofertas.php",
                        type: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function ()
                        {
                          $(".se-pre-con").show();
                        },
                        success: function (data)
                        {
                           // alert(data);
                            switch (data) {
                                case "0":
                                    $.Notification.notify('success', 'top center', 'Guardado con exito', 'Guardado con exito.');
                                    window.location.replace("init_ofertas.php");
                                    break;
                                case "1":
                                    $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                                    break;
                                case "2":
                                    $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                                    break;
                                case "3":
                                    $.Notification.notify('error', 'top center', 'Formato de imagen incorrecto', 'Formato de imagen incorrecto.');
                                    break;

                            }

                        },
                        error: function (e)
                        {
                            //$("#err").html(e).fadeIn();
                        }
                    });
                }

            } else {
                $("#tab_1").click();
            }

        } else {
            $("#tab_2").click();
        }

    });

    $("#atras_4").click(function () {
        $("#tab_4").click();
    });

});