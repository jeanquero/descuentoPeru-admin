$(window).load(function () {

    $('#hasta').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(),
        autoclose: true,
            todayHighlight: true
            
    });

    $('#desde').datepicker({
        startDate: new Date(),
        autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
    })

    $('#desde').on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#hasta').datepicker('setStartDate', minDate);
  });

    $('#hasta').on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#desde').datepicker('setEndDate', minDate);
  });
});

function getDate(element) {
    var date;
    try {
        date = $.datepicker.parseDate("dd-mm-yyyy", element);
    } catch (error) {
        date = null;
    }
     alert(date);
    return date;
}
