<?php
ob_start();
require_once "../views/registry.php";
require_once "header.php";
registryPerson();
ob_end_flush();
?>


<link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="assets/plugins/bootstrapvalidator/src/css/bootstrapValidator.css" rel="stylesheet" type="text/css" />

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
            <h3 class="text-center"> <img src="assets/images/logoAdmin.jpg"/> </h3>
        </div>

        <div class="panel-body">
            <form class="form-horizontal m-t-20" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" data-parsley-validate novalidate method="post">


                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="m-b-15">Nombre<span class="text-danger">*</span></label>
                        <br/>
                        <input class="form-control" minlength="2" maxlength="30" onkeypress="return soloLetras(event);"  type="text" required placeholder="Nombre"  name="firstName" id="firstName">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="m-b-15">Apellido<span class="text-danger">*</span></label>
                        <br/>
                        <input class="form-control" minlength="2" maxlength="30" onkeypress="return soloLetras(event);"  type="text" required placeholder="Apellido" name="lastName" id="lastName">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="m-b-15">Email<span class="text-danger">*</span></label>
                        <br/>
                        <input class="form-control"  type="email" required placeholder="Email" name="email" id="email">
                    </div>
                </div>

                <div class="form-group m-b-20">
                    <div class="col-xs-12">
                        <label class="m-b-15">Genero<span class="text-danger">*</span></label>
                        <br/>
                        <div class="radio radio-inline">
                            <input type="radio" id="sex1" value="M" name="sex" checked="">
                            <label for="inlineRadio1"> Masculino </label>
                        </div>
                        <div class="radio radio-inline">
                            <input type="radio" id="sex1" value="F" name="sex">
                            <label for="inlineRadio2"> Femenino </label>
                        </div>                                                        
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-xs-12">
                        <label class="m-b-15">Fecha de Nacimiento</label>
                        <br/>
                        <div class="input-group">
                            <input type="text" class="form-control" required placeholder="dd/mm/yyyy" id="datepicker-autoclose" name="birthdate" id="birthdate">
                            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                        </div><!-- input-group -->
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="m-b-15">Usuario<span class="text-danger">*</span></label>
                        <br/>
                        <input class="form-control" notblank type="text" minlength="6" maxlength="50" required placeholder="Usuario" name="login" id="login" data-parsley-pattern="/^[a-zA-Z0-9]*$/" data-parsley-pattern-message="El campo usuario no puede contener espacios ni caracteres especiales ">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="m-b-15">Contase&ntilde;a<span class="text-danger">*</span></label>
                        <br/>
                        <input id="pass1" class="form-control" notblank minlength="6" maxlength="20" type="password" required="" placeholder="Contase&ntilde;a" name="password" data-parsley-pattern="/^[a-zA-Z0-9\^$.*+?=!:|\\/()\[\]{}]*$/" data-parsley-pattern-message="El campo password no puede contener espacios"  >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="m-b-15">Repite la Contase&ntilde;a<span class="text-danger">*</span></label>
                        <br/>
                        <input data-parsley-equalto="#pass1"  minlength="6" maxlength="20" class="form-control" type="password" required placeholder="Repite la Contase&ntilde;a" name="repassword" id="repassword">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                    <div class="checkbox">
		                                            <input id="checkbox0" type="checkbox" data-parsley-required-message="Debe Aceptar los terminos y condiciones"  data-parsley-required >
		                                            <label for="checkbox0">
		                                               Acepto terminos y condiciones 
		                                            </label>
                                                            <a href="#custom-modal"   data-toggle="modal" data-target="#myModal">Leer</a>
                                                            
		                                        </div>
                    </div>    
                </div>
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" class="login" id="login_submit" type="submit">
                            Registrar
                        </button>
                    </div>
                </div>

            </form>
            <div class="row">
        <div class="col-sm-12 text-center">
            <p>
                Ya tienes cuenta?<a href="../index.php" class="text-primary m-l-5"><b>Inicio</b></a>
            </p>
        </div>
    </div>
            
            
            <!-- Modal -->
			<div id="custom-modal" class="modal-demo">
			    <button type="button" class="close" onclick="Custombox.close();">
			        <span>&times;</span><span class="sr-only">Close</span>
			    </button>
			    <h4 class="custom-modal-title">Modal title</h4>
			    <div class="custom-modal-text">
			        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
			    </div>
			</div>

        </div>
        <!-- sample modal content -->
                                    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="myModalLabel">Acerca de estas condiciones</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h4>Condiciones variables</h4>
                                                    <p>Lo que has aceptado hoy es posible que ya no sea válido mañana. Esto lo deja claro descuento Peru desde el principio.
                                                        Una vez te registras, aceptas que las condiciones del servicio puedan cambiar en cualquier momento.</p>
                                                    <hr>
                                                    <h4>Control de tus publicaciones</h4>
                                                    <p>Aunque se supone que eres el propietario de las fotos, vídeos o contenidos que compartes, mientras seas usuario del servicio,
                                                        Descuento Peru tiene también derecho a utilizarlos.</p>
                                                     </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
    </div>

    

</div>

<?php
require_once "footer.php";
?> 


<script src="assets/plugins/moment/moment.js"></script>
<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>
        <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
        <script src="assets/plugins/custombox/dist/legacy.min.js"></script>
<script>
   
    jQuery(document).ready(function () {
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        
        $('form').parsley();
        $('#email').change(function (){
            $.ajax({
                url: '../views/valid.php',
                type: 'POST',
                data: {email: $(this).val()},
                cache: false, // To unable request pages to be cached
                success: function (response) {
                if(response == 1){
                    $.Notification.notify('error','top center','Email Invalido', 'El Email ya se Encuentra Registado.');
                    $('#email').val("");
                }
                    
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });
        });
    });

 
//Se utiliza para que el campo de texto solo acepte letras
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toString();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
        especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }



</script>

<?php
?>
