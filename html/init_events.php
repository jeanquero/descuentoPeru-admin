<?php
ob_start();

require_once "./template/header.php";
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>


<?php
require_once "./template/menus.php";
require_once "../views/events.php";
$events = misEvents();
//var_dump($events);
?>



<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <?php if ($events == NULL) { ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title"><b>Evento</b></h4>

                            <p class="text-muted m-b-30 font-13">
                                Vemos que aun no agregaste ningun evento, es Facil y R&aacute;pido.
                            </p>
                            <a href="mi_event.php" class="btn btn-primary waves-effect waves-light"  data-overlayColor="#36404a">Agregar mi Evento</a>


                        </div>
                    </div>
                </div>
            <?php } else { ?>
            <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">                        
                        <a href="mi_event.php" class="btn btn-pink btn-block text-uppercase waves-effect waves-light" data-animation="push" data-plugin="custommodal" 
                           data-overlaySpeed="100" data-overlayColor="#36404a">Agregar Evento</a>
                    </div>
                     
                </div>
            <br>
                <div class="row">
                    <?php foreach ($events as $value) {
                            
                        ?>
                    <div class="col-md-4" id="div_neg_<?php echo $value->id_commerce;?>">
                        <div class="thumbnail">
                            <img src="<?php echo $value->img; ?>" class="img-responsive" width="200" height="200">
                            <div class="caption">
                                <h3><?php echo $value->name; ?></h3>
                               
                                <p>
                                    <a href="ver_event.php?id=<?php echo $value->id_event;?>" class="btn btn-primary waves-effect waves-light" role="button">Ver</a>
                                  <!--  <a href="mi_negocio_edit.php?id=<?php echo $value->id_oferta;?>" class="btn btn-primary waves-effect waves-light" role="button">Editar</a>-->
                                    <a href="#"  class="btn btn-default waves-effect waves-light m-l-5 eliminar" id="neg_<?php echo $value->id_event;?>" role="button">Eliminar</a>
                                    <a href="#"  class="btn btn-default waves-effect waves-light m-l-5 activar" id="activar_<?php echo $value->id_event;?>_<?php echo $value->active == '0' ? '1' : '0' ;?>" role="button"><?php echo $value->active == '0' ? 'Activar' : 'Bloquear' ;?></a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            <?php } ?>

        </div> <!-- container -->

    </div> <!-- content -->



</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<?php
require_once "./template/footer.php";
ob_end_flush();
?> 
<script type="text/javascript">    
    $(document).ready(function () {
        $(".eliminar").click(function (){
            var r = confirm("¿Quiere Eliminar el evento?");
          if (r) {
          var del = $(this).attr("id").split("_"); 
          $.ajax({
                    url: "../views/events.php",
                    type: "POST",
                    data: {even:del[0], id: del[1]},
                    
                    
                    beforeSend: function ()
                    {
                        //  $(".se-pre-con").show();
                    },
                    success: function (data)
                    {
                       // alert(data);
                        $("#div_neg_"+del[1]).remove();
                        $(".se-pre-con").hide();
                        switch (data) {                            
                            case "0":
                                $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                                break;


                        }
                        location.reload();

                    },
                    error: function (e)
                    {
                        //$("#err").html(e).fadeIn();
                    }
                });
      }
        });


        $(".activar").click(function (){
          var del = $(this).attr("id").split("_"); 
          $.ajax({
                    url: "../views/events.php",
                    type: "POST",
                    data: {acti:del[0], id: del[1], actibool: del[2]},
                    
                    
                    beforeSend: function ()
                    {
                        //  $(".se-pre-con").show();
                    },
                    success: function (data)
                    {
                       // alert(data);
                        $("#div_neg_"+del[1]).remove();
                        $(".se-pre-con").hide();
                        switch (data) {                            
                            case "0":
                                $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                                break;


                        }
                        location.reload();

                    },
                    error: function (e)
                    {
                        //$("#err").html(e).fadeIn();
                    }
                });
        });
    })
</script>    