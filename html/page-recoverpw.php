<?php

  ob_start();
require_once "../views/registry.php";
require_once "header.php";
resetPass();
ob_end_flush();
?>
<link href="assets/plugins/bootstrapvalidator/src/css/bootstrapValidator.css" rel="stylesheet" type="text/css" />

		<div class="account-pages"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page">
			<div class=" card-box">
				<div class="panel-heading">
					<h3 class="text-center"> Restablecer el Password </h3>
				</div>

				<div class="panel-body">
					<form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" data-parsley-validate novalidate method="post"class="text-center">
						<div class="alert alert-info alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
								×
							</button>
							Introduzca su  <b>Email</b>  y se le enviarán las instrucciones!
						</div>
						<div class="form-group m-t-40">
							<div class="col-xs-12">
                                                            
                                                            <input type="email" class="form-control" placeholder="Introduzca su Email" name="email" required="" autocomplete="off" >
                                                            
							</div>
                                                    
						</div>
                                            <br>
                                            
                                            <div class="form-group text-center m-t-40">
							<div class="col-xs-12">
								<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" class="login" id="login_submit" type="submit">
									Reiniciar
								</button>
							</div>
						</div>

					</form>
				</div
                                 <div class="col-sm-12">
                        <a href="index.php" class="text-dark"> Volver al Inicio</a>
                    </div>
			</div>
			

		</div>

		<script>
			var resizefunc = [];
		</script>

	
 <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>
	</body>
</html>