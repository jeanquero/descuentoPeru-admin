<?php
ob_start();

require_once "./template/header.php";
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>


<?php
require_once "./template/menus.php";
require_once "../views/events.php";
$event = buscarEvents($_GET["id"]);
$time = buscarTime($_GET["id"]);

?>			
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">





            <div class="card-box">



                <div class="row">
                    <div class="col-md-4">
                        <a href="init_events.php" class="btn btn-primary waves-effect waves-light" role="button">Volver</a>

                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img src="<?php echo !empty($event->img) ? $event->img : $event->img_commerce ?>" class="img-responsive">
                            <div class="caption">
                                <h3>Descripci&oacute;n</h3>
                                <p>
                                    <?php echo $event->description ?>
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        
                        <div class="col-md-4"></div>
                        <a href="mi_event_edit.php?id=<?php echo $_GET["id"];?>" class="btn btn-primary waves-effect waves-light" role="button">Modificar</a>

                        <a href="#" class="btn btn-primary waves-effect waves-light activar" id="activar_<?php echo $event->id_event;?>_<?php echo $event->active == '0' ? '1' : '0' ;?>" role="button" role="button"><?php echo $event->active == '0' ? 'Activar' : 'Bloquear' ;?></a>
                        
                    </div>

                </div>
                 <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div class="col-md-12"><i class="ti-location-pin"></i></div>
                        <div class="col-md-4"><h4><?php echo $event->address ?></h4></div>

                    </div>
                    <div class="col-md-3">
                        <?php if($time != null) {?>
                        <div class="col-md-12"><i class="ti-time"></i></div>
                        <?php foreach ($time as $value) {
                            
                        ?>
                        <div class="col-md-12"><h4>*<?php echo $value->hour ?></h4></div>
                        <?php }
                    }?>
                    </div>
                    <div class="col-md-3">
                       
                         <?php if(!empty($event->phone)) {
                          $cel = explode(",", $event->phone);
                          if (count($cel)>1) {
                            ?>
<div class="col-md-12"><i class="ti-mobile"></i></div>
                             <?php
                                        for ($i=0; $i < count($cel); $i++) { 
                            ?>
                        <div class="col-md-12"><h4>*<?php echo $cel[$i] ?></h4></div>
                         <?php }}}?>
                        <?php if(!empty($event->office_phone)) {?>
                        <div class="col-md-12"><h4>*<?php echo $event->office_phone ?></h4></div>
                    <?php }?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <?php if($event->web != "") {?>
                        <div class="col-md-12"><i class="ti-gallery"></i></div>
                        <div class="col-md-4"><h4><?php echo $event->web ?></h4></div>
                         <?php } ?>
                    </div>
                    
                    <div class="col-md-3">
                       <div class="col-md-12">Evento Valido </div>
                        <?php if($event->several_days=="1") {?>
                        <div class="col-md-12"><h4>Desde<?php echo formatoFecha($event->from) ?></h4></div>
                     
                        
                    <?php } else { ?>
<div class="col-md-12"><h4><?php echo formatoFecha($event->day) ?></h4></div>
                    <?php } ?>

                    </div>
                    <div class="col-md-3 ">
                        <?php if($event->email!="") {?>
                        <div class="col-md-12"><i class="ti-email"></i></div>
                        <div class="col-md-4"><h4><?php echo $event->email ?></h4></div>
                        <?php } ?>
                    </div>
                    
<div class="col-md-1"></div>
                </div>
                
                
            </div>







        </div> <!-- container -->

    </div> <!-- content -->



</div>


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<?php
require_once "./template/footer.php";
ob_end_flush();
?> 

<script type="text/javascript">    
    $(document).ready(function () {
        $(".activar").click(function (){
          var del = $(this).attr("id").split("_"); 
          $.ajax({
                    url: "../views/events.php",
                    type: "POST",
                    data: {acti:del[0], id: del[1], actibool: del[2]},
                    
                    
                    beforeSend: function ()
                    {
                        //  $(".se-pre-con").show();
                    },
                    success: function (data)
                    {
                       // alert(data);
                        $("#div_neg_"+del[1]).remove();
                        $(".se-pre-con").hide();
                        switch (data) {                            
                            case "0":
                                $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                                break;


                        }
                        location.reload();

                    },
                    error: function (e)
                    {
                        //$("#err").html(e).fadeIn();
                    }
                });
        });
    })
</script>    