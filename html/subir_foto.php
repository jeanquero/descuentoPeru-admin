<?php
ob_start();
require_once "./template/header.php";
require_once "../views/addresses.php";
?>
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/style.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/jquery.fileupload.css">
<link href="assets/plugins/bootstrapvalidator/src/css/bootstrapValidator.css" rel="stylesheet" type="text/css" />
<!-- ========== Left Sidebar Start ========== -->    
<?php
require_once "./template/menus.php";
?>

<div class="content-page">
    <div class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-30 header-title"><b>Subir Fotos</b></h4>
                    <form  id="form_subir_foto" data-parsley-validate  novalidate method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-5 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">
                                <span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Subir Foto</span>
                                    <!-- The file input field used as target for the file upload widget -->
                                    <input id="fileupload" type="file" name="foto" accept="image/*" >

                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">
                                <label class="col-md-2 control-label">Titulo</label>
                                <div class="col-md-10">
                                <input class="form-control" minlength="2" maxlength="30"   type="text" required placeholder="Titulo"  name="title" id="title">
                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">

                                <label class="col-md-2 control-label"></label>
                                <div class="radio radio-inline">
                                    <input type="radio" id="criterio" value="1" name="type" checked="">
                                    <label for="criterio"> Profesional </label>
                                </div>
                                <div class="radio radio-inline">
                                    <input type="radio" id="criterio2" value="0" name="type">
                                    <label for="criterio2"> Amateur </label>
                                </div> 

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">
                                <label class="col-md-2 control-label">Descripcion</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="descripcion" id="descripcion" rows="5" ></textarea>
                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">


                                <label for="department" class="col-md-2 control-label">Direccion</label>
                                <div class="col-md-10">
                                    <select required class="form-control" id="department" name="department" >
                                        <?php department(""); ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <hr>
                        <div class="row" id="submit_foto"> 
                            <div class="col-lg-5 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">
                                <button type="submit" id="button_submitC" class="btn btn-info waves-effect waves-light">Guardar</button> 
                                <button type="button" id="close_img" class="btn btn-info waves-effect waves-light">Eliminar</button> 
                            </div>
                        </div> 
                    </form>
                </div>

            </div>
            <div class="col-lg-4">

                <div class="card-box">
                    <h4 class="m-t-0 m-b-30 header-title"><b>Vista Previa</b></h4>
                    <!-- The global progress bar
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div> -->
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>

                </div>




            </div>




        </div>
    </div>
</div>

<?php
require_once "./template/footer.php";
ob_end_flush();
?>

<script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="assets/plugins/jquery-file-upload-master/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-validate.js"></script>

<script>
    $(document).ready(function () {

        $("form").submit(function (event)
        {
            var file_data = $('#fileupload').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);

            event.preventDefault();
            $.ajax({
                url: "../views/file.php",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function ()
                {
                    $(".se-pre-con").show();
                },
                success: function (data)
                {
                     $(".se-pre-con").hide();
                    switch (data) {
                        case "0":                           
                            $.Notification.notify('success', 'top center', 'Foto Guardada Con Exito', 'Foto Guardada Con Exito.');

                            $("#files").empty();
                            $("#submit_foto").hide();
                            $("#fileupload").val("");
                            $("#descripcion").val("");
                            $("#department").val("");
                            break;
                        case "1":
                            $.Notification.notify('error', 'top center', 'Error Guardando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "2":
                            $.Notification.notify('error', 'top center', 'Error Guardando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "3":
                            $.Notification.notify('error', 'top center', 'Formato de imagen incorrecto', 'Formato de imagen incorrecto.');
                            break;

                    }

                },
                error: function (e)
                {
                            $.Notification.notify('error', 'top center', 'Error Guardando', 'Error en sistema comuniquese con Soporte Tecnico.');
                }
            });


        });
        $("#button_submit").click(function () {
            $("form").submit();
        });
    });
    /*jslint unparam: true, regexp: true */
    /*global window, $ */
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'server/php/',
                uploadButton = $('<button/>')
                .addClass('btn btn-primary')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function () {
                    var $this = $(this),
                            data = $this.data();
                    $this
                            .off('click')
                            .text('Abort')
                            .on('click', function () {
                                $this.remove();
                                data.abort();
                            });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
        $("#submit_foto").hide();
        $('#fileupload').change(function (){
             $("#files").empty();
        });
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            autoUpload: false,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize: 999000000,
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            limitMultiFileUploads: 1,
            disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
            previewMaxWidth: 200,
            previewMaxHeight: 200,
            previewCrop: true
        }).on('fileuploadadd', function (e, data) {
            data.context = $('<div/>').appendTo('#files');
            $.each(data.files, function (index, file) {
                var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                if (!index) {
                    node
                            .append('<br>')

                    $("#close_img").click(function () {


                        $("#files").empty();
                        $("#submit_foto").hide();
                    });
                }

                node.appendTo(data.context);
            });
        }).on('fileuploadprocessalways', function (e, data) {
            var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
            if (file.preview) {
                node
                        .prepend('<br>')
                        .prepend(file.preview);
            }
            if (file.error) {
                node
                        .append('<br>')
                // .append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                $("#submit_foto").show();

            }

        }).on('fileuploaddone', function (e, data) {
            $.each(data.result.files, function (index, file) {
                if (file.url) {
                    var link = $('<a>')
                            .attr('target', '_blank')
                            .prop('href', file.url);
                    $(data.context.children()[index])
                            .wrap(link);
                } else if (file.error) {
                    var error = $('<span class="text-danger"/>').text(file.error);
                    $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                }
            });
        }).on('fileuploadfail', function (e, data) {
            $.each(data.files, function (index) {
                var error = $('<span class="text-danger"/>').text('File upload failed.');
                $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
            });
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

    });
</script>


