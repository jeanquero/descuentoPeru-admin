<?php
ob_start();

require_once "./template/header.php";
require_once "../views/genery.php";
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>


<?php
require_once "./template/menus.php";
?>
<!-- Left Sidebar End -->

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">




            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Registro de Usuario</b></h4>

                        <div class="row">
                            <form class="form-horizontal" role="form" data-parsley-validate novalidate method="post" >                                    

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Nombre<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <input class="form-control" minlength="2" maxlength="30" onkeypress="return soloLetras(event);"  type="text" required placeholder="Nombre"  name="firstName" id="firstName">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="example-email">Email<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <input class="form-control"  type="email" required placeholder="Email" name="email" id="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Fecha de Nacimiento<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" required placeholder="dd/mm/yyyy" id="datepicker-autoclose" name="birthdate" id="birthdate">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Telefono</label>
                                        <div class="col-md-10">
                                            <input class="form-control" data-mask="+99-999-999-999" type="text"  placeholder="Telefono" value="" name="phone" id="phone"> 
                                        </div>
                                    </div>                                                                        
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Rol<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <select required class="form-control" id="rol" name="rol" >
                                                <?php rol(null); ?>
                                            </select>
                                        </div>
                                    </div> 




                                </div>

                                <div class="col-md-6">


                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="example-email">Apellido<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <input class="form-control" minlength="2" maxlength="30" onkeypress="return soloLetras(event);"  type="text" required placeholder="Apellido" name="lastName" id="lastName">
                                        </div>
                                    </div>                                   
                                    <div class="form-group">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-2">
                                            <label class="mcol-md-12 control-label">Genero<span class="text-danger">*</span></label>
                                        </div>
                                        <div class="col-md-9">


                                            <div class="col-md-6">
                                                <input type="radio" id="sex1" value="M" name="sex" checked="">
                                                <label for="inlineRadio1"> Masculino </label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="radio" id="sex1" value="F" name="sex">
                                                <label for="inlineRadio2"> Femenino </label>
                                            </div>                                                        
                                        </div>
                                    </div>                                    


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Usuario<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <input class="form-control" notblank type="text" minlength="6" maxlength="50" required placeholder="Usuario" name="login" id="login" data-parsley-pattern="/^[a-zA-Z0-9]*$/" data-parsley-pattern-message="El campo usuario no puede contener espacios ni caracteres especiales " />
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Contase&ntilde;a<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <input id="pass1" class="form-control" notblank minlength="6" maxlength="20" type="password" required="" placeholder="Contase&ntilde;a" name="password" data-parsley-pattern="/^[a-zA-Z0-9\^$.*+?=!:|\\/()\[\]{}]*$/" data-parsley-pattern-message="El campo password no puede contener espacios"  />

                                        </div>
                                    </div>  

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Repite la Contase&ntilde;a<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <input data-parsley-equalto="#pass1"  minlength="6" maxlength="20" class="form-control" type="password" required placeholder="Repite la Contase&ntilde;a" name="repassword" id="repassword">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-10"></div>
                                        <div class="col-sm-2">
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Guardar</button> 
                                        </div>
                                    </div>

                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>







        </div> <!-- container -->

    </div> <!-- content -->



</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<?php
require_once "./template/footer.php";
ob_end_flush();
?> 


<script src="assets/plugins/moment/moment.js"></script>
<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>
<!-- Modal-Effect -->
<script src="assets/plugins/custombox/dist/custombox.min.js"></script>
<script src="assets/plugins/custombox/dist/legacy.min.js"></script>
<script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
<script>

                                                jQuery(document).ready(function () {
                                                    jQuery('#datepicker-autoclose').datepicker({
                                                        autoclose: true,
                                                        todayHighlight: true,
                                                        format: 'dd-mm-yyyy'
                                                    });

                                                    $('form').parsley();
                                                    $('#email').change(function () {
                                                        $.ajax({
                                                            url: '../views/valid.php',
                                                            type: 'POST',
                                                            data: {email: $(this).val()},
                                                            cache: false, // To unable request pages to be cached
                                                            success: function (response) {
                                                                if (response == 1) {
                                                                    $.Notification.notify('error', 'top center', 'Email Invalido', 'El Email ya se Encuentra Registado.');
                                                                    $('#email').val("");
                                                                }

                                                            },

                                                            error: function (e) {
                                                                alert("Error en sistema comuniquese con Soporte Tecnico");
                                                                console.log(e);
                                                            }
                                                        });
                                                    });
                                                });

                                                $('form').submit(function (event) {
                                                    event.preventDefault();
                                                    $.ajax({
                                                        url: "../views/admin.php",
                                                        type: "POST",
                                                        data: new FormData(this),
                                                        contentType: false,
                                                        cache: false,
                                                        processData: false,
                                                        beforeSend: function ()
                                                        {
                                                            $(".se-pre-con").show();
                                                        },
                                                        success: function (data)
                                                        {
                                                            
                                                            $(".se-pre-con").hide();
                                                            switch (data) {
                                                                case "1":
                                                                    $.Notification.notify('success', 'top center', 'Guardado con exito', 'Guardado con exito.');
                                                                    location.reload();
                                                                    break;
                                                                case "2":
                                                                    $.Notification.notify('error', 'top center', 'Error Guardando', 'Error en sistema comuniquese con Soporte Tecnico.');
                                                                    break;
                                                                case "4":
                                                                    $.Notification.notify('error','top center','Email Invalido', 'El Email ya se Encuentra Registado.');
                                                                    break;
                                                                case "3":
                                                                    $.Notification.notify('error','top center','Usuario Invalido', 'El Usuario ya se Encuentra Registado.');
                                                                    break;

                                                            }

                                                        },
                                                        error: function (e)
                                                        {
                                                            //$("#err").html(e).fadeIn();
                                                        }
                                                    });
                                                });
//Se utiliza para que el campo de texto solo acepte letras
                                                function soloLetras(e) {
                                                    key = e.keyCode || e.which;
                                                    tecla = String.fromCharCode(key).toString();
                                                    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
                                                    especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

                                                    tecla_especial = false
                                                    for (var i in especiales) {
                                                        if (key == especiales[i]) {
                                                            tecla_especial = true;
                                                            break;
                                                        }
                                                    }

                                                    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                                                        return false;
                                                    }
                                                }



</script>
