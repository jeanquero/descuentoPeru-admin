<?php
session_start();
ob_start();
if(isset($_SESSION['login']) || !empty($_SESSION['login']) )
{
     header("location: html/index.php");
   
}
require_once "views/login.php";
require_once "header_login.php";
login();
ob_end_flush();
?>
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        	<div class=" card-box">
            


            <div class="panel-body">
                <div class="panel-heading"> 
                
                <h3 class="text-center"> <img src="html/assets/images/logoAdmin.jpg"/> </h3>
            </div> 
            <form class="form-horizontal m-t-20" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" data-parsley-validate novalidate method="post">
                
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="m-b-15">Usuario<span class="text-danger">*</span></label>
                                                        <br/>
                        <input class="form-control"  type="text" minlength="6" maxlength="50" autocomplete="off"  required placeholder="Usuario" name="login" id="login">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="m-b-15">Password<span class="text-danger">*</span></label>
                                                        <br/>
                        <input class="form-control" type="password" required  placeholder="Password" name="password" id="password" >
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup">
                                Recuérdame
                            </label>
                        </div>
                        
                    </div>
                </div>
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">Inicio</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        <a href="html/page-recoverpw.php" class="text-dark"><i class="fa fa-lock m-r-5"></i> Olvidaste tu contraseña?</a>
                    </div>
                </div>
            </form> 
            
            </div> 
                    <div class="row">
            	<div class="col-sm-12 text-center">
            		<p>No tienes una cuenta? <a href="html/page-register.php" class="text-primary m-l-5"><b>Regístrate</b></a></p>
                        
                    </div>
            </div>
            </div>                              
                
            
        </div>
        <script type="text/javascript" src="html/assets/plugins/parsleyjs/dist/parsley.js"></script>
         <script >
			jQuery(document).ready(function() {

		
                
				$('form').parsley();
			});
		</script>
 <?php
require_once "footer_login.php";
?>       
        

       