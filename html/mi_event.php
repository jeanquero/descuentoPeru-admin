<?php
ob_start();
require_once "../views/addresses.php";
require_once "../views/genery.php";
require_once "./template/header.php";
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>
<link type="text/css" href="assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="assets/plugins/bootstrapvalidator/src/css/bootstrapValidator.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/select2/select2.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
<!-- ========== Left Sidebar Start ========== -->   
<link rel="stylesheet" type="text/css" href="assets/plugins/jquery.steps/demo/css/jquery.steps.css" />
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/style.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/jquery.fileupload.css">
<?php
require_once "./template/menus.php";
?>

<style>
    .elm_li {margin: 2px 0 10px 5px;}
</style>

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->                      
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <div class="row">     
                <div class="col-lg-12"> 
                    <ul class="nav nav-tabs tabs">
                        <li class="active tab">
                            <a href="#home-2" data-toggle="tab" id="tab_1" aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                <span class="hidden-xs">Datos Principales</span> 
                            </a> 
                        </li>                         
                        <li class="tab"> 
                            <a href="#messages-2" data-toggle="tab" id="tab_2" aria-expanded="true"> 
                                <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> 
                                <span class="hidden-xs">Descripcion</span> 
                            </a> 
                        </li> 

                        <li class="tab"> 
                            <a href="#horarios-2" data-toggle="tab" id="tab_3"  aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-cog"></i></span> 
                                <span class="hidden-xs">Horarios de Atenci&oacute;n</span> 
                            </a> 
                        </li> 
                        <li class="tab"> 
                            <a href="#addrees_tap" id="tab_4" data-toggle="tab" aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                <span class="hidden-xs">Direccion</span> 
                            </a> 
                        </li> 
                        <li class="tab"> 
                            <a href="#datos-2" data-toggle="tab" id="tab_5"  aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-cog"></i></span> 
                                <span class="hidden-xs">Datos de Contacto</span> 
                            </a> 
                        </li>
                        <li class="tab"> 
                            <a href="#settings-2" data-toggle="tab" id="tab_6"  aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-cog"></i></span> 
                                <span class="hidden-xs">Restricciones Personalizadas</span> 
                            </a> 
                        </li> 

                    </ul> 
                    <div class="tab-content"> 

                        <div class="tab-pane active" id="home-2"> 
                            <form id="home_2_form">

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="name">Nombre del Evento </label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" required id="name" name="name" type="text">
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <div class="col-lg-4">
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Subir Imagen</span>
                                            <input name="foto" type="file" id="foto" accept="image/*"/>
                                        </span>
                                    </div>
                                    <div class="col-lg-6">
                                        <div id="files" class="files"></div>
                                    </div>
                                </div>
 
                            </form>
                               
                            <div class="form-group clearfix">
                                <div class="col-lg-11"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_1"> <span>Siguiente</span>  </button>

                                </div>
                            </div>   
                        </div> 

                        <div class="tab-pane" id="messages-2">
                            <form id="messages-2_form">

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="description"> Describe tu Empresa</label>
                                    <div class="col-lg-10">
                                        <textarea id="description" required name="description" class="form-control" data-parsley-id="50" ></textarea>

                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="col-lg-12 control-label " for="exp"> Tu evento cura varios dias?</label>
                                    <div class="col-lg-2">
                                        <label class="col-lg-5 control-label " for="horario"> Si</label>
                                        <input id="si_va_day"  type="radio" name="varios_dias" value="1"  data-color="#81c868"/>
                                    </div>

                                    <div class="col-lg-2">
                                        <label class="col-lg-5 control-label " for="horario"> No</label>
                                        <input id="no_va_day" checked type="radio" name="varios_dias" value="0"  data-color="#81c868"/>
                                    </div>
                                    <div class="col-lg-12">
                                        <hr  style="color: #2184be;border:0;  border-top:1px solid">
                                    </div>
                                </div>
                                <div class="form-group clearfix" id="evento_valio">
                                    <label class="col-lg-2 control-label " for="dat_evento"> Evento Valida</label>
                                    <div class="col-lg-12">
                                        <label class="col-lg-2 " for="desde"> Desde</label>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control"  placeholder="dd/mm/aaaa" id="desde" name="desde" >
                                        </div>
                                        <label class="col-lg-2 " for="hasta"> Hasta</label>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control"  placeholder="dd/mm/aaaa" id="hasta" name="hasta" >

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix" id="evento_dia">
                                    <label class="col-lg-2 control-label " for="dat_evento"> Evento Valida</label>
                                    <div class="col-lg-12">
                                        <label class="col-lg-2 " for="dia"> Introduce el d&iacute;a de tu evento</label>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control" required placeholder="dd/mm/aaaa" id="dia" name="dia" >
                                        </div>

                                    </div>
                                </div>


                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_1"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_2"> <span>Siguiente</span>  </button>

                                </div>
                            </div>
                        </div> 
                        <div class="tab-pane" id="horarios-2">

                            <form id="horarios-2_form">
                                <div class="col-lg-9">
                                    <div class="form-group clearfix">


                                        <div class="col-lg-12">
                                            <hr  style="color: #2184be;border:0;  border-top:1px solid">
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">

                                        <div class="col-lg-5">
                                            <label class="col-lg-7 control-label " for="horario"> Horario Personalizado</label>
                                            <input id="horas_1" type="radio" name="horas" value="1"  data-color="#81c868"/>
                                        </div>

                                        <div class="col-lg-5">
                                            <label class="col-lg-6 control-label " for="horario"> Dura todo el d&iacute;a</label>
                                            <input id="horas_2" checked type="radio" name="horas" value="24"  data-color="#81c868"/>
                                        </div>
                                        <div class="col-lg-12">
                                            <hr  style="color: #2184be;border:0;  border-top:1px solid">
                                        </div>
                                    </div>

                                    <div class="form-group clearfix" id="intervalos">
                                        <label class="col-lg-2 control-label " for="horario">¿Tiene Intervalo de Descanso?</label>
                                        <div class="col-lg-2">
                                            Si <input id="inter_1" type="radio" name="inter"  />
                                        </div>
                                        <div class="col-lg-2">
                                            No <input id="inter_2" checked type="radio" name="inter"   />
                                        </div>


                                        <div class="col-lg-3" id="num_inter">
                                            <label class="col-lg-6 control-label " for="h_inter"> Numeros de Intervalos</label>
                                            <select id="hor_inter">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                
                                            </select>
                                        </div>


                                    </div>

                                   
                                    <div class="form-group clearfix" id="dh_horas">


                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                            <select id="desde_man" name="desde[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                            <select id="hasta_man" name="hasta[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div id="intervalos_select_2">
                                             <div class="col-lg-1" id="num_inter">Cerrado</div>

                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                            <select id="desde_noc" name="desde[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                            <select id="hasta_noc" name="hasta[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>

                                        </div>
                                        </div>

                                        <div id="intervalos_select_3">
                                             <div class="col-lg-1" id="num_inter">Cerrado</div>

                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                            <select id="desde_noc" name="desde[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                            <select id="hasta_noc" name="hasta[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>

                                        </div>
                                        </div>

                                        <div id="intervalos_select_4">
                                             <div class="col-lg-1" id="num_inter">Cerrado</div>

                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                            <select id="desde_noc" name="desde[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                            <select id="hasta_noc" name="hasta[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>

                                        </div>
                                        </div>
                                       
                                    </div>
                                    <div class="form-group clearfix" id="add_hour">

                                        <div class="col-lg-4">
                                            <button type="button" class="btn btn-default waves-effect" id="add_horas">Agregar</button> 
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3" id="table_hour">
                                    <div class="form-group clearfix">
                                        <table class="table m-0">

                                            <thead>
                                                <tr>
                                                <th>Horario del Evento</th>

                                                </tr>
                                            </thead>
                                            <tbody id="horas_tr">
                                                <tr>                                               
                                                <td>Hora</td>
                                                <td></td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                
                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_2"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_3"> <span>Siguiente</span>  </button>

                                </div>
                            </div>
                        </div> 

                        <div class="tab-pane" id="addrees_tap">
                            <form id="addrees_form">

                                <div class="form-group clearfix">
                                    <label for="department" class="col-lg-2 control-label">Departamento</label> 
                                    <div class="col-lg-10">
                                        <select  class="form-control" required id="department" name="department" >
                                            <?php department(null); ?>


                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="province" class="col-lg-2 control-label">Provincia</label> 
                                    <div class="col-lg-10">
                                        <select  class="form-control" required id="province" name="province" >
                                        </select>
                                    </div>
                                </div> 

                                <div class="form-group clearfix">
                                    <label for="district" class="col-lg-2 control-label">Distrito</label> 
                                    <div class="col-lg-10">
                                        <select  class="form-control" required id="district" name="district">
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="addrees">Direccion</label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" required id="addrees" name="addrees" type="text">
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-lg-12 control-label " for="exp"> ¿Quien organiza el evento?</label>
                                    <div class="col-lg-4">
                                        <label class="col-lg-5 control-label " for="horario"> Mi Empresa</label>
                                        <input id="organizador_1" checked type="radio" name="organizador" value="1"  data-color="#81c868"/>
                                    </div>

                                    <div class="col-lg-6">
                                        <label class="col-lg-2 control-label " for="horario"> Otro</label>
                                        <div class="col-lg-2">
                                            <input id="organizador_2"  type="radio" name="organizador" value="0"  data-color="#81c868"/>
                                        </div>
                                        <div class="col-lg-4">
                                            <input  id="otro" name="otro" type="text">
                                        </div>
                                    </div>

                                </div>

                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_3"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_4"> <span>Siguiente</span>  </button>

                                </div>
                            </div>
                        </div> 

                        <div class="tab-pane" id="datos-2">
                            <form id="datos-2_form">
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="face">Facebook </label>
                                    <div class="col-lg-4">
                                        <input class=" form-control" id="face" name="face"  type="text">
                                    </div>
                                    <label class="col-lg-2 control-label " for="web">Web </label>
                                    <div class="col-lg-4">
                                        <input class=" form-control" id="web"  name="web" type="url">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="cel">Telefono Celular </label>
                                     <div class="col-lg-4 input_fields_wrap">
                                        <input class=" col-lg-10" id="cel"  data-mask="+99-999-999-999" name="cel[]" type="text"><i class="col-lg-2  ti-plus add_field_button"></i>

                                    </div>
                                    <label class="col-lg-2 control-label "  for="ofi">Telefono Oficina </label>
                                    <div class="col-lg-4">
                                        <input class=" form-control" id="ofi" data-mask="+99-999-999-999" name="ofi" type="text">
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="email">Email</label>
                                    <div class="col-lg-4">
                                        <input class=" form-control" id="email"  name="email" type="email">
                                    </div>


                                </div>
                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_4"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_5"> <span>Siguiente</span>  </button>

                                </div>
                            </div>
                        </div> 
                        <div class="tab-pane" id="settings-2">
                            <form id="settings-2_form">
                                <div class="form-group clearfix">
                                    <label class="col-lg-12 control-label " for="exp"> ¿El evento es?</label>
                                    <div class="col-lg-3">
                                        <label class="col-lg-5 control-label " for="horario"> Gratuito</label>
                                        <input id="type_event_1" checked type="radio" name="type_event" value="1"  data-color="#81c868"/>
                                    </div>

                                    <div class="col-lg-8">
                                        <label class="col-lg-3 control-label " for="horario"> De pago</label>
                                        <div class="col-lg-2">
                                            <input id="type_event_2"  type="radio" name="type_event" value="0"  data-color="#81c868"/>
                                        </div>

                                        <div class="col-lg-3">

                                            <select id="id_coin">
                                                <option value="1">Sol</option>
                                                <option value="2">USD</option>                                              

                                            </select>                                            </div>

                                        <div class="col-lg-3">
                                            <input class=" form-control" id="coin" name="coin" type="number" step="any" >
                                        </div>
                                    </div>
                                    <div class="col-lg-12" id="id_coin_div">

                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-lg-12 control-label " for="exp"> ¿Quienes pueden asistir al evento?</label>
                                    <div class="col-lg-2">
                                        <label class="col-lg-5 control-label " for="horario"> Toda la familia</label>
                                        <input id="assistance_type_1" checked type="radio" name="assistance_type" value="1"  data-color="#81c868"/>
                                    </div>

                                    <div class="col-lg-2">
                                        <label class="col-lg-5 control-label " for="horario"> Niños</label>
                                        <input id="assistance_type_2"  type="radio" name="assistance_type" value="5"  data-color="#81c868"/>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="col-lg-6 control-label " for="horario"> Adolecentes</label>
                                        <input id="assistance_type_3"  type="radio" name="assistance_type" value="16"  data-color="#81c868"/>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="col-lg-5 control-label " for="horario"> Adultos</label>
                                        <input id="assistance_type_4"  type="radio" name="assistance_type" value="21"  data-color="#81c868"/>
                                    </div>

                                </div>
                                <div class="col-lg-4">
                                    <div class="checkbox">
                                        <input id="checkbox0" type="checkbox" data-parsley-required-message="Debe Aceptar los terminos y condiciones"  data-parsley-required  >
                                        <label for="checkbox0">
                                            Acepto terminos y condiciones 
                                        </label>
                                        <a href="#custom-modal"   data-toggle="modal" data-target="#myModal">Leer</a>

                                    </div>
                                </div>
                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_5"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_6"> <span>Guardar</span>  </button>

                                </div>
                            </div>
                        </div> 




                    </div> 
                </div> 
            </div>



        </div> <!-- container -->

    </div> <!-- content -->


</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->



<?php
require_once "./template/footer.php";
ob_end_flush();
?>        


<script src="assets/plugins/moment/moment.js"></script>
<script type="text/javascript" src="assets/plugins/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="assets/plugins/moment/moment.js"></script>
<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

<script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
<script src="assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>
<!--Form Wizard-->
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>



<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="assets/plugins/jquery-file-upload-master/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="assets/plugins/jquery-file-upload-master/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-validate.js"></script>
<script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

<script src="template/js/fechas.js" type="text/javascript"></script>
<script src="template/js/siguiente_event.js" type="text/javascript"></script>
<script src="template/js/horas_events.js" type="text/javascript"></script>
<script src="template/js/file.js" type="text/javascript"></script>
<script src="template/js/phone.js" type="text/javascript"></script>
<script type="text/javascript">
    var extras = [];
    var int_dias = [];
    var int_horas = [];
    $(window).load(function () {
        $('.selectpicker').selectpicker();
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        $('.autonumber').autoNumeric('init');
        $('.portfolioFilter a').click(function () {
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });
    $(document).ready(function () {
        $("#intervalos").hide();
    $("#dh_horas").hide();
    $("#num_inter").hide();
    $("#add_hour").hide();
    $("#table_hour").hide();
        $('#dia').datepicker({
            startDate: new Date(),
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        $("#evento_valio").hide();

        $("#si_va_day").change(function () {
            $('#desde').prop('required', true);
            $('#hasta').prop('required', true);
            $('#dia').prop('required', false);
            $("#evento_valio").show();
            $("#evento_dia").hide();
        });
        $("#no_va_day").change(function () {
            $('#desde').prop('required', false);
            $('#hasta').prop('required', false);
            $('#dia').prop('required', true);
            $("#evento_dia").show();
            $("#evento_valio").hide();
        });
        $("#otro").hide();
        $("#organizador_2").change(function () {
            $('#otro').prop('required', true);
            $("#otro").show();
        });
        $("#organizador_1").change(function () {
            $('#otro').prop('required', false);
            $("#otro").hide();
        });

        $("#id_coin").hide();
        $("#coin").hide();
        $("#type_event_2").change(function () {
            $('#id_coin').prop('required', true);
            $('#coin').prop('required', true);
            $("#id_coin").show();
            $("#coin").show();
        });
        $("#type_event_1").change(function () {
            $('#id_coin').prop('required', false);
            $('#coin').prop('required', false);
            $("#id_coin").hide();
            $("#coin").hide();
        });









        $('form').parsley();

        $('#department').change(function ()
        {
            var idDepartment = $(this).val();
            $('#province').empty();
            $('#district').empty();
            $.ajax({
                cache: false,
                url: '../views/addresses.php?action=department&id=' + idDepartment,
                type: 'GET',
                cache: false,
                success: function (response) {
                    $('#province').append(response);
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });
        });

        $('#province').change(function ()
        {
            var idProvince = $(this).val();
            $('#district').empty();
            $.ajax({
                url: '../views/addresses.php?action=province&id=' + idProvince,
                type: 'GET',
                cache: false,
                success: function (response) {
                    $('#district').append(response);
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });
        });

    });


    //Se utiliza para que el campo de texto solo acepte letras
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toString();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
        especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }
</script>

