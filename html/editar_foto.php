<?php
ob_start();
require_once "./template/header.php";
require_once "../views/addresses.php";
require_once "../views/fileEdit.php";
$res = file2($_GET["id"]);
?>

<link href="assets/plugins/bootstrapvalidator/src/css/bootstrapValidator.css" rel="stylesheet" type="text/css" />
<!-- ========== Left Sidebar Start ========== -->    
<?php
require_once "./template/menus.php";
?>

<div class="content-page">
    <div class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-30 header-title"><b>Editar Fotos</b></h4>
                    <form  id="form_subir_foto" data-parsley-validate  novalidate method="post" >
                         <div class="row">
                            <div class="col-lg-3 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">
                                <label class="col-md-2 control-label">Titulo</label>
                                <div class="col-md-10">
                                    <input class="form-control" minlength="2" maxlength="30" value="<?php echo $res[9]; ?>"  type="text" required placeholder="Titulo"  name="title" id="title">
                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">

                                <label class="col-md-2 control-label"></label>
                                <div class="radio radio-inline">
                                    <input type="radio" id="criterio" value="1" name="type" <?php echo $res[3] == "1" ? "checked" : ""; ?> >
                                    <label for="criterio"> Profesional </label>
                                </div>
                                <div class="radio radio-inline">
                                    <input type="radio" id="criterio2" value="0" name="type" <?php echo $res[3] == "0" ? "checked" : ""; ?>>
                                    <label for="criterio2"> Amateur </label>
                                </div> 
                                <input type="hidden" id="id_file" name="id_file" value="<?php echo $res[0]; ?>">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">
                                <label class="col-md-2 control-label">Descripcion</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="descripcion" id="descripcion" rows="5" ><?php echo $res[4]; ?></textarea>
                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">


                                <label for="department" class="col-md-2 control-label">Direccion</label>
                                <div class="col-md-10">
                                    <select required class="form-control" id="department" name="department" >
                                        <?php department($res[5]); ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <hr>
                        <div class="row"> 
                            <div class="col-lg-5 m-t-10"></div>
                            <div class="col-lg-6 m-t-10">
                                <button type="button" id="button" class="btn btn-info waves-effect waves-light">Guardar</button> 
                            </div>
                        </div> 
                    </form>
                </div>

            </div>
            <div class="col-lg-4">

                <div class="card-box">
                    <h4 class="m-t-0 m-b-30 header-title"><b>Vista</b></h4>
                    
                        <img src="<?php echo $res[2]; ?>" alt="image" class="img-responsive" width="200"/>
                       
                    


                </div>




            </div>




        </div>
    </div>
</div>

<?php
require_once "./template/footer.php";
ob_end_flush();
?>

<script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>


<script>
    $(document).ready(function () {
         $("#button").click(function (event){
             $("form").submit();
         });
        $("form").submit(function (event)
        {
            
          
            event.preventDefault();
            $.ajax({
                url: "../views/fileEdit.php",
                type: "POST",
                data: $(this).serialize(),
                cache: false,
                beforeSend: function ()
                {
                    $(".se-pre-con").show();
                },
                success: function (data)
                {
                    $(".se-pre-con").hide();
                    switch (data) {
                        case "1":
                            $.Notification.notify('success', 'top center', 'Foto Guardada Con Exito', 'Foto Guardada Con Exito.');
                            break;
                        case "0":
                            $.Notification.notify('error', 'top center', 'Error Guardando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "2":
                            $.Notification.notify('error', 'top center', 'Error Guardando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "3":
                            $.Notification.notify('error', 'top center', 'Formato de imagen incorrecto', 'Formato de imagen incorrecto.');
                            break;

                    }

                },
                error: function (e)
                {
                    $.Notification.notify('error', 'top center', 'Error Guardando', 'Error en sistema comuniquese con Soporte Tecnico.');
                }
            });


        });

    });

</script>


