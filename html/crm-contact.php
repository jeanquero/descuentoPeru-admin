<?php
ob_start();

require_once "./template/header.php";
require_once "../views/email.php";
$mesajes = enviados($_GET["id_envia"]);
$person = person($_GET["id_envia"]);
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>


<?php
require_once "./template/menus.php";
?>


<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->                      
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">



            <div class="row">


                <div class="col-lg-12">
                    <div class="card-box">

                        <div class="contact-card">
                            <a class="pull-left" href="#">
                                <img class="img-circle" src="<?php echo!empty($person[8]) ? $person[8] : "assets/images/users/avatar-1.jpg"; ?>" alt="">
                            </a>
                            <div class="member-info">
                                <h4 class="m-t-0 m-b-5 header-title"><b><?php echo $person[1] . " " . $person[2]; ?></b></h4>

                                <div class="m-t-20">
                                    <a href="email-compose.php?id_recive=<?php echo $person[0]; ?>&name=<?php echo $person[1] . " " . $person[2]; ?>" class="btn btn-purple waves-effect waves-light btn-sm">Enviar Mensaje</a>

                                </div>
                            </div>

                        </div>

                        <div class="">

                            <div class="p-20">
                                <h4 class="m-b-20 header-title"><b>Mensajes</b></h4>
                                <div class="nicescroll p-l-r-10" style="max-height: 555px;">
                                    <div class="timeline-2">
                                        <?php foreach ($mesajes as $value) {
                                            ?>        
                                            <div class="time-item">
                                                <div class="item-info">
                                                    <div class="text-muted"><small><?php
                                        $dt1 = new DateTime($value->create_chat);
                                        $dt2 = new DateTime(date("Y/m/d"));
                                        $i = $dt1->diff($dt2);
                                        echo $i->format('Enviado hace %a dias %h horas %i minuto(s) %s segundo(s)');
                                            ?></small></div>
                                                    <p><strong><a href="#" class="text-info"><?php echo $value->first_name . " " . $value->last_name; ?>:          </a></strong><strong><?php echo $value->message; ?></strong></p>
                                                </div>
                                            </div>
                                            <?php }
                                        ?>  


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>





        </div> <!-- container -->

    </div> <!-- content -->



</div>





<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<?php
require_once "./template/footer.php";
ob_end_flush();
?>  