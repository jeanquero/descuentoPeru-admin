<?php
ob_start();
require_once "../views/addresses.php";
require_once "../views/genery.php";
require_once "./template/header.php";
?>

<!--venobox lightbox-->
<link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css"/>
<link type="text/css" href="assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="assets/plugins/bootstrapvalidator/src/css/bootstrapValidator.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/select2/select2.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
<!-- ========== Left Sidebar Start ========== -->   
<link rel="stylesheet" type="text/css" href="assets/plugins/jquery.steps/demo/css/jquery.steps.css" />
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/style.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="assets/plugins/jquery-file-upload-master/css/jquery.fileupload.css">
<?php
require_once "./template/menus.php";
require_once "../views/negocio.php";
$negocios = buscarNegocio($_GET["id"]);
$time = buscarTime($_GET["id"]);
$extras = buscarServiceExtra($_GET["id"]);
?>

<style>
    .elm_li {margin: 2px 0 10px 5px;}
</style>

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->                      
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <div class="row">     
                <div class="col-lg-12"> 
                    <ul class="nav nav-tabs tabs">
                        <li class="active tab">
                            <a href="#home-2" data-toggle="tab" id="tab_1" aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                <span class="hidden-xs">Datos Principales</span> 
                            </a> 
                        </li> 
                        <li class="tab"> 
                            <a href="#profile-2" id="tab_2" data-toggle="tab" aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                <span class="hidden-xs">Direccion</span> 
                            </a> 
                        </li> 
                        <li class="tab"> 
                            <a href="#messages-2" data-toggle="tab" id="tab_3" aria-expanded="true"> 
                                <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> 
                                <span class="hidden-xs">Logo del Negocio</span> 
                            </a> 
                        </li> 
                        <li class="tab"> 
                            <a href="#settings-2" data-toggle="tab" id="tab_4"  aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-cog"></i></span> 
                                <span class="hidden-xs">Servicios Extras</span> 
                            </a> 
                        </li> 
                        <li class="tab"> 
                            <a href="#horarios-2" data-toggle="tab" id="tab_5"  aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-cog"></i></span> 
                                <span class="hidden-xs">Horarios de Atenci&oacute;n</span> 
                            </a> 
                        </li> 
                        <li class="tab"> 
                            <a href="#datos-2" data-toggle="tab" id="tab_6"  aria-expanded="false"> 
                                <span class="visible-xs"><i class="fa fa-cog"></i></span> 
                                <span class="hidden-xs">Datos de Contacto</span> 
                            </a> 
                        </li> 
                    </ul> 
                    <div class="tab-content"> 

                        <div class="tab-pane active" id="home-2"> 
                            <form id="home_2_form">

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="name">Nombre de la Empresa </label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" required id="name" value="<?php echo $negocios->name ?>" name="name" type="text">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="category"> Categoria</label>
                                    <div class="col-lg-10">
                                        <select class="selectpicker m-b-0" required  data-style="btn-white" id="category" name="category">
                                            <?php category($negocios->id_category); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="sub_category">Sub Categoria</label>
                                    <div class="col-lg-10">
                                        <select class="selectpicker m-b-0" required  data-style="btn-white" id="sub_category" name="sub_category">
                                            <?php sub_category($negocios->id_sub_category); ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-11"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_1"> <span>Siguiente</span>  </button>

                                </div>
                            </div>   
                        </div> 
                        <div class="tab-pane" id="profile-2">
                            <form id="profile-2_form">

                                <div class="form-group clearfix">
                                    <label for="department" class="col-lg-2 control-label">Departamento</label> 
                                    <div class="col-lg-10">
                                        <select  class="form-control" required id="department" name="department" >
                                            <?php department($negocios->id_department); ?>


                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="province" class="col-lg-2 control-label">Provincia</label> 
                                    <div class="col-lg-10">
                                        <select  class="form-control" required id="province" name="province" >
                                            <?php provinceSelect($negocios->id_department, $negocios->id_province); ?>
                                        </select>
                                    </div>
                                </div> 

                                <div class="form-group clearfix">
                                    <label for="district" class="col-lg-2 control-label">Distrito</label> 
                                    <div class="col-lg-10">
                                        <select  class="form-control" required id="district" name="district">
                                            <?php districtSelect($negocios->id_province, $negocios->id_district); ?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="addrees">Direccion</label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" required id="addrees" value="<?php echo $negocios->address ?>" name="addrees" type="text">
                                    </div>
                                </div>
                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_1"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_2"> <span>Siguiente</span>  </button>

                                </div>
                            </div>
                        </div> 
                        <div class="tab-pane" id="messages-2">
                            <form id="messages-2_form">
                                <div class="form-group clearfix">
                                    <div class="col-lg-4">
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Subir Imagen</span>
                                            <input name="foto" type="file" id="foto"  accept="image/*"/>
                                        </span>
                                    </div>
                                    <div class="col-lg-6">
                                        <div id="files" class="files">
                                            <img src="<?php echo $negocios->img ?>" width="200" height="200"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="description"> Describe tu Empresa</label>
                                    <div class="col-lg-10">
                                        <textarea id="description" required name="description" class="form-control" data-parsley-id="50" ><?php echo $negocios->description; ?></textarea>

                                    </div>
                                </div>

                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_2"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_3"> <span>Siguiente</span>  </button>

                                </div>
                            </div>
                        </div> 
                        <div class="tab-pane" id="settings-2">
                            <form id="settings-2_form">
                                <div class="form-group clearfix">
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="extra" name="extra" type="text">
                                        <input type="hidden" name="service_extra_delete" id="service_extra_delete">
                                    </div>
                                    <div class="col-lg-4">
                                        <button type="button" class="btn btn-default waves-effect" id="add">Agregar</button> 
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4">
                                        <ul id="add_old">
 <?php
                                   
                                      $i=0;
                                      foreach ($extras as $value) {
                                        echo '<li id="li_del_'.$i.'">'.$value->service. ' <button type="button" class="btn btn-default remove_extra " id="bn_del_'.$i.'" >Eliminar</button> <br>  </li> ';
     
     $i++;
                                                }?>
                                        </ul>
                                        <ul id="add_ul">

                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_3"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_4"> <span>Siguiente</span>  </button>

                                </div>
                            </div>
                        </div> 

                        <div class="tab-pane" id="horarios-2">
                            <form id="horarios-2_form">
                                <div class="col-lg-9">
                                    <div class="form-group clearfix">
                                        <div class="col-lg-12">
                                            <table class="table m-0">

                                                <thead>
                                                    <tr>
                                                    <th>Lunes</th>
                                                    <th>Martes</th>
                                                    <th>Miercoles</th>
                                                    <th>Jueves</th>
                                                    <th>Viernes</th>
                                                    <th>Sabado</th>
                                                    <th>Domingo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    <td><input id="dia_1" type="checkbox" class="dia" name="lunes" value="Lunes"  data-color="#81c868"/></td>
                                                    <td><input id="dia_2" type="checkbox" class="dia" name="martes" value="Martes"  data-color="#81c868"/></td>
                                                    <td><input id="dia_3" type="checkbox" class="dia" name="miercoles" value="Miercoles"  data-color="#81c868"/></td>
                                                    <td><input id="dia_4" type="checkbox" class="dia" name="jueves" value="Jueves"  data-color="#81c868"/></td>
                                                    <td><input id="dia_5" type="checkbox" class="dia" name="vierne" value="Viernes"  data-color="#81c868"/></td>
                                                    <td><input id="dia_6" type="checkbox" class="dia" name="sabado" value="Sabados"  data-color="#81c868"/></td>
                                                    <td><input id="dia_7" type="checkbox" class="dia" name="domingo" value="Domingos"  data-color="#81c868"/></td>
                                                    </tr>


                                                </tbody>
                                            </table>

                                        </div>

                                        <div class="col-lg-12">
                                            <hr  style="color: #2184be;border:0;  border-top:1px solid">
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">

                                        <div class="col-lg-3">
                                            <label class="col-lg-7 control-label " for="horario"> Horario Personalizado</label>
                                            <input id="horas_1" type="radio" name="horas" value="1"  data-color="#81c868"/>
                                        </div>

                                        <div class="col-lg-2">
                                            <label class="col-lg-6 control-label " for="horario"> 24 Horas</label>
                                            <input id="horas_2" checked type="radio" name="horas" value="24"  data-color="#81c868"/>
                                        </div>
                                        <div class="col-lg-12">
                                            <hr  style="color: #2184be;border:0;  border-top:1px solid">
                                        </div>
                                    </div>

                                    <div class="form-group clearfix" id="intervalos">
                                        <label class="col-lg-2 control-label " for="horario">¿Tiene Intervalo de Descanso?</label>
                                        <div class="col-lg-2">
                                            Si <input id="inter_1" type="radio" name="inter"  />
                                        </div>
                                        <div class="col-lg-2">
                                            No <input id="inter_2" checked type="radio" name="inter"   />
                                        </div>


                                        <div class="col-lg-3" id="num_inter">
                                            <label class="col-lg-6 control-label " id="number_inter" for="h_inter"> Numeros de Intervalos</label>
                                            <select id="hor_inter">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                
                                            </select>
                                        </div>


                                    </div>

                                    <div class="form-group clearfix" id="dh_horas">


                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                            <select id="desde_man" name="desde[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                            <select id="hasta_man" name="hasta[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div id="intervalos_select_2">
                                             <div class="col-lg-1" id="num_inter">Cerrado</div>

                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                            <select id="desde_noc" name="desde[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                            <select id="hasta_noc" name="hasta[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>

                                        </div>
                                        </div>

                                        <div id="intervalos_select_3">
                                             <div class="col-lg-1" id="num_inter">Cerrado</div>

                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                            <select id="desde_noc" name="desde[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                            <select id="hasta_noc" name="hasta[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>

                                        </div>
                                        </div>

                                        <div id="intervalos_select_4">
                                             <div class="col-lg-1" id="num_inter">Cerrado</div>

                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Desde</label>
                                            <select id="desde_noc" name="desde[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1" id="num_inter">
                                            <label class="col-lg-6 control-label " for="horario"> Hasta</label>
                                            <select id="hasta_noc" name="hasta[]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>

                                        </div>
                                        </div>
                                       
                                    </div>
                                    <div class="form-group clearfix">

                                        <div class="col-lg-4">
                                            <button type="button" class="btn btn-default waves-effect" id="add_horas">Agregar</button> 
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group clearfix">
                                        <table class="table m-0">

                                            <thead>
                                                <tr>
                                                <th>Horario de Atencio</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                <td>D&iacute;a</td>
                                                <td>Hora</td>
                                                <td></td>
                                                </tr>
                                      <?php
                                   
                                      $i=0;
                                      foreach ($time as $value) {
     echo '<tr id="tr_' . $i . '_upd" ><td>' . $value->dia . '</td><td>' . $value->hora . '</td><td><button type="button" class="btn btn-default eliminar" id="del_td_' . $i . '_upd">Eliminar</button></td></tr>';
     $i++;
                                                }?>

                                            </tbody>
                                            <tbody id="horas_tr"></tbody>
                                        </table>
                                    </div>
                                </div>
                                 <input type="hidden" name="del_time" id="del_time">
                                <input type="hidden" name="int_dias" id="int_dias">
                                <input type="hidden" name="int_horas" id="int_horas">
                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_4"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_5"> <span>Siguiente</span>  </button>

                                </div>
                            </div>
                        </div> 

                        <div class="tab-pane" id="datos-2">
                            <form id="datos-2_form">
                                <input type="hidden" value="<?php echo $_GET["id"]; ?>" id="id_com"name="id_com"/>
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="face">Facebook </label>
                                    <div class="col-lg-4">
                                        <input class=" form-control" id="face" value="<?php echo $negocios->facebook; ?>" name="face"  type="text">
                                    </div>
                                    <label class="col-lg-2 control-label " for="web">Web </label>

                                    <div class="col-lg-4">

                                        <input class=" form-control" id="web" value="<?php echo $negocios->web; ?>"  name="web" type="url">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label " for="cel">Telefono Celular </label>
                                    <?php $cel = explode(",", $negocios->phone); ?>
                                   <div class="col-lg-4 input_fields_wrap">
                                        <input class=" col-lg-10" id="cel"  data-mask="+99-999-999-999" name="cel[]" type="text" value="<?php echo $cel[0]; ?>"><i class="col-lg-2  ti-plus add_field_button"></i>
                                     <?php if (count($cel)>1) {
                                        for ($i=1; $i < count($cel); $i++) { 
                                            echo '<div><input class=" col-lg-10"   data-mask="+99-999-999-999" name="cel[]" type="text" value="'.$cel[$i].'"><i  class="remove_field  ti-close col-lg-2"></i></div>';
                                        }
                                     } ?>
                                    </div>
                                   
                                    <label class="col-lg-2 control-label "  for="ofi">Telefono Oficina </label>
                                    <div class="col-lg-4">
                                        <input class=" form-control" id="ofi"  value="<?php echo $negocios->office_phone; ?>"  data-mask="+99-99-99-99" name="ofi" type="text">
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-lg-2 control-label "   for="email">Email</label>
                                    <div class="col-lg-4">
                                        <input class=" form-control" id="email"  value="<?php echo $negocios->email; ?>"  name="email" type="email">
                                    </div>


                                </div>
                            </form>
                            <div class="form-group clearfix">
                                <div class="col-lg-10"></div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="atras_5"> <span>Atras</span>  </button>

                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success" type="button" id="siguiente_6"> <span>Guardar</span>  </button>

                                </div>
                            </div>
                        </div> 
                    </div> 
                </div> 
            </div>



        </div> <!-- container -->

    </div> <!-- content -->


</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->



<?php
require_once "./template/footer.php";
ob_end_flush();
?>        


<script src="assets/plugins/moment/moment.js"></script>
<script type="text/javascript" src="assets/plugins/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="assets/plugins/moment/moment.js"></script>
<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.js"></script>

<script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
<script src="assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>
<!--Form Wizard-->
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>



<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="assets/plugins/jquery-file-upload-master/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="assets/plugins/jquery-file-upload-master/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="assets/plugins/jquery-file-upload-master/js/jquery.fileupload-validate.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

<script src="template/js/phone.js" type="text/javascript"></script>
<script src="template/js/siguiente_negocio_edit.js" type="text/javascript"></script>
<script src="template/js/horario.js" type="text/javascript"></script>
<script src="template/js/servicios_extra.js" type="text/javascript"></script>

<script type="text/javascript">
    var extras = [];
    var int_dias = [];
    var int_horas = [];
    $(window).load(function () {
        $('.selectpicker').selectpicker();
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        $('.autonumber').autoNumeric('init');
        $('.portfolioFilter a').click(function () {
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });
    $(document).ready(function () {
       
        <?php
                                     
                                      $i=0;
                                      foreach ($time as $value) {
                                          echo '$("#del_td_'.$i.'_upd").click(function () {'
                                                  . ' $("#tr_' . $i . '_upd").remove();
if($("#del_time").val()==""){
                                                     '
                                                  . '$("#del_time").val('.$value->id_timetable.');'
                                                  . '}else {'
                                                . '$("#del_time").val($("#del_time").val()+","+'.$value->id_timetable.');
                                                  }
                                                   });';
     $i++;
                                                }?>

                                                <?php
                                     
                                      $i=0;
                                      foreach ($extras as $value) {
                                          echo '$("#bn_del_'.$i.'").click(function () {'
                                                  . ' $("#li_del_' . $i . '").remove();
if($("#service_extra_delete").val()==""){
                                                     '
                                                  . '$("#service_extra_delete").val('.$value->id_extra_service.');'
                                                  . '}else {'
                                                . '$("#service_extra_delete").val($("#service_extra_delete").val()+","+'.$value->id_extra_service.');
                                                  }
                                                   });';
     $i++;
                                                }?>
              

        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            }
        });

        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        $('form').parsley();

        $('#department').change(function ()
        {
            var idDepartment = $(this).val();
            $('#province').empty();
            $('#district').empty();
            $.ajax({
                cache: false,
                url: '../views/addresses.php?action=department&id=' + idDepartment,
                type: 'GET',
                cache: false,
                success: function (response) {
                    $('#province').append(response);
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });
        });

        $('#province').change(function ()
        {
            var idProvince = $(this).val();
            $('#district').empty();
            $.ajax({
                url: '../views/addresses.php?action=province&id=' + idProvince,
                type: 'GET',
                cache: false,
                success: function (response) {
                    $('#district').append(response);
                },

                error: function (e) {
                    alert("Error en sistema comuniquese con Soporte Tecnico");
                    console.log(e);
                }
            });
        });
        $("#messages-2_form").submit(function (event)
        {
            var file_data = $('#foto').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append("data", $("#messages-2_form").serialize())
            form_data.append("data", $("#home_2_form").serialize())
            event.preventDefault();
            $.ajax({
                url: "../views/negocio.php",
                type: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function ()
                {
                    $(".se-pre-con").show();
                },
                success: function (data)
                {
                    alert(data);
                    switch (data) {
                        case "0":
                            location.reload();
                            break;
                        case "1":
                            $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "2":
                            $.Notification.notify('error', 'top center', 'Error Actualizando', 'Error en sistema comuniquese con Soporte Tecnico.');
                            break;
                        case "3":
                            $.Notification.notify('error', 'top center', 'Formato de imagen incorrecto', 'Formato de imagen incorrecto.');
                            break;

                    }

                },
                error: function (e)
                {
                    //$("#err").html(e).fadeIn();
                }
            });
        });
    });
    /*jslint unparam: true, regexp: true */
    /*global window, $ */
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'server/php/',
                uploadButton = $('<button/>')
                .addClass('btn btn-primary')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function () {
                    var $this = $(this),
                            data = $this.data();
                    $this
                            .off('click')
                            .text('Abort')
                            .on('click', function () {
                                $this.remove();
                                data.abort();
                            });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
        $("#submit_foto").hide();
        $('#foto').change(function () {
            $("#files").empty();
        });
        $('#foto').fileupload({
            url: url,
            dataType: 'json',
            autoUpload: false,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize: 999000000,
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            limitMultiFileUploads: 1,
            disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
            previewMaxWidth: 200,
            previewMaxHeight: 200,
            previewCrop: true
        }).on('fileuploadadd', function (e, data) {
            data.context = $('<div/>').appendTo('#files');
            $.each(data.files, function (index, file) {
                var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                if (!index) {
                    node
                            .append('<br>')

                    $("#close_img").click(function () {


                        $("#files").empty();
                        $("#submit_foto").hide();
                    });
                }

                node.appendTo(data.context);
            });
        }).on('fileuploadprocessalways', function (e, data) {
            var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
            if (file.preview) {
                node
                        .prepend('<br>')
                        .prepend(file.preview);
            }
            if (file.error) {
                node
                        .append('<br>')
                // .append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                $("#submit_foto").show();

            }

        }).on('fileuploaddone', function (e, data) {
            $.each(data.result.files, function (index, file) {
                if (file.url) {
                    var link = $('<a>')
                            .attr('target', '_blank')
                            .prop('href', file.url);
                    $(data.context.children()[index])
                            .wrap(link);
                } else if (file.error) {
                    var error = $('<span class="text-danger"/>').text(file.error);
                    $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                }
            });
        }).on('fileuploadfail', function (e, data) {
            $.each(data.files, function (index) {
                var error = $('<span class="text-danger"/>').text('File upload failed.');
                $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
            });
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

    });

    //Se utiliza para que el campo de texto solo acepte letras
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toString();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
        especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }
</script>

