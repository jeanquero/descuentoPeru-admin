<?php
ob_start();
require_once "./template/header.php";
require_once "../views/gallery.php";
$galllery = gallery();
?>

<!-- ========== Left Sidebar Start ========== -->    
<?php
require_once "./template/menus.php";
?>

<div class="content-page">
    <div class="content">       <div class="container">




            <!-- SECTION FILTER
            ================================================== -->  
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 ">
                    <div class="portfolioFilter">
                        <a href="#" data-filter="*" class="current">Todas</a>
                        <a href="#" data-filter=".profesional">Profesional</a>
                        <a href="#" data-filter=".amateur">Amateur</a>

                    </div>
                </div>
            </div>

            <div class="row port">
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-4">                        
                        <a href="subir_foto.php" class="btn btn-pink btn-block text-uppercase waves-effect waves-light" data-animation="push" data-plugin="custommodal" 
                           data-overlaySpeed="100" data-overlayColor="#36404a">Subir Fotos</a>
                    </div>
                </div>
                <br>
                <div class="portfolioContainer">
                    <?php
                    if ($galllery != NULL) {
                        foreach ($galllery as $res) {
                            ?>
                            <div class="col-sm-6 col-lg-3 col-md-4  <?php echo $res->type == "0" ? "amateur" : "profesional"; ?>">
                                <div class="gal-detail thumb">
                                    <h4><i class="ti-heart"></i> <?php echo $res->point == NULL ? "0" : $res->point; ?></h4>
                                    <a href="<?php echo $res->url; ?>" class="image-popup" >
                                        <img src="<?php echo $res->url; ?>" class="thumb-img" alt="work-thumbnail">
                                    </a>
                                    <br>
                                    <br>
                                    <p>
                                        <a href="#" id="eliminar_<?php echo $res->id_file; ?>" class="btn btn-primary waves-effect waves-light a_mod" role="button">Eliminar</a>
                                        <a href="#" id="editar_<?php echo $res->id_file; ?>" class="btn btn-default waves-effect waves-light m-l-5 a_mod" role="button">Editar</a>
                                    </p>

                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>




                </div>
            </div> <!-- End row -->

        </div> 

    </div>
</div>

<?php
require_once "./template/footer.php";
ob_end_flush();
?>


<script type="text/javascript" src="assets/plugins/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>

<script type="text/javascript">
    $(window).load(function () {
        $(".a_mod").click(function () {
            var envio = $(this).attr("id").split("_");
            if (envio[0] == "eliminar") {
                var r = confirm("¿Quiere Eliminar la foto?");
                if (r == true) {
                    $.ajax({
                        url: '../views/gallery.php',
                        type: 'POST',
                        data: {id: envio[1]},
                        cache: false, // To unable request pages to be cached
                        success: function (response) {
                            if (response) {
                                location.reload();
                            } else {
                                $.Notification.notify('error', 'top center', 'Error Eliminando', 'Error en sistema comuniquese con Soporte Tecnico.');

                            }

                        },

                        error: function (e) {
                            alert("Error en sistema comuniquese con Soporte Tecnico");
                            console.log(e);
                        }
                    });
                }
            } else {
                window.location.replace("editar_foto.php?id=" + envio[1]);
            }
        });
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.portfolioFilter a').click(function () {
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });
    $(document).ready(function () {
        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            }
        });
    });
</script>



